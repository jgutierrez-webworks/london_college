<?php

namespace Database\Seeders;

use Exception;
use App\Globals\Constants;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        try {

            DB::beginTransaction();

            DB::table('roles')->insert([
                [
                    'name' => Constants::NAME_ROLE_SUPERADMINISTRATOR,
                    'display_name' => Str::ucfirst(Constants::NAME_ROLE_SUPERADMINISTRATOR),
                    'description' => 'Has Special full access to the entire system'
                ],
                [
                    'name' => Constants::NAME_ROLE_ADMINISTRATOR,
                    'display_name' => Str::ucfirst(Constants::NAME_ROLE_ADMINISTRATOR),
                    'description' => 'Has full access to the entire system'
                ],
                [
                    'name' => Constants::NAME_ROLE_MODERATOR,
                    'display_name' => Str::ucfirst(Constants::NAME_ROLE_MODERATOR),
                    'description' => 'Has limited access to the system'
                ],
                [
                    'name' => Constants::NAME_ROLE_CLIENT,
                    'display_name' => Str::ucfirst(Constants::NAME_ROLE_CLIENT),
                    'description' => 'Has limited access to the system'
                ]
            ]);

            DB::commit();
        } catch (Exception $e) {
            DB::rollback();
            throw $e;
        }
    }
}

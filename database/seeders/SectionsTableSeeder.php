<?php

namespace Database\Seeders;

use Exception;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class SectionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        try {

            DB::beginTransaction();

            DB::table('seccion')->insert([
                [
                    'slug'        => Str::slug('Alumnos'),
                    'nombre'      => 'Alumnos',
                    'created_at'  => date('Y-m-d H:i:s'),
                    'updated_at'  => date('Y-m-d H:i:s'),
                ],
                [
                    'slug'        => Str::slug('Centro Padres'),
                    'nombre'      => 'Centro Padres',
                    'created_at'  => date('Y-m-d H:i:s'),
                    'updated_at'  => date('Y-m-d H:i:s'),
                ],
                [
                    'slug'        => Str::slug('Dirección'),
                    'nombre'      => 'Dirección',
                    'created_at'  => date('Y-m-d H:i:s'),
                    'updated_at'  => date('Y-m-d H:i:s'),
                ]
            ]);

            DB::commit();
            
        } catch (Exception $e) {
            DB::rollback();
            throw $e;
        }
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNoticiaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('noticia', function (Blueprint $table) {
            $table->id();
            $table->string('codigo', 50)->unique();
            $table->string('slug', 255)->unique();
            $table->date('fecha');
            $table->string('titulo', 255);
            $table->string('descripcion_corta', 500);
            $table->longText('descripcion');
            $table->unsignedBigInteger('user_id');
            $table->unsignedBigInteger('seccion_id');
            $table->unsignedBigInteger('categoria_id');
            $table->unsignedBigInteger('banner_archivo_id')->nullable();
            $table->tinyInteger('destacado')->default(0);
            $table->tinyInteger('estado')->default(1);
            $table->string('tags');
            $table->string('observaciones')->nullable();
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('user_id', 'noticia_user_id_foreign')
                ->references('id')->on('users')
                ->onDelete('no action')
                ->onUpdate('no action');

            $table->foreign('seccion_id', 'noticia_seccion_id_foreign')
                ->references('id')->on('seccion')
                ->onDelete('no action')
                ->onUpdate('no action');

            $table->foreign('categoria_id', 'noticia_categoria_id_foreign')
                ->references('id')->on('categoria')
                ->onDelete('no action')
                ->onUpdate('no action');

            $table->foreign('banner_archivo_id', 'noticia_banner_archivo_id_foreign')
                ->references('id')->on('archivo')
                ->onDelete('no action')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('noticia');
    }
}

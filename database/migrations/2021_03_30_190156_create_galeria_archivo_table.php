<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGaleriaArchivoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('galeria_archivo', function (Blueprint $table) {
            $table->unsignedBigInteger('galeria_id');
            $table->unsignedBigInteger('archivo_id');

            $table->foreign('galeria_id', 'galeria_archivo_galeria_id_foreign')
                ->references('id')->on('galeria')
                ->onDelete('no action')
                ->onUpdate('no action');

            $table->foreign('archivo_id', 'galeria_archivo_archivo_id_foreign')
                ->references('id')->on('archivo')
                ->onDelete('no action')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('galeria_archivo');
    }
}

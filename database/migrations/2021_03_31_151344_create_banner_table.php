<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBannerTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('banner', function (Blueprint $table) {
            $table->id();
            $table->string('codigo', 50)->unique();
            $table->tinyInteger('orden');
            $table->unsignedBigInteger('archivo_id')->nullable();
            $table->string('titulo_principal', 255)->nullable();
            $table->string('titulo_secundario', 255)->nullable();
            $table->tinyInteger('link_redirect_activo')->default(0);
            $table->string('url_link_redirect', 255)->nullable();
            $table->tinyInteger('estado')->default(1);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('banner');
    }
}

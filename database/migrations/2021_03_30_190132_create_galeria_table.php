<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGaleriaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('galeria', function (Blueprint $table) {
            $table->id();
            $table->string('codigo', 50)->unique();
            $table->string('slug', 255)->unique();
            $table->string('nombre', 255);
            $table->string('descripcion', 255);
            $table->date('fecha');
            $table->string('lugar')->nullable();
            $table->tinyInteger('destacado')->default(0);
            $table->tinyInteger('estado')->default(1);
            $table->string('observaciones')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('galeria');
    }
}

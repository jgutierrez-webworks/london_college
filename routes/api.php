<?php

use App\Http\Controllers\Api\ApiController;
use App\Http\Controllers\Api\BannersController;
use App\Http\Controllers\Api\CategoriesController;
use App\Http\Controllers\Api\GalleryController;
use App\Http\Controllers\Api\HeadlinesController;
use App\Http\Controllers\Api\HomeController;
use App\Http\Controllers\Api\NewsController;
use App\Http\Controllers\Api\ProfileController;
use App\Http\Controllers\Api\SiteController;
use App\Http\Controllers\Api\UsersController;
use App\Http\Controllers\Auth\AuthController;
use App\Http\Controllers\Auth\ForgotPasswordController;
use App\Http\Controllers\Auth\ResetPasswordController;
use App\Http\Controllers\Auth\VerificationController;
use App\Http\Controllers\Site\SectionsController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::name('api.')->group(function () {

    Route::middleware(['guest:api'])->group(function ($router) {

        Route::prefix('auth')->name('auth.')->group(function () {

            Route::post('/login', [AuthController::class, 'login'])->name('login');

            Route::post('/register', [AuthController::class, 'register'])->name('register');

            Route::post('/forgot-password', [ForgotPasswordController::class, 'sendResetLinkEmail'])->name('forgot-password');

            Route::post('/reset-password', [ResetPasswordController::class, 'reset'])->name('reset-password');

            Route::post('verify/{user}', [VerificationController::class, 'verify'])->name('verify');

            Route::post('resend', [VerificationController::class, 'resend'])->name('resend');

            Route::post('refresh', [AuthController::class, 'refresh'])->name('refresh');
        });
    });

    Route::prefix('site')->name('site.')->group(function () {

        Route::prefix('noticias')->name('news.')->group(function () {

            Route::get('/', [SiteController::class, 'getAllNews'])->name('all');

            Route::get('destacadas', [SiteController::class, 'getAllFeaturedNews'])->name('featured');

            Route::get('recientes', [SiteController::class, 'getAllRecentNews'])->name('recent');

            Route::get('{slug}', [SiteController::class, 'getFullNewsInfo'])->name('show');

            Route::get('{slug}/relacionados', [SiteController::class, 'getAllRelatedNews'])->name('related');            
        });

        Route::prefix('galerias')->name('galleries.')->group(function () {

            Route::get('/', [SiteController::class, 'getAllGalleries'])->name('all');
        });

        Route::prefix('titulares-informativos')->name('headlines.')->group(function () {

            Route::get('/', [SiteController::class, 'getAllHeadlines'])->name('all');
        });
        
        Route::post('contact', [SiteController::class, 'contact'])->name('contact');
    });

    Route::prefix('customs')->name('customs.')->group(function () {
        
        Route::get('roles', [ApiController::class, 'getRoles'])->name('roles.all');

        Route::get('categorias', [ApiController::class, 'getCategories'])->name('categories.all');

        Route::get('categorias-frecuentes', [ApiController::class, 'getFrequentCategories'])->name('categories.frequent');

        Route::get('secciones', [ApiController::class, 'getSections'])->name('sections.all');

        Route::post('users/validar-email', [ApiController::class, 'validUserEmail'])->name('users.valid-user-email');
    });
});

Route::middleware(['auth:api'])->name('api.')->group(function () {

    Route::prefix('auth')->name('auth.')->group(function () {

        Route::post('logout', [AuthController::class, 'logout'])->name('logout');

        Route::get('user', [AuthController::class, 'getAuthUser'])->name('user');
    });

    Route::prefix('home')->name('home.')->group(function () {

        Route::get('estadisticas', [HomeController::class, 'getStatistics'])->name('statistics');
    });

    Route::prefix('profile')->name('profile.')->group(function () {

        Route::get('/', [ProfileController::class, 'profile'])->name('index');

        Route::post('change-general', [ProfileController::class, 'changeGeneral'])->name('change-general');

        Route::post('change-password', [ProfileController::class, 'changePassword'])->name('change-password');
    });

    Route::prefix('banners')->name('banners.')->group(function () {

        Route::get('/', [BannersController::class, 'index'])->name('all');

        Route::post('nuevo', [BannersController::class, 'store'])->name('store');

        Route::get('{id}/detalle', [BannersController::class, 'show'])->name('show');

        Route::post('{id}/editar', [BannersController::class, 'update'])->name('update');

        Route::post('{id}/eliminar', [BannersController::class, 'delete'])->name('delete');

        Route::post('{id}/suspender', [BannersController::class, 'suspend'])->name('suspend');

        Route::post('{id}/activar', [BannersController::class, 'activate'])->name('activate');

        Route::post('{id}/subir-orden', [BannersController::class, 'upOrder'])->name('up-order');

        Route::post('{id}/bajar-orden', [BannersController::class, 'downOrder'])->name('down-order');
    });

    Route::prefix('categorias')->name('categories.')->group(function () {

        Route::get('/', [CategoriesController::class, 'index'])->name('all');

        Route::post('nuevo', [CategoriesController::class, 'store'])->name('store');

        Route::get('{id}/detalle', [CategoriesController::class, 'show'])->name('show');

        Route::post('{id}/editar', [CategoriesController::class, 'update'])->name('update');

        Route::post('{id}/eliminar', [CategoriesController::class, 'delete'])->name('delete');

        Route::post('{id}/suspender', [CategoriesController::class, 'suspend'])->name('suspend');

        Route::post('{id}/activar', [CategoriesController::class, 'activate'])->name('activate');
    });

    Route::prefix('secciones')->name('sections.')->group(function () {

        Route::get('/', [SectionsController::class, 'index'])->name('all');

        Route::post('nuevo', [SectionsController::class, 'store'])->name('store');

        Route::get('{id}/detalle', [SectionsController::class, 'show'])->name('show');

        Route::post('{id}/editar', [SectionsController::class, 'update'])->name('update');

        Route::post('{id}/eliminar', [SectionsController::class, 'delete'])->name('delete');

        Route::post('{id}/suspender', [SectionsController::class, 'suspend'])->name('suspend');

        Route::post('{id}/activar', [SectionsController::class, 'activate'])->name('activate');
    });

    Route::prefix('titulares')->name('headlines.')->group(function () {

        Route::get('/', [HeadlinesController::class, 'index'])->name('all');

        Route::post('nuevo', [HeadlinesController::class, 'store'])->name('store');

        Route::get('{id}/detalle', [HeadlinesController::class, 'show'])->name('show');

        Route::post('{id}/editar', [HeadlinesController::class, 'update'])->name('update');

        Route::post('{id}/eliminar', [HeadlinesController::class, 'delete'])->name('delete');

        Route::post('{id}/suspender', [HeadlinesController::class, 'suspend'])->name('suspend');

        Route::post('{id}/activar', [HeadlinesController::class, 'activate'])->name('activate');
    });

    Route::prefix('noticias')->name('news.')->group(function () {

        Route::get('/', [NewsController::class, 'index'])->name('all');

        Route::post('nuevo', [NewsController::class, 'store'])->name('store');

        Route::get('{id}/detalle', [NewsController::class, 'show'])->name('show');

        Route::post('{id}/editar', [NewsController::class, 'update'])->name('update');

        Route::post('{id}/eliminar', [NewsController::class, 'delete'])->name('delete');

        Route::post('{id}/suspender', [NewsController::class, 'suspend'])->name('suspend');

        Route::post('{id}/activar', [NewsController::class, 'activate'])->name('activate');
    });

    Route::prefix('galerias')->name('gallery.')->group(function () {

        Route::get('/', [GalleryController::class, 'index'])->name('all');

        Route::post('nuevo', [GalleryController::class, 'store'])->name('store');

        Route::get('{id}/detalle', [GalleryController::class, 'show'])->name('show');

        Route::post('{id}/editar', [GalleryController::class, 'update'])->name('update');

        Route::post('{id}/eliminar', [GalleryController::class, 'delete'])->name('delete');

        Route::post('{id}/suspender', [GalleryController::class, 'suspend'])->name('suspend');

        Route::post('{id}/activar', [GalleryController::class, 'activate'])->name('activate');

        Route::prefix('{gallery}/imagenes')->name('images.')->group(function () {

            Route::get('/', [GalleryController::class, 'indexImages'])->name('all');

            Route::post('nuevo', [GalleryController::class, 'storeImage'])->name('store');
        
            Route::post('{file}/eliminar', [GalleryController::class, 'deleteImage'])->name('delete');
        });
    });

    Route::prefix('usuarios')->name('users.')->group(function () {

        Route::get('/', [UsersController::class, 'index'])->name('all');

        Route::post('nuevo', [UsersController::class, 'store'])->name('store');

        Route::get('{id}/detalle', [UsersController::class, 'show'])->name('show');

        Route::post('{id}/editar', [UsersController::class, 'update'])->name('update');

        Route::post('{id}/eliminar', [UsersController::class, 'delete'])->name('delete');

        Route::post('{id}/suspender', [UsersController::class, 'suspend'])->name('suspend');

        Route::post('{id}/activar', [UsersController::class, 'activate'])->name('activate');
    });
});
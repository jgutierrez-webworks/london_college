<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ApplicationController;
use App\Http\Controllers\Site\SiteController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('intranet/')->group(function () {

    Route::get('/', function () {
        return redirect()->to('intranet/login');
    });

    Route::get('/{any}', [ApplicationController::class, 'index'])->where('any', '.*');
});

Route::middleware(['web'])->name('site.')->group(function () {

    Route::get('/', [SiteController::class, 'index'])->name('index');

    Route::get('noticias', [SiteController::class, 'news'])->name('news');

    Route::get('noticias/{slug}', [SiteController::class, 'newsItem'])->name('news-item');

    Route::get('galerias', [SiteController::class, 'galleries'])->name('galleries');

    Route::get('mision', [SiteController::class, 'mission'])->name('mission');

    Route::get('organigrama', [SiteController::class, 'organization'])->name('organization');

    Route::get('resena-historica', [SiteController::class, 'review'])->name('review');

    Route::get('nuestro-colegio', [SiteController::class, 'about'])->name('about');

    Route::get('comandos', function () {
        \Artisan::call('config:cache');
        \Artisan::call('config:clear');
        \Artisan::call('route:clear');
        \Artisan::call('view:clear');
        \Artisan::call('cache:clear');

        return "Comandos ejecutados correctamente";
    });
});


import axios from 'axios'

export const validatorPositive = value => {
	if (value >= 0) {
		return true
	}
	return false
}

export const validatorPassword = password => {
	/* eslint-disable no-useless-escape */
	const regExp = /(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[!@#$%&*()]).{8,}/
	/* eslint-enable no-useless-escape */
	const validPassword = regExp.test(password)
	return validPassword
}

export const validatorCreditCard = creditnum => {
	/* eslint-disable no-useless-escape */
	const cRegExp = /^(?:3[47][0-9]{13})$/
	/* eslint-enable no-useless-escape */
	const validCreditCard = cRegExp.test(creditnum)
	return validCreditCard
}

export const validatorUrlValidator = val => {
	if (val === undefined || val === null || val.length === 0) {
		return true
	}
	/* eslint-disable no-useless-escape */
	const re = /^(http[s]?:\/\/){0,1}(www\.){0,1}[a-zA-Z0-9\.\-]+\.[a-zA-Z]{2,5}[\.]{0,1}/
	/* eslint-enable no-useless-escape */
	return re.test(val)
}

export const validatorFormatRut = rut => {
	rut = rut.split('-')

	if (rut.length < 2) return false

	let T = rut[0]
	let DV1 = rut[1]
	let M = 0, S = 1, R = T

	for (; T; T = Math.floor(T / 10))
		S = (S + T % 10 * (9 - M++ % 6)) % 11

	let DV2 = S ? S - 1 : 'k'

	if (DV1 != DV2) return false

	return true
}

export const validatorUserEmailExists = (email, filterId) => {
	if (!email) {
		return false
	}

	return axios.post(route('api.customs.users.valid-user-email'), {
		email: email,
		user: filterId
	}).then(response => {
		if (response.data.valid != undefined && response.data.valid == 1) {
			return false
		}
		return true
	}).catch(function (error) {
		error: error
	})
}
import { canUserNavigate } from '@/auth/utils'

export default {
    methods: {
        isNotEmpty(str) {
            let pattern = /\S+/
            return pattern.test(str)
        },
        isNumber(str) {
            let pattern = /\[0-9]/g
            return pattern.test(str)
        },
        validateRut(rut) {
            rut = rut.split('-')

            if (rut.length < 2) return false

            let T = rut[0]
            let DV1 = rut[1]
            let M = 0, S = 1, R = T

            for (; T; T = Math.floor(T / 10))
                S = (S + T % 10 * (9 - M++ % 6)) % 11

            let DV2 = S ? S - 1 : 'k'

            if (DV1 != DV2) return false

            return true
        },
        canLoggedNavigate(name) {
            return canUserNavigate(name)
        },
        isImageExtension(value) {
            return this.filterAcceptImageExtension.includes(value)
        },
        isVideoExtension(value) {
            return this.filterAcceptVideoExtension.includes(value)
        }
    },
    data() {
        return {
            filterAcceptImageExtension: ['image/png','image/x-png','image/gif','image/jpeg'],
            filterAcceptVideoExtension: ['video/mp4', 'video/mov', 'video/ogg'],
        }
    },
}
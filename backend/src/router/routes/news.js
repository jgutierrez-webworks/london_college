export default [
    {
        path: '/noticias',
        name: 'news.index',
        component: () => import('@/views/news/IndexNews.vue'),
        meta: {
            pageTitle: 'Noticias',
            breadcrumb: [
                {
                    text: 'Noticias',
                    active: true,
                },
            ],
            roles: ['superadministrador', 'administrador', 'moderador']
        },
    },
    {
        path: '/noticias/nuevo',
        name: 'news.create',
        component: () => import('@/views/news/CreateNews.vue'),
        meta: {
            pageTitle: 'Crear Noticia',
            breadcrumb: [
                {
                    text: 'Noticias',
                    to: { name: 'news.index' }
                },
                {
                    text: 'Nuevo',
                    active: true,
                },
            ],
            roles: ['superadministrador', 'administrador', 'moderador']
        },
    },
    {
        path: '/noticias/:id/editar',
        name: 'news.edit',
        component: () => import('@/views/news/EditNews.vue'),
        meta: {
            pageTitle: 'Editar Noticia',
            breadcrumb: [
                {
                    text: 'Noticias',
                    to: { name: 'news.index' }
                },
                {
                    text: 'Editar',
                    active: true,
                },
            ],
            roles: ['superadministrador', 'administrador', 'moderador']
        },
    },
    {
        path: '/noticias/:id/eliminar',
        name: 'news.delete',
        meta: {
            resource: 'NotView',
            roles: ['superadministrador', 'administrador']
        },
    },
    {
        path: '/noticias/:id/suspender',
        name: 'news.suspend',
        meta: {
            resource: 'NotView',
            roles: ['superadministrador', 'administrador', 'moderador']
        },
    },
    {
        path: '/noticias/:id/activar',
        name: 'news.activate',
        meta: {
            resource: 'NotView',
            roles: ['superadministrador', 'administrador', 'moderador']
        },
    }
]

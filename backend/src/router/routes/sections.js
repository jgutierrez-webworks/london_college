export default [
    {
        path: '/secciones',
        name: 'sections.index',
        component: () => import('@/views/sections/IndexSection.vue'),
        meta: {
            pageTitle: 'Secciones',
            breadcrumb: [
                {
                    text: 'Secciones',
                    active: true,
                },
            ],
            roles: ['superadministrador', 'administrador']
        },
    },
    {
        path: '/secciones/nuevo',
        name: 'sections.create',
        meta: {
            resource: 'NotView',
            roles: ['superadministrador', 'administrador']
        },
    },
    {
        path: '/secciones/:id/editar',
        name: 'sections.edit',
        meta: {
            resource: 'NotView',
            roles: ['superadministrador', 'administrador']
        },
    },
    {
        path: '/secciones/:id/eliminar',
        name: 'sections.delete',
        meta: {
            resource: 'NotView',
            roles: ['superadministrador', 'administrador']
        },
    },
    {
        path: '/secciones/:id/suspender',
        name: 'sections.suspend',
        meta: {
            resource: 'NotView',
            roles: ['superadministrador', 'administrador']
        },
    },
    {
        path: '/secciones/:id/activar',
        name: 'sections.activate',
        meta: {
            resource: 'NotView',
            roles: ['superadministrador', 'administrador']
        },
    }
]

export default [
    {
        path: '/error-404',
        name: 'errors.404',
        component: () => import('@/views/errors/404.vue'),
        meta: {
            layout: 'full',
            resource: 'Auth'
        },
    },
    {
        path: '/error-403',
        name: 'errors.403',
        component: () => import('@/views/errors/403.vue'),
        meta: {
            layout: 'full',
            resource: 'Auth'
        },
    }
]
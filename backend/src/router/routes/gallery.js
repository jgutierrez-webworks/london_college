export default [
    {
        path: '/galeria',
        name: 'gallery.index',
        component: () => import('@/views/gallery/IndexGallery.vue'),
        meta: {
            pageTitle: 'Galerias',
            breadcrumb: [
                {
                    text: 'Galerias',
                    active: true,
                },
            ],
            roles: ['superadministrador', 'administrador', 'moderador'],
            resource: 'CheckCustomGalleryPermission'
        },
    },
    {
        path: '/galeria/nuevo',
        name: 'gallery.create',
        component: () => import('@/views/gallery/CreateGallery.vue'),
        meta: {
            pageTitle: 'Crear Galería',
            breadcrumb: [
                {
                    text: 'Galerias',
                    to: { name: 'gallery.index' }
                },
                {
                    text: 'Nuevo',
                    active: true,
                },
            ],
            roles: ['superadministrador', 'administrador', 'moderador'],
            resource: 'CheckCustomGalleryPermission'
        },
    },
    {
        path: '/galeria/:id/editar',
        name: 'gallery.edit',
        component: () => import('@/views/gallery/EditGallery.vue'),
        meta: {
            pageTitle: 'Editar Galería',
            breadcrumb: [
                {
                    text: 'Galerias',
                    to: { name: 'gallery.index' }
                },
                {
                    text: 'Editar',
                    active: true,
                },
            ],
            roles: ['superadministrador', 'administrador', 'moderador'],
            resource: 'CheckCustomGalleryPermission'
        },
    },
    {
        path: '/galeria/:id/eliminar',
        name: 'gallery.delete',
        meta: {
            resource: 'NotView',
            roles: ['superadministrador', 'administrador']
        },
    },
    {
        path: '/galeria/:id/suspender',
        name: 'gallery.suspend',
        meta: {
            resource: 'NotView',
            roles: ['superadministrador', 'administrador', 'moderador']
        },
    },
    {
        path: '/galeria/:id/activar',
        name: 'gallery.activate',
        meta: {
            resource: 'NotView',
            roles: ['superadministrador', 'administrador', 'moderador']
        },
    },
    {
        path: '/galeria/:gallery/imagenes',
        name: 'gallery.images.index',
        component: () => import('@/views/gallery/IndexImagesGallery'),
        meta: {
            pageTitle: 'Galerias',
            breadcrumb: [
                {
                    text: 'Galerias',
                    to: { name: 'gallery.index' }
                },
                {
                    text: 'Imágenes',
                    active: true,
                },
            ],
            roles: ['superadministrador', 'administrador', 'moderador'],
            resource: 'CheckCustomGalleryPermission'
        },
    },
    {
        path: '/galeria/:gallery/imagenes/nuevo',
        name: 'gallery.images.create',
        meta: {
            resource: 'NotView',
            roles: ['superadministrador', 'administrador']
        },
    },
    {
        path: '/galeria/:gallery/imagenes/:id/editar',
        name: 'gallery.images.edit',
        meta: {
            resource: 'NotView',
            roles: ['superadministrador', 'administrador']
        },
    },
    {
        path: '/galeria/:gallery/imagenes/:id/eliminar',
        name: 'gallery.images.delete',
        meta: {
            resource: 'NotView',
            roles: ['superadministrador', 'administrador']
        },
    },
]
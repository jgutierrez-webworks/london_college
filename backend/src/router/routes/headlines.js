export default [
    {
        path: '/titulares',
        name: 'headlines.index',
        meta: {
            resource: 'NotView',
            roles: ['superadministrador', 'administrador']
        },
    },
    {
        path: '/titulares/nuevo',
        name: 'headlines.create',
        meta: {
            resource: 'NotView',
            roles: ['superadministrador', 'administrador']
        },
    },
    {
        path: '/titulares/:id/editar',
        name: 'headlines.edit',
        meta: {
            resource: 'NotView',
            roles: ['superadministrador', 'administrador']
        },
    },
    {
        path: '/titulares/:id/eliminar',
        name: 'headlines.delete',
        meta: {
            resource: 'NotView',
            roles: ['superadministrador', 'administrador']
        },
    },
    {
        path: '/titulares/:id/suspender',
        name: 'headlines.suspend',
        meta: {
            resource: 'NotView',
            roles: ['superadministrador', 'administrador']
        },
    },
    {
        path: '/titulares/:id/activar',
        name: 'headlines.activate',
        meta: {
            resource: 'NotView',
            roles: ['superadministrador', 'administrador']
        },
    }
]
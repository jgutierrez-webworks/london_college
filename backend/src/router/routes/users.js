export default [
    {
        path: '/usuarios',
        name: 'users.index',
        component: () => import('@/views/users/IndexUser.vue'),
        meta: {
            pageTitle: 'Usuarios',
            breadcrumb: [
                {
                    text: 'Usuarios',
                    active: true,
                },
            ],
            roles: ['superadministrador', 'administrador']
        },
    },
    {
        path: '/usuarios/nuevo',
        name: 'users.create',
        component: () => import('@/views/users/CreateUser.vue'),
        meta: {
            pageTitle: 'Crear Usuario',
            breadcrumb: [
                {
                    text: 'Usuarios',
                    to: { name: 'users.index' }
                },
                {
                    text: 'Nuevo',
                    active: true,
                },
            ],
            roles: ['superadministrador', 'administrador']
        },
    },
    {
        path: '/usuarios/:id/editar',
        name: 'users.edit',
        component: () => import('@/views/users/EditUser.vue'),
        meta: {
            pageTitle: 'Editar Usuario',
            breadcrumb: [
                {
                    text: 'Usuarios',
                    to: { name: 'users.index' }
                },
                {
                    text: 'Editar',
                    active: true,
                },
            ],
            roles: ['superadministrador', 'administrador']
        },
    },
    {
        path: '/usuarios/:id/eliminar',
        name: 'users.delete',
        meta: {
            resource: 'NotView',
            roles: ['superadministrador', 'administrador']
        },
    },
    {
        path: '/usuarios/:id/suspender',
        name: 'users.suspend',
        meta: {
            resource: 'NotView',
            roles: ['superadministrador', 'administrador']
        },
    },
    {
        path: '/usuarios/:id/activar',
        name: 'users.activate',
        meta: {
            resource: 'NotView',
            roles: ['superadministrador', 'administrador']
        },
    }
]

export default [
    {
        path: '/login',
        name: 'auth.login',
        component: () => import('@/views/auth/Login.vue'),
        meta: {
            layout: 'full',
            resource: 'Auth',
            redirectIfLoggedIn: true,
        },
    },
    {
        path: '/forgot-password',
        name: 'auth.forgot-password',
        component: () => import('@/views/auth/ForgotPassword.vue'),
        meta: {
            layout: 'full',
            resource: 'Auth',
            redirectIfLoggedIn: true,
        },
    },
    {
        path: '/reset-password/:token',
        name: 'auth.reset-password',
        component: () => import('@/views/auth/ResetPassword.vue'),
        meta: {
            layout: 'full',
            resource: 'Auth',
            redirectIfLoggedIn: true,
        },
    }
]
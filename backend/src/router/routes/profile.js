export default [
    {
        path: '/perfil',
        name: 'profile.index',
        component: () => import('@/views/profile/AccountSetting.vue'),
        meta: {
            pageTitle: 'Perfil',
            breadcrumb: [
                {
                    text: 'Perfil',
                    active: true,
                },
            ],
            roles: ['superadministrador', 'administrador', 'moderador']
        },
    }
]

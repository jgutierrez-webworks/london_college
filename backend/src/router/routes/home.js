export default [
    {
        path: '/home',
        name: 'home.index',
        component: () => import('@/views/home/IndexHome.vue'),
        meta: {
            pageTitle: 'Dashboard',
            breadcrumb: [
                {
                    text: 'Dashboard',
                    active: true,
                },
            ],
            roles: ['superadministrador', 'administrador', 'moderador']
        },
    }
]
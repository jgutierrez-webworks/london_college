export default [
    {
        path: '/banners',
        name: 'banners.index',
        component: () => import('@/views/banners/IndexBanner.vue'),
        meta: {
            pageTitle: 'Banners',
            breadcrumb: [
                {
                    text: 'Banners',
                    active: true,
                },
            ],
            roles: ['superadministrador', 'administrador', 'moderador'],
            resource: 'CheckCustomBannersPermission'
        },
    },
    {
        path: '/banners/nuevo',
        name: 'banners.create',
        component: () => import('@/views/banners/CreateBanner.vue'),
        meta: {
            pageTitle: 'Crear Banner',
            breadcrumb: [
                {
                    text: 'Banners',
                    to: { name: 'banners.index' }
                },
                {
                    text: 'Nuevo',
                    active: true,
                },
            ],
            roles: ['superadministrador', 'administrador', 'moderador'],
            resource: 'CheckCustomBannersPermission'
        },
    },
    {
        path: '/banners/:id/editar',
        name: 'banners.edit',
        component: () => import('@/views/banners/EditBanner.vue'),
        meta: {
            pageTitle: 'Editar Banner',
            breadcrumb: [
                {
                    text: 'Banners',
                    to: { name: 'banners.index' }
                },
                {
                    text: 'Editar',
                    active: true,
                },
            ],
            roles: ['superadministrador', 'administrador', 'moderador'],
            resource: 'CheckCustomBannersPermission'
        },
    },
    {
        path: '/banners/:id/eliminar',
        name: 'banners.delete',
        meta: {
            resource: 'NotView',
            roles: ['superadministrador', 'administrador']
        },
    },
    {
        path: '/banners/:id/suspender',
        name: 'banners.suspend',
        meta: {
            resource: 'NotView',
            roles: ['superadministrador', 'administrador', 'moderador']
        },
    },
    {
        path: '/banners/:id/activar',
        name: 'banners.activate',
        meta: {
            resource: 'NotView',
            roles: ['superadministrador', 'administrador', 'moderador']
        },
    }
]

export default [
    {
        path: '/categorias',
        name: 'categories.index',
        component: () => import('@/views/categories/IndexCategory.vue'),
        meta: {
            pageTitle: 'Categorías',
            breadcrumb: [
                {
                    text: 'Categorías',
                    active: true,
                },
            ],
            roles: ['superadministrador', 'administrador']
        },
    },
    {
        path: '/categorias/nuevo',
        name: 'categories.create',
        meta: {
            resource: 'NotView',
            roles: ['superadministrador', 'administrador']
        },
    },
    {
        path: '/categorias/:id/editar',
        name: 'categories.edit',
        meta: {
            resource: 'NotView',
            roles: ['superadministrador', 'administrador']
        },
    },
    {
        path: '/categorias/:id/eliminar',
        name: 'categories.delete',
        meta: {
            resource: 'NotView',
            roles: ['superadministrador', 'administrador']
        },
    },
    {
        path: '/categorias/:id/suspender',
        name: 'categories.suspend',
        meta: {
            resource: 'NotView',
            roles: ['superadministrador', 'administrador']
        },
    },
    {
        path: '/categorias/:id/activar',
        name: 'categories.activate',
        meta: {
            resource: 'NotView',
            roles: ['superadministrador', 'administrador']
        },
    }
]

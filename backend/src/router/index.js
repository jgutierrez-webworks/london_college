import Vue from 'vue'
import VueRouter from 'vue-router'

import { isUserLoggedIn, getUserData, getHomeRouteForLoggedInUser } from '@/auth/utils'

import auth from './routes/auth'
import banner from './routes/banners'
import categories from './routes/categories'
import errors from './routes/errors'
import gallery from './routes/gallery'
import headlines from './routes/headlines'
import home from './routes/home'
import news from './routes/news'
import profile from './routes/profile'
import sections from './routes/sections'
import users from './routes/users'

Vue.use(VueRouter)

const router = new VueRouter({
	mode: 'history',
	base: '/intranet',
	scrollBehavior() {
		return { x: 0, y: 0 }
	},
	routes: [
		...auth,
		...banner,
		...categories,
		...sections,
		...errors,
		...gallery,
		...headlines,
		...home,
		...news,
		...profile,
		...sections,
		...users,
		{
			path: '*',
			redirect: 'error-404',
		},
	],
})

router.beforeEach((to, _, next) => {
	const isLoggedIn = isUserLoggedIn()
	const userData = getUserData()

	if (to.meta.resource === 'NotView') {
		next(getHomeRouteForLoggedInUser(userData ? userData.role : null))
	}

	if (to.meta.resource !== 'Auth') {
		if (userData && (to.meta.roles === undefined || !to.meta.roles.includes(userData.role))) {
			// Redirect to login if not logged in*/
			if (!isLoggedIn) return next({ name: 'auth.login' })

			if (isLoggedIn) {
				return next({ name: 'errors.403' })
			}
		}
	}

	if (to.meta.resource === 'CheckCustomBannersPermission') {
		if (userData && userData.role === 'moderador' && !userData.can_access_banners) {
			return next({ name: 'errors.403' })
		}
	}

	if (to.meta.resource === 'CheckCustomGalleryPermission') {
		if (userData && userData.role === 'moderador' && !userData.can_access_gallery) {
			return next({ name: 'errors.403' })			
		}
	}

	// Redirect if logged in
	if (to.meta.redirectIfLoggedIn && isLoggedIn) {
		next(getHomeRouteForLoggedInUser(userData ? userData.role : null))
	}

	return next()
})

export default router
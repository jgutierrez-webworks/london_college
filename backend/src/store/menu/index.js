import administrators from '@/navigation/menu/administrator'
import moderators from '@/navigation/menu/moderator'

const storageMenu = JSON.parse(localStorage.getItem('menu'))
const initialMenu = storageMenu
    ? { items: storageMenu }
    : { items: [] }

export default {
    namespaced: true,
    state: initialMenu,
    actions: {
        load({ commit }, user) {
            let menu = []

            if (user !== null) {
                if (user.role === 'superadministrador' || user.role === 'administrador') {
                    menu = administrators
                }
                else if (user.role === 'moderador') {
                    menu = moderators
                }
            }

            localStorage.setItem('menu', JSON.stringify(menu))

            commit('LOAD_MENU', menu)
        },
        destroy({ commit }) {
            localStorage.removeItem('menu')
            commit('DESTROY_MENU')
        },
    },
    mutations: {
        LOAD_MENU(state, menu) {
            state.items = menu
        },
        DESTROY_MENU(state) {
            state.items = []
        }
    }
}

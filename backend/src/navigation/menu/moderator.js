export default [
    {
        title: 'Dashboard',
        route: 'home.index',
        routes: ['home.index'],
        icon: 'HomeIcon',
    },
    {
        title: 'Banners',
        route: 'banners.index',
        routes: ['banners.index', 'banners.create', 'banners.edit'],
        icon: 'ImageIcon',
    },
    {
        title: 'Noticias',
        route: 'news.index',
        routes: ['news.index', 'news.create', 'news.edit'],
        icon: 'BriefcaseIcon',
    },
    {
        title: 'Galería',
        route: 'gallery.index',
        routes: ['gallery.index', 'gallery.create', 'gallery.edit'],
        icon: 'GridIcon',
    },    
]

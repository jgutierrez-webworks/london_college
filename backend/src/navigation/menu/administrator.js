export default [
    {
        title: 'Dashboard',
        route: 'home.index',
        routes: ['home.index'],
        icon: 'HomeIcon',
    },
    {
        title: 'Banners',
        route: 'banners.index',
        routes: ['banners.index', 'banners.create', 'banners.edit'],
        icon: 'ImageIcon',
    },    
    {
        title: 'Noticias',
        route: 'news.index',
        routes: ['news.index', 'news.create', 'news.edit'],
        icon: 'BriefcaseIcon',
    },
    {
        title: 'Categorias',
        route: 'categories.index',
        routes: ['categories.index'],
        icon: 'ListIcon',
        class: 'ml-1'
    },
    {
        title: 'Secciones',
        route: 'sections.index',
        routes: ['sections.index'],
        icon: 'NavigationIcon',
        class: 'ml-1'
    },
    {
        title: 'Galería',
        route: 'gallery.index',
        routes: ['gallery.index', 'gallery.create', 'gallery.edit'],
        icon: 'GridIcon',
    },
    {
        title: 'Usuarios',
        route: 'users.index',
        routes: ['users.index', 'users.create', 'users.edit'],
        icon: 'UserIcon',
        class: ''
    },
]

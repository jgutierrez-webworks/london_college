<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class RepositoriesProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {        
        $this->app->bind(\App\Repositories\Banners\BannersRepoI::class, \App\Repositories\Banners\BannersRepo::class);
        $this->app->bind(\App\Repositories\Categories\CategoriesRepoI::class, \App\Repositories\Categories\CategoriesRepo::class);
        $this->app->bind(\App\Repositories\Custom\CustomRepoI::class, \App\Repositories\Custom\CustomRepo::class);        
        $this->app->bind(\App\Repositories\Files\FilesRepoI::class, \App\Repositories\Files\FilesRepo::class);
        $this->app->bind(\App\Repositories\Gallery\GalleryRepoI::class, \App\Repositories\Gallery\GalleryRepo::class);
        $this->app->bind(\App\Repositories\Headlines\HeadlinesRepoI::class, \App\Repositories\Headlines\HeadlinesRepo::class);
        $this->app->bind(\App\Repositories\News\NewsRepoI::class, \App\Repositories\News\NewsRepo::class);
        $this->app->bind(\App\Repositories\Sections\SectionsRepoI::class, \App\Repositories\Sections\SectionsRepo::class);
        $this->app->bind(\App\Repositories\Roles\RolesRepoI::class, \App\Repositories\Roles\RolesRepo::class);
        $this->app->bind(\App\Repositories\Users\UsersRepoI::class, \App\Repositories\Users\UsersRepo::class);
    }
}

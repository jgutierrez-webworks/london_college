<?php

namespace App\Models;

use App\Globals\Constants;
use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class News extends Model
{
    use HasFactory, Sluggable;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'noticia';
    
    protected $fillable = [
        'codigo', 'slug', 'fecha', 'titulo', 'descripcion_corta', 'descripcion', 'user_id', 
        'seccion_id', 'categoria_id', 'banner_archivo_id', 'orden_destacado', 'destacado', 
        'estado', 'tags', 'observaciones'
    ];

    protected $dates = [
        'fecha', 'created_at', 'updated_at', 'deleted_at'
    ];

    public static function boot()
    {
        parent::boot();

        static::deleted(function ($model) {
            $banner = $model->banner;

            if (!empty($banner)) {
                //Delete Original
                Storage::delete('public/noticias/banners/' . $model->codigo . '/' . $banner->nombre);
                //Delete Thumbs
                Storage::delete('public/noticias/banners/' . $model->codigo . '/thumbs-ico/' . $banner->nombre);
                Storage::delete('public/noticias/banners/' . $model->codigo . '/thumbs-min/' . $banner->nombre);
                Storage::delete('public/noticias/banners/' . $model->codigo . '/thumbs-mobile/' . $banner->nombre);

                $banner->delete();
            }
        });
    }

    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable(): array
    {
        return [
            'slug' => [
                'source' => 'titulo'
            ]
        ];
    }

    public function user()
    {
        return $this->belongsTo(\App\Models\User::class, 'user_id');
    }

    public function section()
    {
        return $this->belongsTo(\App\Models\Section::class, 'seccion_id');
    }

    public function category()
    {
        return $this->belongsTo(\App\Models\Category::class, 'categoria_id');
    }

    public function banner()
    {
        return $this->belongsTo(\App\Models\File::class, 'banner_archivo_id');
    }

    /**
     * Generate a unique news code
     *
     * @return String
     */
    public function generateCode(): String
    {
        /** Generate Client Code */
        $flag = false;

        while (!$flag) {
            $code = Str::random(20);

            if ($this->where('codigo', $code)->count() == 0) {
                $flag = true;
            }
        }

        return $code;
    }
}
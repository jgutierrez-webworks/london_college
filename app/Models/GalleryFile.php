<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class GalleryFile extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'evento_archivo';

    protected $fillable = [
        'evento_id', 'archivo_id'
    ];

    public function gallery()
    {
        return $this->belongsTo(\App\Models\Gallery::class, 'galeria_id');
    }

    public function file()
    {
        return $this->belongsTo(\App\Models\File::class, 'archivo_id');
    }
}

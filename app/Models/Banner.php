<?php

namespace App\Models;

use App\Globals\Functions;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class Banner extends Model
{
    use HasFactory;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'banner';

    protected $fillable = [
        'codigo', 'orden', 'archivo_id', 'archivo_mobile_id' ,'titulo_principal', 
        'titulo_secundario', 'link_redirect_activo', 'url_link_redirect', 'estado'
    ];

    protected $dates = [
        'created_at', 'updated_at', 'deleted_at'
    ];

    public static function boot()
    {
        parent::boot();

        static::deleted(function ($model) {
            $model->load('file', 'fileMobile');

            $file = $model->file;
            $fileMobile = $model->fileMobile;

            if (!empty($file)) {
                //Delete Original
                Storage::delete('public/banners/' . $model->codigo . '/' . $file->nombre);

                if (Functions::isImageExtension($file->extension)) {
                    //Delete Thumbs
                    Storage::delete('public/banners/' . $model->codigo . '/thumbs-ico/' . $file->nombre);
                    Storage::delete('public/banners/' . $model->codigo . '/thumbs-min/' . $file->nombre);
                    Storage::delete('public/banners/' . $model->codigo . '/thumbs-mobile/' . $file->nombre);
                }

                $file->delete();
            }

            if (!empty($fileMobile)) {
                //Delete Original
                Storage::delete('public/banners/' . $model->codigo . '/mobile/' . $fileMobile->nombre);                

                $fileMobile->delete();
            }
        });
    }

    public function file()
    {
        return $this->belongsTo(\App\Models\File::class, 'archivo_id');
    }

    public function fileMobile()
    {
        return $this->belongsTo(\App\Models\File::class, 'archivo_mobile_id');
    }

    /**
     * Generate a unique news code
     *
     * @return String
     */
    public function generateCode(): String
    {
        /** Generate Client Code */
        $flag = false;

        while (!$flag) {
            $code = Str::random(20);

            if ($this->where('codigo', $code)->count() == 0) {
                $flag = true;
            }
        }

        return $code;
    }
}

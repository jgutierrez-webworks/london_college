<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class File extends Model
{
    use HasFactory;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'archivo';

    protected $fillable = [
        'titulo', 'nombre', 'nombre_original', 'extension'
    ];

    protected $dates = [
        'created_at', 'updated_at', 'deleted_at'
    ];

    public function galleries()
    {
        return $this->belongsToMany(\App\Models\Event::class, 'evento_archivo', 'archivo_id', 'evento_id');
    }
}

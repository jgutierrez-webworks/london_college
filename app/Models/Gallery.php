<?php

namespace App\Models;

use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Gallery extends Model
{
    use HasFactory, Sluggable;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'galeria';

    protected $fillable = [
        'codigo', 'slug', 'nombre', 'descripcion', 'fecha', 'lugar', 'destacado', 'estado', 'observaciones'
    ];

    protected $dates = [
        'fecha', 'created_at', 'updated_at', 'deleted_at'
    ];

    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable(): array
    {
        return [
            'slug' => [
                'source' => 'nombre'
            ]
        ];
    }

    public function files()
    {
        return $this->belongsToMany(\App\Models\File::class, 'galeria_archivo', 'galeria_id', 'archivo_id');
    }

    /**
     * Generate a unique gallery code
     *
     * @return String
     */
    public function generateCode(): String
    {
        /** Generate Client Code */
        $flag = false;

        while (!$flag) {
            $code = Str::random(20);

            if ($this->where('codigo', $code)->count() == 0) {
                $flag = true;
            }
        }

        return $code;
    }
}

<?php

namespace App\Repositories\News;

use Exception;
use App\Globals\Constants;
use App\Helpers\ImageResizer;
use App\Models\News;
use App\Repositories\News\NewsRepoI;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Inquiloper\BaseRepository\BaseRepository;

class NewsRepo extends BaseRepository implements NewsRepoI
{
    public function __construct(News $model)
    {
        parent::__construct($model);    
    }

    /**
     * Get All News
     *
     * @return Collection
     */
    public function getAllNews(): Collection
    {
        return $this->model->when(auth()->check() && auth()->user()->roles->first()->name === Constants::NAME_ROLE_MODERATOR, function($query) { 
            $query->where('user_id', auth()->user()->id);
        })->orderBy('id', 'DESC')->with(['user', 'section', 'category', 'banner'])->get();
    }
    
    /**
     * Get All News By Some Filter
     *
     * @param Array $data
     * @return Collection
     */
    public function getNewsByFilters(Array $data = []): Collection
    {
        $query = $this->model->query();
        $query->select('noticia.*');

        /** Filter By Search */
        $query->when(isset($data['search']), function ($q) use ($data) {
            return $q->where('titulo', 'like', '%' . $data['search'] . '%')->orWhere('tags', 'like', '%' . $data['search'] . '%');
        });

        /** Filter By Section */
        $query->when(isset($data['section']), function ($q) use ($data) {
            return $q->where('seccion_id', '=', function ($q) use ($data) {
                $q->from('seccion')->select('id')->where('slug', $data['section']);
            });
        });

        /** Filter By Category */
        $query->when(isset($data['category']), function ($q) use ($data) {
            return $q->where('categoria_id', '=', function ($q) use ($data) {
                $q->from('categoria')->select('id')->where('slug', '=', $data['category']);
            });
        });

        /** Filter By Tags */
        $query->when(isset($data['tags']), function ($q) use ($data) {
            return $q->where('tags', 'like', '%' . $data['tags'] . '%');
        });        

        $query->where('estado', 1);        
        $query->orderBy('slug', 'ASC');
        $query->with(['user', 'section', 'category', 'banner']);

        return $query->get();
    }

    /**
     * Get All Recent News
     *
     * @return Collection
     */
    public function getRecentNews(): Collection
    {
        return $this->model->where('estado', 1)
            ->orderBy('id', 'ASC')
            ->with(['user', 'section', 'category', 'banner'])
            ->take(Constants::LIMIT_LATEST_ROWS_TABLE)
            ->get();
    }

    /**
     * Get All Featured News
     *
     * @return Collection
     */
    public function getFeaturedNews(): Collection
    {
        return $this->model->where('estado', 1)
            ->where('destacado', 1)
            ->orderBy('orden_destacado', 'ASC')
            ->with(['user', 'section', 'category', 'banner'])
            ->take(Constants::LIMIT_LATEST_ROWS_TABLE)
            ->get();
    }

    /**
     * Get Related News By a slug
     *
     * @param String $slug
     * @return Collection
     */
    public function getRelatedNewsBySlug(String $slug): Collection
    {
        $news = $this->model->where('slug', $slug)->firstOrFail();

        return $this->model->where('id', '<>', $news->id)
            ->where('seccion_id', $news->seccion_id)
            ->where('estado', 1)
            ->orderBy('slug', 'ASC')
            ->with(['user', 'section', 'category', 'banner'])
            ->take(Constants::LIMIT_LATEST_ROWS_TABLE)
            ->get();
    }

    /**
     * Get Full Info News Item
     *
     * @param String $slug
     * @return News
     */
    public function getFullNewsInfo(String $slug): News
    {
        return $this->model->where('slug', $slug)->with(['user', 'section', 'category', 'banner'])->firstOrFail();
    }

    /**
     * Get count of active news
     *
     * @return Int
     */
    public function getTotalNewsActive(): Int
    {
        return $this->model->where('estado', 1)->count();
    }

    /**
     * Store a news with files
     *
     * @param Array $data
     * @param UploadedFile $fileBanner
     * @return News
     */
    public function store(Array $data, UploadedFile $fileBanner): News
    {
        try {

            DB::beginTransaction();

            //First store a news
            $data['codigo'] = $this->model->generateCode();
            $data['user_id'] = auth()->user()->id;
            $news = $this->model->create($data);

            //Then store and asing de banner image
            $fileBanner->store('noticias/banners/' . $news->codigo . '/', 'public');

            ImageResizer::setImageResizer('/storage/noticias/banners/', '/public/noticias/banners/', $news->codigo, $fileBanner, ImageResizer::CODE_FLAG_IMAGE_RESIZER_BANNER);

            $dataFileBanner = [
                'nombre' => $fileBanner->hashName(),
                'nombre_original' => $fileBanner->getClientOriginalName(),
                'extension' => $fileBanner->getClientOriginalExtension()
            ];

            $fileBanner =  $this->model->banner()->create($dataFileBanner);
            
            $news->banner_archivo_id = $fileBanner->id;
            $news->save();

            DB::commit();
            return $news;
            
        } catch (Exception $e) {
            DB::rollback();
            throw $e;
        }
    }

    /**
     * Update a news with his files
     *
     * @param Int $id
     * @param Array $data
     * @param UploadedFile $fileBanner | null
     * @return News
     */
    public function update(Int $id, Array $data, UploadedFile $fileBanner = null): News
    {
        try {

            DB::beginTransaction();

            $news = $this->model->find($id);
            $news->load(['banner']);

            if (!is_null($fileBanner)) {
                $banner = $news->banner;
                $fileBanner->store('noticias/banners/' . $news->codigo . '/', 'public');

                ImageResizer::setImageResizer('/storage/noticias/banners/', '/public/noticias/banners/', $news->codigo, $fileBanner, ImageResizer::CODE_FLAG_IMAGE_RESIZER_BANNER);

                $dataFileBanner = [
                    'nombre' => $fileBanner->hashName(),
                    'nombre_original' => $fileBanner->getClientOriginalName(),
                    'extension' => $fileBanner->getClientOriginalExtension()
                ];

                if (!empty($banner)) {
                    //Delete Original
                    Storage::delete('public/eventos/banners/' . $news->codigo . '/' . $banner->nombre);
                    //Delete Thumbs
                    Storage::delete('public/eventos/banners/' . $news->codigo . '/thumbs-ico/' . $banner->nombre);
                    Storage::delete('public/eventos/banners/' . $news->codigo . '/thumbs-min/' . $banner->nombre);
                    Storage::delete('public/eventos/banners/' . $news->codigo . '/thumbs-mobile/' . $banner->nombre);

                    $banner->update($dataFileBanner);

                } else {
                    $banner = $this->model->banner()->create($dataFileBanner);
                    $data['banner_archivo_id'] = $banner->id;
                }
            }

            $news->update($data);

            DB::commit();
            return $news;

        } catch (Exception $e) {
            DB::rollback();
            throw $e;
        }
    }

    /**
     * Delete a event
     *
     * @param Int $id
     * @return Bool
     */
    public function delete(Int $id): Bool
    {
        try {

            DB::beginTransaction();

            $this->model->findOrFail($id)->delete();

            DB::commit();
            return true;
        } catch (Exception $e) {
            DB::rollback();
            throw $e;
        }
    }

    /**
     * Suspend a news
     *
     * @param Int $id
     * @return Bool
     */
    public function suspend(Int $id): Bool
    {
        try {

            DB::beginTransaction();

            $this->model->where('id', $id)->update([
                'estado' => 0
            ]);

            DB::commit();

            return true;
        } catch (Exception $e) {
            DB::rollback();
            throw $e;
        }
    }

    /**
     * Activate a news
     *
     * @param Int $id
     * @return Bool
     */
    public function activate(Int $id): Bool
    {
        try {

            DB::beginTransaction();

            $this->model->where('id', $id)->update([
                'estado' => 1
            ]);

            DB::commit();

            return true;
        } catch (Exception $e) {
            DB::rollback();
            throw $e;
        }
    }
}
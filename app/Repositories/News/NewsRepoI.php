<?php

namespace App\Repositories\News;

use App\Models\News;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\UploadedFile;

interface NewsRepoI
{
    /**
     * Get All News
     *
     * @return Collection
     */
    public function getAllNews(): Collection;

    /**
     * Get All News By Some Filter
     *
     * @param Array $data
     * @return Collection
     */
    public function getNewsByFilters(Array $data = []): Collection;

    /**
     * Get All Recent News
     *
     * @return Collection
     */
    public function getRecentNews(): Collection;
    
    /**
     * Get All Featured News
     *
     * @return Collection
     */
    public function getFeaturedNews(): Collection;
    
    /**
     * Get Related News By a slug
     *
     * @param String $slug
     * @return Collection
     */
    public function getRelatedNewsBySlug(String $slug): Collection;

    /**
     * Get Full Info News Item
     *
     * @param String $slug
     * @return News
     */
    public function getFullNewsInfo(String $slug): News;

    /**
     * Get count of active news
     *
     * @return Int
     */
    public function getTotalNewsActive(): Int;

    /**
     * Store a news with files
     *
     * @param Array $data
     * @param UploadedFile $fileBanner
     * @return News
     */
    public function store(Array $data, UploadedFile $fileBanner): News;

    /**
     * Update a news with his files
     *
     * @param Int $id
     * @param Array $data
     * @param UploadedFile $fileBanner | null
     * @return News
     */
    public function update(Int $id, Array $data, UploadedFile $fileBanner = null): News;

    /**
     * Delete a event
     *
     * @param Int $id
     * @return Bool
     */
    public function delete(Int $id): Bool;

    /**
     * Suspend a news
     *
     * @param Int $id
     * @return Bool
     */
    public function suspend(Int $id): Bool;

    /**
     * Activate a news
     *
     * @param Int $id
     * @return Bool
     */
    public function activate(Int $id): Bool;
}
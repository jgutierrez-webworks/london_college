<?php

namespace App\Repositories\Headlines;

use Exception;
use App\Globals\Constants;
use App\Models\Headline;
use App\Repositories\Headlines\HeadlinesRepoI;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\DB;
use Inquiloper\BaseRepository\BaseRepository;

class HeadlinesRepo extends BaseRepository implements HeadlinesRepoI
{
    public function __construct(Headline $model)
    {
        parent::__construct($model);
    }

    /**
     * Get All Headlines
     *
     * @return Collection
     */
    public function getAllHeadlines(): Collection
    {
        return $this->model->orderBy('id', 'DESC')->get();
    }

    /**
     * Get All Headlines
     *
     * @return Collection
     */
    public function getAllActiveHeadlines(): Collection
    {
        return $this->model->where('estado', 1)->orderBy('id', 'DESC')->get();
    }

    /**
     * Suspend a headline
     *
     * @param Int $id
     * @return Bool
     */
    public function suspend(Int $id): Bool
    {
        try {

            DB::beginTransaction();

            $this->model->where('id', $id)->update([
                'estado' => 0
            ]);

            DB::commit();
            return true;
            
        } catch (Exception $e) {
            DB::rollback();
            throw $e;
        }
    }

    /**
     * Activate a headline
     *
     * @param Int $id
     * @return Bool
     */
    public function activate(Int $id): Bool
    {
        try {

            DB::beginTransaction();

            $this->model->where('id', $id)->update([
                'estado' => 1
            ]);

            DB::commit();
            return true;

        } catch (Exception $e) {
            DB::rollback();
            throw $e;
        }
    }
}
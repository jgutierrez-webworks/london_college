<?php

namespace App\Repositories\Headlines;

use Illuminate\Database\Eloquent\Collection;

interface HeadlinesRepoI
{
    /**
     * Get All Headlines
     *
     * @return Collection
     */
    public function getAllHeadlines(): Collection;

    /**
     * Get All Headlines
     *
     * @return Collection
     */
    public function getAllActiveHeadlines(): Collection;

    /**
     * Suspend a headline
     *
     * @param Int $id
     * @return Bool
     */
    public function suspend(Int $id): Bool;

    /**
     * Activate a headline
     *
     * @param Int $id
     * @return Bool
     */
    public function activate(Int $id): Bool;
}
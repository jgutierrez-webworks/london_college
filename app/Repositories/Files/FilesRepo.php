<?php

namespace App\Repositories\Files;

use Exception;
use App\Models\File;
use App\Globals\Constants;
use App\Repositories\Files\FilesRepoI;
use Inquiloper\BaseRepository\BaseRepository;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class FilesRepo extends BaseRepository implements FilesRepoI
{
    public function __construct(File $model)
    {
        parent::__construct($model);
    }
}
<?php

namespace App\Repositories\Banners;

use Exception;
use App\Globals\Constants;
use App\Globals\Functions;
use App\Helpers\ImageResizer;
use App\Models\Banner;
use App\Repositories\Banners\BannersRepoI;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Inquiloper\BaseRepository\BaseRepository;

class BannersRepo extends BaseRepository implements BannersRepoI
{
    public function __construct(Banner $model)
    {
        parent::__construct($model);
    }

    /**
     * Get All Banners
     *
     * @return Collection
     */
    public function getAllBanners(): Collection
    {
        return $this->model->orderBy('orden', 'ASC')->with(['file', 'fileMobile'])->get();
    }

    /**
     * Get All Active Banners
     *
     * @return Collection
     */
    public function getAllActiveBanners(): Collection
    {
        return $this-> model->where('estado', 1)->orderBy('orden', 'ASC')->with(['file', 'fileMobile'])->get();
    }

    /**
     * Get count of active banners
     *
     * @return Int
     */
    public function getTotalBannersActive(): Int
    {
        return $this->model->where('estado', 1)->count();
    }

    /**
     * Store a banner with files
     *
     * @param Array $data
     * @param UploadedFile $file
     * @param UploadedFile $fileMobile
     * 
     * @return Banner
     */
    public function store(array $data, UploadedFile $file, UploadedFile $fileMobile): Banner
    {
        try {

            DB::beginTransaction();

            //First store a banner
            $lastOrden = $this->model->max('orden');
            $data['orden'] = ($lastOrden) ? $lastOrden + 1 : 1;
            $data['codigo'] = $this->model->generateCode();

            $banner = $this->model->create($data);

            //Then store and asing de banner image
            $file->store('banners/' . $banner->codigo . '/', 'public');

            if (Functions::isImageExtension($file->getClientOriginalExtension())) {
                ImageResizer::setImageResizer('/storage/banners/', '/public/banners/', $banner->codigo, $file, ImageResizer::CODE_FLAG_IMAGE_RESIZER_BANNER);
            }

            $dataFile = [
                'nombre'          => $file->hashName(),
                'nombre_original' => $file->getClientOriginalName(),
                'extension'       => $file->getClientOriginalExtension()
            ];

            $file = $this->model->file()->create($dataFile);

            /** File Mobile */
            $fileMobile->store('banners/' . $banner->codigo . '/mobile/', 'public');            

            $dataFileMobile = [
                'nombre'          => $fileMobile->hashName(),
                'nombre_original' => $fileMobile->getClientOriginalName(),
                'extension'       => $fileMobile->getClientOriginalExtension()
            ];

            $fileMobile = $this->model->fileMobile()->create($dataFileMobile);

            $banner->archivo_id = $file->id;
            $banner->archivo_mobile_id = $fileMobile->id;
            $banner->save();

            DB::commit();
            return $banner;

        } catch (Exception $e) {
            DB::rollback();
            throw $e;
        }
    }

    /**
     * Update a banner with his files
     *
     * @param Int $id
     * @param Array $data
     * @param UploadedFile $file | null
     * @param UploadedFile $fileMobile | null
     * 
     * @return Banner
     */
    public function update(Int $id, array $data, UploadedFile $file = null, UploadedFile $fileMobile = null): Banner
    {
        try {

            DB::beginTransaction();

            $banner = $this->model->find($id);
            $banner->load(['file', 'fileMobile']);

            if (!is_null($file)) {
                $fileBanner = $banner->file;
                $file->store('banners/' . $banner->codigo . '/', 'public');

                if(Functions::isImageExtension($file->getClientOriginalExtension())) {
                    ImageResizer::setImageResizer('/storage/banners/', '/public/banners/', $banner->codigo, $file, ImageResizer::CODE_FLAG_IMAGE_RESIZER_BANNER);
                }

                $dataFile = [
                    'nombre'          => $file->hashName(),
                    'nombre_original' => $file->getClientOriginalName(),
                    'extension'       => $file->getClientOriginalExtension()
                ];

                if (!empty($fileBanner)) {                    
                    //Delete Original
                    Storage::delete('public/banners/' . $banner->codigo . '/' . $fileBanner->nombre);

                    if(Functions::isImageExtension($fileBanner->extension)) {
                        //Delete Thumbs
                        Storage::delete('public/banners/' . $banner->codigo . '/thumbs-ico/' . $fileBanner->nombre);
                        Storage::delete('public/banners/' . $banner->codigo . '/thumbs-min/' . $fileBanner->nombre);
                        Storage::delete('public/banners/' . $banner->codigo . '/thumbs-mobile/' . $fileBanner->nombre);
                    }

                    $fileBanner->update($dataFile);
                } else {
                    $fileBanner = $this->model->file()->create($dataFile);
                    $data['archivo_id'] = $fileBanner->id;
                }
            }

            if (!is_null($fileMobile)) {
                $fileBannerMobile = $banner->fileMobile;
                $fileMobile->store('banners/' . $banner->codigo . '/mobile/', 'public');
                
                $dataFileMobile = [
                    'nombre'          => $fileMobile->hashName(),
                    'nombre_original' => $fileMobile->getClientOriginalName(),
                    'extension'       => $fileMobile->getClientOriginalExtension()
                ];

                if (!empty($fileBannerMobile)) { 
                    //Delete Original
                    Storage::delete('public/banners/' . $banner->codigo . '/mobile/' . $fileBannerMobile->nombre);

                    $fileBannerMobile->update($dataFileMobile);
                } else {
                    $fileBannerMobile = $this->model->fileMobile()->create($dataFileMobile);
                    $data['archivo_mobile_id'] = $fileBannerMobile->id;
                }
            }

            $banner->update($data);

            DB::commit();
            return $banner;

        } catch (Exception $e) {
            DB::rollback();
            throw $e;
        }
    }

    /**
     * Delete a event
     *
     * @param Int $id
     * @return Bool
     */
    public function delete(Int $id): Bool
    {
        try {

            DB::beginTransaction();

            $this->model->findOrFail($id)->delete();

            DB::commit();
            return true;

        } catch (Exception $e) {
            DB::rollback();
            throw $e;
        }
    }

    /**
     * Suspend a banner
     *
     * @param Int $id
     * @return Bool
     */
    public function suspend(Int $id): Bool
    {
        try {

            DB::beginTransaction();

            $this->model->where('id', $id)->update([
                'estado' => 0
            ]);

            DB::commit();

            return true;
        } catch (Exception $e) {
            DB::rollback();
            throw $e;
        }
    }

    /**
     * Activate a banner
     *
     * @param Int $id
     * @return Bool
     */
    public function activate(Int $id): Bool
    {
        try {

            DB::beginTransaction();

            $this->model->where('id', $id)->update([
                'estado' => 1
            ]);

            DB::commit();

            return true;
        } catch (Exception $e) {
            DB::rollback();
            throw $e;
        }
    }

    /**
     * Up Order of a event
     *
     * @param Int $id
     * @return Bool
     */
    public function upOrder(Int $id): Bool
    {
        try {

            DB::beginTransaction();

            $banner = $this->model->find($id);

            //Decrement next order
            $this->model->where('orden', ($banner->orden + 1))->update([
                'orden' => $banner->orden
            ]);

            //Up orden selected
            $banner->orden = ($banner->orden + 1);
            $banner->save();

            DB::commit();
            return true;

        } catch (Exception $e) {
            DB::rollback();
            throw $e;
        }
    }

    /**
     * Down Order of a event
     *
     * @param Int $id
     * @return Bool
     */
    public function downOrder(Int $id): Bool
    {
        try {

            DB::beginTransaction();

            $banner = $this->model->find($id);

            //Increment next order
            $this->model->where('orden', ($banner->orden - 1))->update([
                'orden' => $banner->orden
            ]);

            //Up orden selected
            $banner->orden = ($banner->orden - 1);
            $banner->save();

            DB::commit();
            return true;
            
        } catch (Exception $e) {
            DB::rollback();
            throw $e;
        }
    }
}
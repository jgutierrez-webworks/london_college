<?php

namespace App\Repositories\Banners;

use App\Models\Banner;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\UploadedFile;

interface BannersRepoI
{
    /**
     * Get All Banners
     *
     * @return Collection
     */
    public function getAllBanners(): Collection;

    /**
     * Get All Active Banners
     *
     * @return Collection
     */
    public function getAllActiveBanners(): Collection;

    /**
     * Get count of active banners
     *
     * @return Int
     */
    public function getTotalBannersActive(): Int;

    /**
     * Store a banner with files
     *
     * @param Array $data
     * @param UploadedFile $file
     * @param UploadedFile $fileMobile
     * 
     * @return Banner
     */
    public function store(array $data, UploadedFile $file, UploadedFile $fileMobile): Banner;

    /**
     * Update a banner with his files
     *
     * @param Int $id
     * @param Array $data
     * @param UploadedFile $file | null
     * @param UploadedFile $fileMobile | null
     * 
     * @return Banner
     */
    public function update(Int $id, array $data, UploadedFile $file = null, UploadedFile $fileMobile = null): Banner;

    /**
     * Delete a event
     *
     * @param Int $id
     * @return Bool
     */
    public function delete(Int $id): Bool;

    /**
     * Suspend a banner
     *
     * @param Int $id
     * @return Bool
     */
    public function suspend(Int $id): Bool;


    /**
     * Activate a banner
     *
     * @param Int $id
     * @return Bool
     */
    public function activate(Int $id): Bool;

    /**
     * Up Order of a event
     *
     * @param Int $id
     * @return Bool
     */
    public function upOrder(Int $id): Bool;

    /**
     * Down Order of a event
     *
     * @param Int $id
     * @return Bool
     */
    public function downOrder(Int $id): Bool;
}
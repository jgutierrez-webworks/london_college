<?php

namespace App\Repositories\Custom;

use Illuminate\Database\Eloquent\Collection;

interface CustomRepoI
{
    /**
     * Get Categories
     *
     * @return Collection
     */
    public function getCategories(): Collection;

    /**
     * Get Frequent Categories
     *
     * @return Collection
     */
    public function getFrequentCategories(): Collection;

    /**
     * Get Sections
     *
     * @return Collection
     */
    public function getSections(): Collection;
}
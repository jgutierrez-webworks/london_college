<?php

namespace App\Repositories\Custom;

use Exception;
use App\Globals\Constants;
use App\Models\Category;
use App\Models\Section;
use App\Repositories\Custom\CustomRepoI;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\DB;
use Inquiloper\BaseRepository\BaseRepository;

class CustomRepo extends BaseRepository implements CustomRepoI
{
    protected $categoryModel, $sectionModel;

    public function __construct(Category $categoryModel, Section $sectionModel)
    {
        $this->categoryModel = $categoryModel;
        $this->sectionModel = $sectionModel;
    }

    /**
     * Get Categories
     *
     * @return Collection
     */
    public function getCategories(): Collection
    {
        return $this->categoryModel->when(auth()->check() && auth()->user()->roles->first()->name === Constants::NAME_ROLE_MODERATOR, function($query) {
            $categories = auth()->user()->categories;

            if($categories->isNotEmpty()) {
                $query->whereIn('id', $categories->pluck('id')->toArray());
            }
        })->where('estado', 1)->orderBy('id', 'ASC')->get();
    }

    /**
     * Get Frequent Categories
     *
     * @return Collection
     */
    public function getFrequentCategories(): Collection
    {
        return $this->categoryModel->select(['slug', 'nombre'])
            ->selectRaw('(SELECT count(*) FROM noticia WHERE noticia.categoria_id = categoria.id) AS frecuencia')
            ->orderBy('frecuencia', 'DESC')
            ->get();
    }

    /**
     * Get Sections
     *
     * @return Collection
     */
    public function getSections(): Collection
    {
        return $this->sectionModel->where('estado', 1)->orderBy('id', 'ASC')->get();
    }
}
<?php

namespace App\Repositories\Gallery;

use App\Models\Gallery;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\UploadedFile;

interface GalleryRepoI
{
    /**
     * Get All gallery
     *
     * @return Collection
     */
    public function getAllGalleries(): Collection;

    /**
     * Get All Events by status filter
     *
     * @param Int $status
     * @return Collection
     */
    public function getAllGalleriesByStatus(Int $status): Collection;

    /**
     * Get full event with relations by slug
     *
     * @param String $slug
     * @return \App\Models\Gallery
     */
    public function getFullGalleryInfo(String $slug): Gallery;

    /**
     * Store a gallery
     *
     * @param Array $data
     * @return \App\Models\Gallery
     */
    public function store(Array $data): Gallery;

    /**
     * Update a gallery
     *
     * @param Int $id
     * @param Array $data
     * @return Bool
     */
    public function edit(Int $id, Array $data): Bool;

    /**
     * Delete a gallery
     *
     * @param Int $id
     * @return Bool
     */
    public function delete(Int $id): Bool;

    /**
     * Activate a gallery
     *
     * @param Int $id
     * @return Bool
     */
    public function activate(Int $id): Bool;

    /**
     * Suspend a gallery
     *
     * @param Int $id
     * @return Bool
     */
    public function suspend(Int $id): Bool;

    /**
     * Store a image resource in a specific gallery
     *
     * @param Int $galleryId
     * @param UploadedFile $file
     * @return Bool
     */
    public function storeImage(Int $galleryId, UploadedFile $file): Bool;
/**
     * Delete a gallery image
     *
     * @param Int $galleryId
     * @param Int $fileId
     * @return Bool
     */
    public function deleteImage(Int $galleryId, Int $fileId): Bool;
}
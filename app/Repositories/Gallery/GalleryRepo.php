<?php

namespace App\Repositories\Gallery;

use Exception;
use App\Globals\Constants;
use App\Helpers\ImageResizer;
use App\Models\File;
use App\Models\Gallery;
use App\Repositories\Gallery\GalleryRepoI;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Inquiloper\BaseRepository\BaseRepository;

class GalleryRepo extends BaseRepository implements GalleryRepoI
{
    protected $fileModel;

    public function __construct(File $fileModel, Gallery $model)
    {
        parent::__construct($model);

        $this->fileModel = $fileModel;
    }

    /**
     * Get All gallery
     *
     * @return Collection
     */
    public function getAllGalleries(): Collection
    {
        return $this->model->orderBy('id', 'DESC')->get();
    }
    
    /**
     * Get All Events by status filter
     *
     * @param Int $status
     * @return Collection
     */
    public function getAllGalleriesByStatus(Int $status): Collection
    {
        return $this->model->where('estado', $status)->orderBy('id', 'DESC')->with(['files'])->get();
    }

    /**
     * Get full event with relations by slug
     *
     * @param String $slug
     * @return \App\Models\Gallery
     */
    public function getFullGalleryInfo(String $slug): Gallery
    {
        return $this->model->where('slug', $slug)->with(['files'])->firstOrFail();
    }

    /**
     * Store a gallery
     *
     * @param Array $data
     * @return \App\Models\Gallery
     */
    public function store(Array $data): Gallery
    {
        try {

            DB::beginTransaction();

            $data['codigo'] = $this->model->generateCode();
            $gallery = $this->model->create($data);         

            DB::commit();
            return $gallery;
            
        } catch (Exception $e) {
            DB::rollback();
            throw $e;
        }
    }

    /**
     * Update a gallery
     *
     * @param Int $id
     * @param Array $data
     * @return Bool
     */
    public function edit(Int $id, array $data): Bool
    {
        try {

            DB::beginTransaction();

            $this->model->findOrFail($id)->update($data);

            DB::commit();
            return true;
            
        } catch (Exception $e) {
            DB::rollback();
            throw $e;
        }
    }

    /**
     * Delete a gallery
     *
     * @param Int $id
     * @return Bool
     */
    public function delete(Int $id): Bool
    {
        try {

            DB::beginTransaction();

            $gallery = $this->model->findOrFail($id);
            $gallery->load('files');

            if($gallery->files->isNotEmpty()) {
                foreach ($gallery->files as $file) {
                    //Delete Original
                    Storage::delete('public/galeria/' . $gallery->codigo . '/' . $file->nombre);
                    //Delete Thumbs
                    Storage::delete('public/galeria/' . $gallery->codigo . '/thumbs-ico/' . $file->nombre);
                    Storage::delete('public/galeria/' . $gallery->codigo . '/thumbs-min/' . $file->nombre);
                    Storage::delete('public/galeria/' . $gallery->codigo . '/thumbs-mobile/' . $file->nombre);

                    $gallery->files()->detach($file);
                    $file->delete();
                }
            }

            $gallery->delete();

            DB::commit();
            return true;
            
        } catch (Exception $e) {
            DB::rollback();
            throw $e;
        }
    }

    /**
     * Activate a gallery
     *
     * @param Int $id
     * @return Bool
     */
    public function activate(Int $id): Bool
    {
        try {

            DB::beginTransaction();

            $this->model->where('id', $id)->update([
                'estado' => 1
            ]);

            DB::commit();
            return true;

        } catch (Exception $e) {
            DB::rollback();
            throw $e;
        }
    }

    /**
     * Suspend a gallery
     *
     * @param Int $id
     * @return Bool
     */
    public function suspend(Int $id): Bool
    {
        try {

            DB::beginTransaction();

            $this->model->where('id', $id)->update([
                'estado' => 0
            ]);

            DB::commit();
            return true;

        } catch (Exception $e) {
            DB::rollback();
            throw $e;
        }
    }

    /**
     * Store a image resource in a specific gallery
     *
     * @param Int $galleryId
     * @param UploadedFile $file
     * @return Bool
     */
    public function storeImage(Int $galleryId, UploadedFile $file): Bool
    {
        try {

            DB::beginTransaction();

            $gallery = $this->model->findOrFail($galleryId);

            //Then store and asing de banner image
            $file->store('galeria/' . $gallery->codigo . '/', 'public');

            ImageResizer::setImageResizer('/storage/galeria/', '/public/galeria/', $gallery->codigo, $file, ImageResizer::CODE_FLAG_IMAGE_RESIZER_BANNER);            

            $data = [
                'nombre'          => $file->hashName(),
                'nombre_original' => $file->getClientOriginalName(),
                'extension'       => $file->getClientOriginalExtension()
            ];

            $galleryFile = $this->fileModel->create($data);        
            $gallery->files()->attach($galleryFile);

            DB::commit();
            return true;

        } catch (Exception $e) {
            DB::rollback();
            throw $e;
        }
    }

    /**
     * Delete a gallery image
     *
     * @param Int $galleryId
     * @param Int $fileId
     * @return Bool
     */
    public function deleteImage(Int $galleryId, Int $fileId): Bool
    {
        try {

            $gallery = $this->model->findOrFail($galleryId);
            $file = $this->fileModel->findOrFail($fileId);

            //Delete Original
            Storage::delete('public/galeria/' . $gallery->codigo . '/' . $file->nombre);
            //Delete Thumbs
            Storage::delete('public/galeria/' . $gallery->codigo . '/thumbs-ico/' . $file->nombre);
            Storage::delete('public/galeria/' . $gallery->codigo . '/thumbs-min/' . $file->nombre);
            Storage::delete('public/galeria/' . $gallery->codigo . '/thumbs-mobile/' . $file->nombre);

            $gallery->files()->detach($file);
            $file->delete();

            DB::commit();
            return true;

        } catch (Exception $e) {
            DB::rollback();
            throw $e;
        }
    }
}
<?php

namespace App\Repositories\Users;

use App\Models\User;
use Illuminate\Database\Eloquent\Collection;

interface UsersRepoI
{
	/**
     * Get All Users
     *
     * @return Collection
     */
    public function getAllUsers(): Collection;
	
	/**
     * Get all administrators users
     *
     * @return Collection
     */
    public function getAllAdministrators(): Collection;
	
	/**
     * Get all moderators users
     *
     * @return Collection
     */
    public function getAllModerators(): Collection;
	
	/**
     * Get all clients users
     *
     * @return Collection
     */
    public function getAllClients(): Collection;
	
	/**
     * Get count registered users
     *
     * @return Int
     */
    public function getCountUsers(): Int;

    /**
     * Get count registered users
     *
     * @return Int
     */
    public function getCountActiveUsers(): Int;
	
	/**
     * Get all users by and field and array values
     *
     * @param String $field
     * @param Array $array
     * @return Collection
     */
    public function getUsersByWhereIn(String $field, Array $array): Collection;
	
	/**
     * Create a user
     *
     * @param Array $data
     * @param String $role
     * @return User
     */
    public function createUser(Array $data, String $role, Array $categories = []): User;
	
	/**
     * Update a user by data
     *
     * @param Int $id
     * @param Array $data
     * @param String $role
     * @return Bool
     */
    public function updateUser(Int $id, array $data, String $role, array $categories = []): Bool;

    /**
     * Delete a user
     *
     * @param Int $id
     * @return Bool
     */
    public function deleteUser(Int $id): Bool;

	/**
     * Suspend a user
     *
     * @param Int $id
     * @return Bool
     */
    public function suspend(Int $id): Bool;
	
	/**
     * Activate a user
     *
     * @param Int $id
     * @return Bool
     */
    public function activate(Int $id): Bool;
}
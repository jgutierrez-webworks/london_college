<?php

namespace App\Repositories\Users;

use Exception;
use App\Models\User;
use App\Globals\Constants;
use App\Models\Role;
use App\Repositories\Users\UsersRepoI;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\DB;
use Inquiloper\BaseRepository\BaseRepository;

class UsersRepo extends BaseRepository implements UsersRepoI
{
	protected $roleModel;

    public function __construct(Role $roleModel, User $model)
    {
        parent::__construct($model);

        $this->roleModel = $roleModel;
    }
    
    /**
     * Get All Users
     *
     * @return Collection
     */
    public function getAllUsers(): Collection
    {
    	$users = $this->model->when(auth()->user()->hasRole(Constants::NAME_ROLE_SUPERADMINISTRATOR), function ($q) {
            return $q->where('id', '!=', auth()->user()->id);
        })->when(! auth()->user()->hasRole(Constants::NAME_ROLE_SUPERADMINISTRATOR), function ($q) {
            return $q->whereHas('roles', function ($query) {
                $query->whereNotIn('name', [Constants::NAME_ROLE_SUPERADMINISTRATOR]);
            })->where('id', '!=', auth()->user()->id);
        })->orderBy('id', 'DESC')->with(['roles'])->get();

        return $users;
    }

    /**
     * Get all administrators users
     *
     * @return Collection
     */
    public function getAllAdministrators(): Collection
    {
        return $this->model->whereHas('roles', function($query) {
            $query->where('roles.name', Constants::NAME_ROLE_ADMINISTRATOR);
        })->get();
    }

    /**
     * Get all moderators users
     *
     * @return Collection
     */
    public function getAllModerators(): Collection
    {
        return $this->model->whereHas('roles', function ($query) {
            $query->where('roles.name', Constants::NAME_ROLE_MODERATOR);
        })->get();
    }

    /**
     * Get all clients users
     *
     * @return Collection
     */
    public function getAllClients(): Collection
    {
        return $this->model->whereHas('roles', function($query) {
            $query->where('roles.name', Constants::NAME_ROLE_CLIENT);
        })->get();
    }

    /**
     * Get count registered users
     *
     * @return Int
     */
    public function getCountUsers(): Int
    {
        return $this->model->count();
    }

    /**
     * Get count registered users
     *
     * @return Int
     */
    public function getCountActiveUsers(): Int
    {
        return $this->model->where('status', 1)->count();
    }

    /**
     * Get all users by and field and array values
     *
     * @param String $field
     * @param Array $array
     * @return Collection
     */
    public function getUsersByWhereIn(String $field, Array $array): Collection
    {
        return $this->model->whereIn($field, $array)->get();
    }

    /**
     * Create a user
     *
     * @param Array $data
     * @param String $role
     * @return User
     */
    public function createUser(Array $data, String $role, Array $categories = []): User
    {
        try {

            DB::beginTransaction();
                        
            $roles = $this->roleModel->where('name', $role)->first();

            $user = $this->model->create($data);
            $user->attachRole($roles);

            if(!empty($categories)) {
                $user->categories()->attach($categories);
            }

            DB::commit();

            return $user;

        } catch (Exception $e) {
            DB::rollback();
            return false;   
        }
    }
    
    /**
     * Update a user by data
     *
     * @param Int $id
     * @param Array $data
     * @param String $role
     * @return Bool
     */
    public function updateUser(Int $id, Array $data, String $role, Array $categories = []): Bool
    {
        try {

            DB::beginTransaction();
            
            $user = $this->model->find($id);
            $user->fill($data);
            $user->save();

            $rolUser = $user->roles->first();

            //Si se le cambio el rol al usuario
            if($role !== $rolUser->name)
            {
                $user->detachRole($rolUser);
                $roles = $this->roleModel->where('name', $role)->first();

                $user->attachRole($roles);                
            }

            if (!empty($categories)) {
                $user->load('categories');
                $user->categories()->detach();
                $user->categories()->attach($categories);
            }

            DB::commit();

            return true;

        } catch (Exception $e) {
            DB::rollback();
            return false;   
        }
    }

    /**
     * Delete a user
     *
     * @param Int $id
     * @return Bool
     */
    public function deleteUser(Int $id): Bool
    {
        try {

            DB::beginTransaction();

            $user = $this->model->find($id);

            if(!empty($user)) {
                $user->load('categories');
                $user->categories()->detach();
                $user->delete();

                DB::commit();
                return true;
            }                                
        } catch (Exception $e) {
            DB::rollback();
            return false;
        }
    }

    /**
     * Suspend a user
     *
     * @param Int $id
     * @return Bool
     */
    public function suspend(Int $id): Bool
    {
        try {
            
            DB::beginTransaction();

            $this->model->where('id', $id)->update([
                'status' => 0
            ]);

            DB::commit();

            return true;

        } catch (Exception $e) {
            DB::rollback();
            throw $e;
        }
    }

    /**
     * Activate a user
     *
     * @param Int $id
     * @return Bool
     */
    public function activate(Int $id): Bool
    {
        try {
            
            DB::beginTransaction();

            $this->model->where('id', $id)->update([
                'status' => 1
            ]);

            DB::commit();

            return true;

        } catch (Exception $e) {
            DB::rollback();
            throw $e;
        }      
    }
}
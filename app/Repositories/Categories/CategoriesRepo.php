<?php

namespace App\Repositories\Categories;

use Exception;
use App\Globals\Constants;
use App\Models\Category;
use App\Repositories\Categories\CategoriesRepoI;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\DB;
use Inquiloper\BaseRepository\BaseRepository;

class CategoriesRepo extends BaseRepository implements CategoriesRepoI
{
    public function __construct(Category $model)
    {
        parent::__construct($model);
    }

    /**
     * Get All Categories
     *
     * @return Collection
     */
    public function getAllCategories(): Collection
    {
        return $this->model->orderBy('slug', 'ASC')->get();
    }

    /**
     * Suspend a category
     *
     * @param Int $id
     * @return Bool
     */
    public function suspend(Int $id): Bool
    {
        try {

            DB::beginTransaction();

            $this->model->where('id', $id)->update([
                'estado' => 0
            ]);

            DB::commit();

            return true;
        } catch (Exception $e) {
            DB::rollback();
            throw $e;
        }
    }

    /**
     * Activate a category
     *
     * @param Int $id
     * @return Bool
     */
    public function activate(Int $id): Bool
    {
        try {

            DB::beginTransaction();

            $this->model->where('id', $id)->update([
                'estado' => 1
            ]);

            DB::commit();

            return true;
        } catch (Exception $e) {
            DB::rollback();
            throw $e;
        }
    }
}
<?php

namespace App\Repositories\Categories;

use Illuminate\Database\Eloquent\Collection;

interface CategoriesRepoI
{
    /**
     * Get All Categories
     *
     * @return Collection
     */
    public function getAllCategories(): Collection;

    /**
     * Suspend a category
     *
     * @param Int $id
     * @return Bool
     */
    public function suspend(Int $id): Bool;

    /**
     * Activate a category
     *
     * @param Int $id
     * @return Bool
     */
    public function activate(Int $id): Bool;
}
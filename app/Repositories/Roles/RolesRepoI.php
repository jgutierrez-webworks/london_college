<?php

namespace App\Repositories\Roles;

use Illuminate\Database\Eloquent\Collection;

interface RolesRepoI
{
    /**
     * Get roles
     *
     * @return Collection
     */
    public function getRolesList(): Collection;
}
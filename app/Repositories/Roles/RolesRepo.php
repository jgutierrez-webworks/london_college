<?php

namespace App\Repositories\Roles;

use App\Models\Role;
use App\Globals\Constants;
use App\Repositories\Roles\RolesRepoI;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\DB;
use Inquiloper\BaseRepository\BaseRepository;

class RolesRepo extends BaseRepository implements RolesRepoI
{
    public function __construct(Role $model)
    {
        parent::__construct($model);
    }

    /**
     * Get Roles
     *
     * @return Collection
     */
    public function getRolesList(): Collection
    {
        return $this->model->whereNotIn('name', [Constants::NAME_ROLE_SUPERADMINISTRATOR])
            ->orderBy('id', 'ASC')
            ->get();
    }
}
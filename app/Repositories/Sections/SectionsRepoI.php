<?php

namespace App\Repositories\Sections;

use Illuminate\Database\Eloquent\Collection;

interface SectionsRepoI
{
    /**
     * Get All Sections
     *
     * @return Collection
     */
    public function getAllSections(): Collection;

    /**
     * Suspend a section
     *
     * @param Int $id
     * @return Bool
     */
    public function suspend(Int $id): Bool;

    /**
     * Activate a section
     *
     * @param Int $id
     * @return Bool
     */
    public function activate(Int $id): Bool;
}
<?php

namespace App\Repositories\Sections;

use Exception;
use App\Globals\Constants;
use App\Models\Section;
use App\Repositories\Sections\SectionsRepoI;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\DB;
use Inquiloper\BaseRepository\BaseRepository;

class SectionsRepo extends BaseRepository implements SectionsRepoI
{
    public function __construct(Section $model)
    {
        parent::__construct($model);
    }

    /**
     * Get All Sections
     *
     * @return Collection
     */
    public function getAllSections(): Collection
    {
        return $this->model->orderBy('slug', 'ASC')->get();
    }

    /**
     * Suspend a section
     *
     * @param Int $id
     * @return Bool
     */
    public function suspend(Int $id): Bool
    {
        try {

            DB::beginTransaction();

            $this->model->where('id', $id)->update([
                'estado' => 0
            ]);

            DB::commit();

            return true;
        } catch (Exception $e) {
            DB::rollback();
            throw $e;
        }
    }

    /**
     * Activate a section
     *
     * @param Int $id
     * @return Bool
     */
    public function activate(Int $id): Bool
    {
        try {

            DB::beginTransaction();

            $this->model->where('id', $id)->update([
                'estado' => 1
            ]);

            DB::commit();

            return true;
        } catch (Exception $e) {
            DB::rollback();
            throw $e;
        }
    }
}
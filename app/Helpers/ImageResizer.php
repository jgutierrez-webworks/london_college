<?php

namespace App\Helpers;

use Exception;
use Illuminate\Http\UploadedFile;
use Intervention\Image\Facades\Image;

class ImageResizer
{
    const CODE_FLAG_IMAGE_RESIZER_BANNER = 1;
    const CODE_FLAG_IMAGE_RESIZER_OTHER = 2;
    
    /**
     * Set resize image and store in a custom folder
     *
     * @param String $storagePath
     * @param String $publicPath
     * @param String $keyColumn
     * @param File $file
     * @param Int $flag
     * @return Bool
     */
    public static function setImageResizer(String $storagePath, String $publicPath, String $keyColumn, UploadedFile $file, Int $flag): Bool
    {
        try {

            $imageResize = Image::make($file);

            //Validate Folder
            self::validateFolders($publicPath, $keyColumn, $flag);

            // backup status
            $imageResize->backup();

            $imageFileWidth = $imageResize->width();

            //Resize Original Image
            if ($imageFileWidth > 1980) {
                $imageResize->resize(1980, null, function ($constraint) {
                    $constraint->aspectRatio();
                    $constraint->upsize();
                });

                $imageResize->save(public_path() . $storagePath . $keyColumn . '/' . $file->hashName());

                // reset image (return to backup state)
                $imageResize->reset();
            }
            
            // perform some modifications
            $imageResize->resize(100, 100);
            $imageResize->save(public_path() . $storagePath . $keyColumn . '/thumbs-ico/' . $file->hashName());

            // reset image (return to backup state)
            $imageResize->reset();

            // perform other modifications
            $imageResize->resize(600, null, function ($constraint) {
                $constraint->aspectRatio();
            });

            $imageResize->save(public_path() . $storagePath . $keyColumn . '/thumbs-min/' . $file->hashName());

            if ($flag === self::CODE_FLAG_IMAGE_RESIZER_BANNER) {
                // reset image (return to backup state)
                $imageResize->reset();

                // perform other modifications
                $imageResize->resize(480, null, function ($constraint) {
                    $constraint->aspectRatio();
                });
                $imageResize->save(public_path() . $storagePath . $keyColumn . '/thumbs-mobile/' . $file->hashName());
            }

            return true;

        } catch (Exception $e) {
            throw $e;
        }
    }
    
    /**
     * Validate folder and create if not exists
     *
     * @param String $path
     * @param String $keyColumn
     * @param Int $flag
     * @return void
     */
    private static function validateFolders(String $path, String $keyColumn, Int $flag): void
    {
        if (!is_dir(storage_path('app' . $path . $keyColumn . '/thumbs-ico/'))) {
            mkdir(storage_path('app' . $path . $keyColumn . '/thumbs-ico/'));
        }

        if (!is_dir(storage_path('app' . $path . $keyColumn . '/thumbs-min/'))) {
            mkdir(storage_path('app' . $path . $keyColumn . '/thumbs-min/'));
        }

        if($flag === self::CODE_FLAG_IMAGE_RESIZER_BANNER) 
        {
            if (!is_dir(storage_path('app' . $path . $keyColumn . '/thumbs-mobile/'))) {
                mkdir(storage_path('app' . $path . $keyColumn . '/thumbs-mobile/'));
            }
        }        
    }
}

<?php

namespace App\Http\Requests\News;

use Illuminate\Foundation\Http\FormRequest;

class EditNewsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $newsId = intval($this->segment(3));
 
        return [
            'titulo'            => 'required|string|unique:noticia,titulo,' . $newsId,
            'fecha'             => 'required|date',
            'seccion'           => 'required|integer',
            'categoria'         => 'required|integer',
            'destacado'         => 'required|integer',
            'orden_destacado'   => 'nullable|integer',
            'banner'            => 'nullable',
            'tags.*'            => "required|string|distinct|min:1",
            'descripcion'       => 'required|string',
            'descripcion_corta' => 'required|string',
            'observaciones'     => 'nullable'
        ];
    }
}

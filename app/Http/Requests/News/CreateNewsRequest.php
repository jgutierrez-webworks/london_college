<?php

namespace App\Http\Requests\News;

use Illuminate\Foundation\Http\FormRequest;

class CreateNewsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'titulo'            => 'required|string|unique:noticia',
            'fecha'             => 'required|date',
            'seccion'           => 'required|integer',
            'categoria'         => 'required|integer',
            'destacado'         => 'required|integer',
            'orden_destacado'   => 'nullable|integer',
            'banner'            => 'required|file|image',
            'tags.*'            => "required|string|distinct|min:1",
            'descripcion_corta' => 'required|string',
            'descripcion'       => 'required|string',
            'observaciones'     => 'nullable'
        ];
    }
}

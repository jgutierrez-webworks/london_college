<?php

namespace App\Http\Requests\Banners;

use Illuminate\Foundation\Http\FormRequest;

class CreateBannerRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'file'                 => 'required|file',
            'file_mobile'          => 'required|file',
            'titulo_principal'     => 'nullable|string',
            'titulo_secundario'    => 'nullable|string',
            'link_redirect_activo' => 'nullable',
            'url_link_redirect'    => 'nullable|string'
        ];
    }
}

<?php

namespace App\Http\Requests\Headlines;

use Illuminate\Foundation\Http\FormRequest;

class EditHeadlineRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $headlineId = intval($this->segment(3));

        return [
            'nombre' => 'required|string|unique:titular_informativo,nombre,' . $headlineId,
        ];
    }
}

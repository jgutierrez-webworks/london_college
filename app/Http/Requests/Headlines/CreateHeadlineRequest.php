<?php

namespace App\Http\Requests\Headlines;

use Illuminate\Foundation\Http\FormRequest;

class CreateHeadlineRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nombre' => 'required|string|unique:titular_informativo'
        ];
    }
}

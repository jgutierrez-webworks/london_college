<?php

namespace App\Http\Requests\Users;

use Illuminate\Foundation\Http\FormRequest;

class EditUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $userId = intval($this->segment(3));

        return [
            'name'                  => 'required|string',
            'email'                 => 'required|email|unique:users,email,' . $userId,
            'phone'                 => 'nullable|string',
            'role'                  => 'required|string',
            'password'              => 'nullable',
            'password_confirmation' => 'nullable',
        ];
    }
}

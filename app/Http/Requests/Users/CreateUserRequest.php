<?php

namespace App\Http\Requests\Users;

use App\Globals\Constants;
use Illuminate\Foundation\Http\FormRequest;

class CreateUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'                  => 'required|string',
            'email'                 => 'required|email|unique:users',
            'phone'                 => 'nullable|string',
            'role'                  => 'required|string',
            'password'              => 'required|min:4|confirmed',
            'password_confirmation' => 'min:4',
            'categories'            => 'required_if:role,==,' . Constants::NAME_ROLE_MODERATOR,
            'can_access_banners'    => 'nullable',
            'can_access_gallery'    => 'nullable'
        ];
    }
}

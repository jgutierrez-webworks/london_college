<?php

namespace App\Http\Resources\Admin\Sections;

use Illuminate\Http\Resources\Json\ResourceCollection;

class SectionsCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return SectionResource::collection($this->collection);
    }
}

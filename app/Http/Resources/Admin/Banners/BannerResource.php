<?php

namespace App\Http\Resources\Admin\Banners;

use App\Http\Resources\Admin\Files\FileResource;
use Illuminate\Http\Resources\Json\JsonResource;

class BannerResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'                   => $this->id,
            'codigo'               => $this->codigo,
            'orden'                => $this->orden,
            'file'                 => new FileResource($this->whenLoaded('file')),
            'file_mobile'          => new FileResource($this->whenLoaded('fileMobile')),
            'titulo_principal'     => $this->titulo_principal,
            'titulo_secundario'    => $this->titulo_secundario,
            'link_redirect_activo' => $this->link_redirect_activo,
            'url_link_redirect'    => $this->url_link_redirect,
            'estado'               => $this->estado,
            'created_at'           => $this->created_at->diffForHumans(),
            'updated_at'           => $this->updated_at->diffForHumans(),

            'base_path_banner'     => asset('storage/banners/' . $this->codigo)
        ];
    }
}

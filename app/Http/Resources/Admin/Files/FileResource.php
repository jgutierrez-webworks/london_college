<?php

namespace App\Http\Resources\Admin\Files;

use App\Globals\Functions;
use Illuminate\Http\Resources\Json\JsonResource;

class FileResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'               => $this->id,
            'titulo'           => $this->titulo,
            'nombre'           => $this->nombre,
            'nombre_original'  => $this->nombre_original,
            'extension'        => $this->extension,
            'created_at'       => $this->created_at->diffForHumans(),
            'updated_at'       => $this->updated_at->diffForHumans(),

            'is_image'         => Functions::isImageExtension($this->extension),
            'is_video'         => Functions::isVideoExtension($this->extension),
        ];
    }
}

<?php

namespace App\Http\Resources\Admin\Users;

use App\Http\Resources\Admin\Categories\CategoriesCollection;
use App\Http\Resources\Admin\Roles\RolesCollection;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Resources\Json\JsonResource;

class UserResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request): Array
    {
        return [
            'id'                 => $this->id,
            'name'               => $this->name,
            'email'              => $this->email,
            'phone'              => $this->phone,
            'role'               => $this->getRoleUser($this->roles),
            'can_access_banners' => $this->can_access_banners,
            'can_access_gallery' => $this->can_access_gallery,
            'status'             => $this->status,
            'created_at'         => $this->created_at->diffForHumans(),
            'updated_at'         => $this->updated_at->diffForHumans(),

            'roles'              => new RolesCollection($this->whenLoaded('roles')),
            'categories'         => new CategoriesCollection($this->whenLoaded('categories')),
        ];
    }

    /**
     * Get Role of User
     *
     * @param Collection $roles
     * @return String
     */
    private function getRoleUser(Collection $roles): String
    {
        $role = '';

        if (!empty($roles->first())) {
            $role = $roles->first()->display_name;
        }

        return $role;
    }
}

<?php

namespace App\Http\Resources\Admin\News;

use App\Http\Resources\Admin\Categories\CategoryResource;
use App\Http\Resources\Admin\Files\FileResource;
use App\Http\Resources\Admin\Sections\SectionResource;
use App\Http\Resources\Admin\Users\UserResource;
use Illuminate\Http\Resources\Json\JsonResource;

class NewsResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'                => $this->id,
            'codigo'            => $this->codigo,
            'slug'              => $this->slug,
            'fecha'             => $this->fecha->format('Y-m-d'),
            'fecha_format'      => $this->fecha->format('d-m-Y'),
            'titulo'            => $this->titulo,
            'descripcion_corta' => $this->descripcion_corta,
            'descripcion'       => $this->descripcion,            
            'user'              => new UserResource($this->whenLoaded('user')),
            'section'           => new SectionResource($this->whenLoaded('section')),
            'category'          => new CategoryResource($this->whenLoaded('category')),
            'banner'            => new FileResource($this->whenLoaded('banner')),
            'destacado'         => $this->destacado,
            'orden_destacado'   => $this->orden_destacado,
            'estado'            => $this->estado,
            'tags'              => $this->tags,
            'observaciones'     => $this->observaciones,
            'created_at'        => $this->created_at->diffForHumans(),
            'updated_at'        => $this->updated_at->diffForHumans()
        ];
    }
}

<?php

namespace App\Http\Resources\Site\News;

use App\Http\Resources\Site\Categories\CategoryResource;
use App\Http\Resources\Site\Files\FileResource;
use App\Http\Resources\Site\Sections\SectionResource;
use App\Http\Resources\Site\Users\UserResource;
use Illuminate\Http\Resources\Json\JsonResource;

class NewsResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'codigo'                => $this->codigo,
            'slug'                  => $this->slug,
            'fecha'                 => $this->fecha->format('Y-m-d'),
            'fecha_format'          => $this->fecha->format('d-m-Y'),
            'titulo'                => $this->titulo,
            'descripcion'           => $this->descripcion,
            'descripcion_corta'     => $this->descripcion_corta,
            'user'                  => new UserResource($this->whenLoaded('user')),
            'section'               => new SectionResource($this->whenLoaded('section')),
            'category'              => new CategoryResource($this->whenLoaded('category')),
            'banner'                => new FileResource($this->whenLoaded('banner')),
            'destacado'             => $this->destacado,
            'orden_destacado'       => $this->orden_destacado,
            'estado'                => $this->estado,
            'tags'                  => explode(',', $this->tags),
            'tags_format'           => $this->tags,
            'created_at'            => $this->created_at->diffForHumans(),
            'updated_at'            => $this->updated_at->diffForHumans(),

            'link'                  => route('site.news-item', $this->slug),
            'base_path_news'        => asset('storage/noticias/banners/' . $this->codigo),
            'base_path_news_ico'   => asset('storage/noticias/banners/' . $this->codigo . '/thumbs-ico'),
            'base_path_news_mobile' => asset('storage/noticias/banners/' . $this->codigo . '/thumbs-mobile'),
            'base_path_news_min'    => asset('storage/noticias/banners/' . $this->codigo . '/thumbs-min'),
        ];
    }
}
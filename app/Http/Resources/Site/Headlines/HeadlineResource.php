<?php

namespace App\Http\Resources\Site\Headlines;

use Illuminate\Http\Resources\Json\JsonResource;

class HeadlineResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'slug'       => $this->slug,
            'nombre'     => $this->nombre,
            'estado'     => $this->estado,
            'created_at' => $this->created_at->diffForHumans(),
        ];
    }
}

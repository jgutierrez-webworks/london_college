<?php

namespace App\Http\Resources\Site\Categories;

use Illuminate\Http\Resources\Json\JsonResource;

class CategoryResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'slug'   => $this->slug,
            'nombre' => $this->nombre
        ];
    }
}

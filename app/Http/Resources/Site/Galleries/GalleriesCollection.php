<?php

namespace App\Http\Resources\Site\Galleries;

use Illuminate\Http\Resources\Json\ResourceCollection;

class GalleriesCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return GalleryResource::collection($this->collection);
    }
}

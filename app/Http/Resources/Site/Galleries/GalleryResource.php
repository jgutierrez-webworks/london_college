<?php

namespace App\Http\Resources\Site\Galleries;

use App\Globals\Structures;
use App\Http\Resources\Site\Files\FilesCollection;
use Illuminate\Http\Resources\Json\JsonResource;

class GalleryResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'codigo'              => $this->codigo,
            'slug'                => $this->slug,
            'nombre'              => $this->nombre,
            'descripcion'         => $this->descripcion,
            'fecha'               => $this->fecha->format('Y-m-d'),
            'fecha_format'        => $this->fecha->format('d-m-Y'),
            'fecha_format_short'  => Structures::$meses[$this->fecha->format('m')] . ' ' . $this->fecha->format('Y'),
            'lugar'               => $this->lugar,
            'estado'              => $this->estado,            
            'created_at'          => $this->created_at->diffForHumans(),

            'files'               => new FilesCollection($this->whenLoaded('files')),
            'base_path_gallery'   => asset('storage/galeria/' . $this->codigo)
        ];
    }
}

<?php

namespace App\Http\Controllers\Api;

use App\Globals\Alerts;
use App\Http\Controllers\Controller;
use App\Http\Requests\Gallery\CreateGalleryImageRequest;
use App\Http\Requests\Gallery\CreateGalleryRequest;
use App\Http\Requests\Gallery\EditGalleryRequest;
use App\Http\Resources\Admin\Gallery\GalleryCollection;
use App\Http\Resources\Admin\Gallery\GalleryResource;
use App\Repositories\Gallery\GalleryRepoI;
use Illuminate\Http\Request;

class GalleryController extends Controller
{
    protected $galleryRepo;

    public function __construct(GalleryRepoI $galleryRepo)
    {
        $this->galleryRepo = $galleryRepo;
    }

    public function index()
    {
        $galleries = $this->galleryRepo->getAllGalleries();

        return response()->json(new GalleryCollection($galleries));
    }

    public function store(CreateGalleryRequest $request)
    {
        $data = $request->validated();
        $this->galleryRepo->store($data);

        return response()->json([
            'status'  => 'OK',
            'message' => Alerts::CREATE_SUCCESS
        ]);
    }

    public function show($id)
    {
        $gallery = $this->galleryRepo->with(['files'])->findOneBy(['id' => $id]);

        return response()->json(new GalleryResource($gallery));
    }

    public function update($id, EditGalleryRequest $request)
    {
        $data = $request->validated();
        $this->galleryRepo->edit($id, $data);

        return response()->json([
            'status'  => 'OK',
            'message' => Alerts::UPDATE_SUCCESS
        ]);
    }

    public function delete($id, Request $request)
    {
        $this->galleryRepo->delete($id);

        return response()->json([
            'status'  => 'OK',
            'message' => Alerts::DELETE_SUCCESS
        ]);
    }

    public function suspend($id, Request $request)
    {
        $this->galleryRepo->suspend($id);

        return response()->json([
            'status'  => 'OK',
            'message' => Alerts::SUSPEND_SUCCESS
        ]);
    }

    public function activate($id, Request $request)
    {
        $this->galleryRepo->activate($id);

        return response()->json([
            'status'  => 'OK',
            'message' => Alerts::ACTIVATE_SUCCESS
        ]);
    }

    public function storeImage($galleryId, CreateGalleryImageRequest $request)
    {
        if (isset($request->file) && $request->hasFile('file') && $request->file('file')->isValid()) {
            $file = $request->file('file');

            if($this->galleryRepo->storeImage($galleryId, $file)) {
                return response()->json([
                    'status'  => 'OK',
                    'message' => Alerts::CREATE_SUCCESS
                ]);   
            }
        }

        return response()->json([
            'status'  => 'ERROR',
            'message' => Alerts::ERROR_505
        ]);
    }

    public function deleteImage($galleryId, $fileId, Request $request)
    {
        if($this->galleryRepo->deleteImage($galleryId, $fileId)) {
            return response()->json([
                'status'  => 'OK',
                'message' => Alerts::DELETE_SUCCESS
            ]);    
        }

        return response()->json([
            'status'  => 'ERROR',
            'message' => Alerts::ERROR_505
        ]);
    }
}

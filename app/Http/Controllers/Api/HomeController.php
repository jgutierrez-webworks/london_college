<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Repositories\Banners\BannersRepoI;
use App\Repositories\News\NewsRepoI;
use App\Repositories\Users\UsersRepoI;

class HomeController extends Controller
{
    protected $bannersRepo, $newsRepo, $usersRepo;

    public function __construct(BannersRepoI $bannersRepo, NewsRepoI $newsRepo, UsersRepoI $usersRepo)
    {
        $this->bannersRepo = $bannersRepo;
        $this->newsRepo = $newsRepo;
        $this->usersRepo = $usersRepo;
    }

    public function getStatistics() 
    {
        $dataStatistics = [];
        $dataStatistics['totalBanners'] = $this->bannersRepo->getTotalBannersActive();
        $dataStatistics['totalNews'] = $this->newsRepo->getTotalNewsActive();
        $dataStatistics['totalUsers'] = $this->usersRepo->getCountActiveUsers();
        
        return response()->json($dataStatistics);
    }
}

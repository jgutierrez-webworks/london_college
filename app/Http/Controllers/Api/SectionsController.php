<?php

namespace App\Http\Controllers\Site;

use App\Globals\Alerts;
use App\Http\Controllers\Controller;
use App\Http\Requests\Sections\CreateSectionRequest;
use App\Http\Requests\Sections\EditSectionRequest;
use App\Http\Resources\Admin\Sections\SectionsCollection;
use App\Repositories\Sections\SectionsRepoI;
use Illuminate\Http\Request;

class SectionsController extends Controller
{
    protected $sectionsRepo;

    public function __construct(SectionsRepoI $sectionsRepo)
    {
        $this->sectionsRepo = $sectionsRepo;
    }

    public function index()
    {
        $sections = $this->sectionsRepo->getAllSections();

        return response()->json(new SectionsCollection($sections));
    }

    public function store(CreateSectionRequest $request)
    {
        $data = $request->validated();

        $this->sectionsRepo->create($data);

        return response()->json([
            'status'  => 'OK',
            'message' => Alerts::CREATE_SUCCESS
        ]);
    }

    public function show($id)
    {
        $section = $this->sectionsRepo->findOneBy(['id' => $id]);

        return response()->json($section);
    }

    public function update($id, EditSectionRequest $request)
    {
        $data = $request->validated();
        $this->sectionsRepo->updateBy(['id' => $id], $data);

        return response()->json([
            'status'  => 'OK',
            'message' => Alerts::UPDATE_SUCCESS
        ]);
    }

    public function delete($id, Request $request)
    {
        $this->sectionsRepo->deleteBy(['id' => $id]);

        return response()->json([
            'status'  => 'OK',
            'message' => Alerts::DELETE_SUCCESS
        ]);
    }

    public function suspend($id, Request $request)
    {
        $this->sectionsRepo->suspend($id);

        return response()->json([
            'status'  => 'OK',
            'message' => Alerts::SUSPEND_SUCCESS
        ]);
    }

    public function activate($id, Request $request)
    {
        $this->sectionsRepo->activate($id);

        return response()->json([
            'status'  => 'OK',
            'message' => Alerts::ACTIVATE_SUCCESS
        ]);
    }
}
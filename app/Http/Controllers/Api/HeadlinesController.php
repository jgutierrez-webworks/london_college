<?php

namespace App\Http\Controllers\Api;

use App\Globals\Alerts;
use App\Http\Controllers\Controller;
use App\Http\Requests\Headlines\CreateHeadlineRequest;
use App\Http\Requests\Headlines\EditHeadlineRequest;
use App\Http\Resources\Admin\Headlines\HeadlineResource;
use App\Http\Resources\Admin\Headlines\HeadlinesCollection;
use App\Repositories\Headlines\HeadlinesRepoI;
use Illuminate\Http\Request;

class HeadlinesController extends Controller
{
    protected $headlinesRepo;

    public function __construct(HeadlinesRepoI $headlinesRepo)
    {
        $this->headlinesRepo = $headlinesRepo;
    }

    public function index()
    {
        $headlines = $this->headlinesRepo->getAllHeadlines();

        return response()->json(new HeadlinesCollection($headlines));
    }

    public function store(CreateHeadlineRequest $request)
    {
        $data = $request->validated();

        $this->headlinesRepo->create($data);

        return response()->json([
            'status'  => 'OK',
            'message' => Alerts::CREATE_SUCCESS
        ]);
    }

    public function show($id)
    {
        $headline = $this->headlinesRepo->findOneBy(['id' => $id]);

        return response()->json(new HeadlineResource($headline));
    }

    public function update($id, EditHeadlineRequest $request)
    {
        $data = $request->validated();
        
        $this->headlinesRepo->updateBy(['id' => $id], $data);

        return response()->json([
            'status'  => 'OK',
            'message' => Alerts::UPDATE_SUCCESS
        ]);
    }

    public function delete($id, Request $request)
    {
        $this->headlinesRepo->deleteBy(['id' => $id]);

        return response()->json([
            'status'  => 'OK',
            'message' => Alerts::DELETE_SUCCESS
        ]);
    }

    public function suspend($id, Request $request)
    {
        $this->headlinesRepo->suspend($id);

        return response()->json([
            'status'  => 'OK',
            'message' => Alerts::SUSPEND_SUCCESS
        ]);
    }

    public function activate($id, Request $request)
    {
        $this->headlinesRepo->activate($id);

        return response()->json([
            'status'  => 'OK',
            'message' => Alerts::ACTIVATE_SUCCESS
        ]);
    }
}

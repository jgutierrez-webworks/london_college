<?php

namespace App\Http\Controllers\Api;

use App\Globals\Alerts;
use App\Globals\Constants;
use App\Http\Controllers\Controller;
use App\Http\Requests\Site\ContactRequest;
use App\Http\Resources\Site\Galleries\GalleriesCollection;
use App\Http\Resources\Site\Headlines\HeadlinesCollection;
use App\Http\Resources\Site\News\NewsCollection;
use App\Http\Resources\Site\News\NewsResource;
use App\Repositories\Gallery\GalleryRepoI;
use App\Repositories\Headlines\HeadlinesRepoI;
use App\Repositories\News\NewsRepoI;
use Illuminate\Http\Request;

class SiteController extends Controller
{
    protected $galleriesRepo, $headlinesRepo, $newsRepo;

    public function __construct(GalleryRepoI $galleriesRepo, HeadlinesRepoI $headlinesRepo, NewsRepoI $newsRepo)
    {
        $this->galleriesRepo = $galleriesRepo;
        $this->headlinesRepo = $headlinesRepo;
        $this->newsRepo = $newsRepo;
    }
    
    public function getAllNews(Request $request)
    {
        $news = $this->newsRepo->getNewsByFilters($request->query->all());

        return response()->json(new NewsCollection($news));
    }

    public function getAllFeaturedNews()
    {
        $news = $this->newsRepo->getFeaturedNews();

        return response()->json(new NewsCollection($news));
    }

    public function getAllRecentNews()
    {
        $news = $this->newsRepo->getRecentNews();

        return response()->json(new NewsCollection($news));
    }

    public function getFullNewsInfo($slug)
    {
        $newsItem = $this->newsRepo->getFullNewsInfo($slug);

        return response()->json(new NewsResource($newsItem));
    }

    public function getAllRelatedNews($slug)
    {
        $news = $this->newsRepo->getRelatedNewsBySlug($slug);

        return response()->json(new NewsCollection($news));
    }

    public function getAllGalleries()
    {
        $galleries = $this->galleriesRepo->getAllGalleriesByStatus(Constants::STATUS_ACTIVE);

        return response()->json(new GalleriesCollection($galleries));
    }

    public function getAllHeadlines()
    {
        $headlines = $this->headlinesRepo->getAllActiveHeadlines();

        return response()->json(new HeadlinesCollection($headlines));
    }

    public function contact(ContactRequest $request)
    {
        $data = $request->validated();

        $subject = 'Consulta Formulario de Contacto | COLEGIO EDUCACIONAL LONDON';
        $toemail = "contacto@londoncollege.com";

        $headers = "MIME-Version: 1.0\n"
            . "From: \"" . $data['name'] . "\" <" . $data['email'] . ">\n"
            . "Content-type: text/html; charset=iso-8859-1\n";

        $body = '
			        <html>
						<head>
							<title>Formulario de Contacto</title>
						</head>
						<body>								
							<table styles="margin-top: 50px;">
								<tr>
									<td><b>Nombre</b></td>
									<td>: ' . $data['name'] . '</td>
								</tr>
								<tr>
									<td><b>Email</b></td>
									<td>: ' . $data['email'] . '</td>
								</tr>
								<tr>
									<td><b>Whatsapp</b></td>
									<td>: ' . $data['phone'] . '</td>
								</tr>
								<tr>
									<td><b>Mensaje</b></td>
									<td style="text-align: justify;">: ' . $data['message'] . '</td>
								</tr>
							</table>
						</body>
					</html>
	            ';

        if (mail($toemail, $subject, $body, $headers)) {
            $response = [
                'status'  => 'OK',
                'message' => Alerts::SEND_CONTACT_SUCCESS
            ];
        } else {
            $response = [
                'status'  => 'ERROR',
                'message' => Alerts::SEND_CONTACT_SUCCESS
            ];
        }

        
        return response()->json($response);
    }
}

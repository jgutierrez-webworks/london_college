<?php

namespace App\Http\Controllers\Api;

use App\Globals\Alerts;
use App\Globals\Constants;
use App\Http\Controllers\Controller;
use App\Http\Requests\Profile\ChangePasswordRequest;
use App\Http\Requests\Profile\GeneralRequest;
use App\Http\Resources\Admin\Profile\ProfileResource;
use App\Repositories\Users\UsersRepoI;

class ProfileController extends Controller
{
    protected $usersRepo;

    public function __construct(UsersRepoI $usersRepo)
    {
        $this->usersRepo = $usersRepo;
    }

    public function profile()
    {
        return response()->json(new ProfileResource(auth()->user()));
    }

    public function changeGeneral(GeneralRequest $request)
    {
        $data = $request->validated();

        $this->usersRepo->updateBy(['id' => auth()->user()->id], [
            'name' => $data['name']
        ]);

        return response()->json([
            'status'  => 'OK',
            'message' => Alerts::UPDATE_PROFILE_SUCCESS
        ]);
    }

    public function changePassword(ChangePasswordRequest $request)
    {
        $data = $request->validated();

        $this->usersRepo->updateBy(['id' => auth()->user()->id], [
            'password' => bcrypt($data['password'])
        ]);

        return response()->json([
            'status'  => 'OK',
            'message' => Alerts::UPDATE_PASSWORD_SUCCESS
        ]);
    }
}

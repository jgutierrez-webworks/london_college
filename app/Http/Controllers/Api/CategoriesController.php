<?php

namespace App\Http\Controllers\Api;

use App\Globals\Alerts;
use App\Http\Controllers\Controller;
use App\Http\Requests\Categories\CreateCategoryRequest;
use App\Http\Requests\Categories\EditCategoryRequest;
use App\Http\Resources\Admin\Categories\CategoriesCollection;
use App\Http\Resources\Admin\Categories\CategoryResource;
use App\Repositories\Categories\CategoriesRepoI;
use Illuminate\Http\Request;

class CategoriesController extends Controller
{
    protected $categoriesRepo;

    public function __construct(CategoriesRepoI $categoriesRepo)
    {
        $this->categoriesRepo = $categoriesRepo;
    }

    public function index()
    {
        $categories = $this->categoriesRepo->getAllCategories();

        return response()->json(new CategoriesCollection($categories));
    }

    public function store(CreateCategoryRequest $request)
    {
        $data = $request->validated();

        $this->categoriesRepo->create($data);

        return response()->json([
            'status'  => 'OK',
            'message' => Alerts::CREATE_SUCCESS
        ]);
    }

    public function show($id)
    {
        $category = $this->categoriesRepo->findOneBy(['id' => $id]);

        return response()->json(new CategoryResource($category));
    }

    public function update($id, EditCategoryRequest $request)
    {
        $data = $request->validated();
        $this->categoriesRepo->updateBy(['id' => $id], $data);

        return response()->json([
            'status'  => 'OK',
            'message' => Alerts::UPDATE_SUCCESS
        ]);
    }

    public function delete($id, Request $request)
    {
        $this->categoriesRepo->deleteBy(['id' => $id]);

        return response()->json([
            'status'  => 'OK',
            'message' => Alerts::DELETE_SUCCESS
        ]);
    }

    public function suspend($id, Request $request)
    {
        $this->categoriesRepo->suspend($id);

        return response()->json([
            'status'  => 'OK',
            'message' => Alerts::SUSPEND_SUCCESS
        ]);
    }

    public function activate($id, Request $request)
    {
        $this->categoriesRepo->activate($id);

        return response()->json([
            'status'  => 'OK',
            'message' => Alerts::ACTIVATE_SUCCESS
        ]);
    }
}

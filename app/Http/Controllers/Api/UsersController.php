<?php

namespace App\Http\Controllers\Api;

use App\Globals\Alerts;
use App\Http\Controllers\Controller;
use App\Http\Requests\Users\CreateUserRequest;
use App\Http\Requests\Users\EditUserRequest;
use App\Http\Resources\Admin\Users\UserResource;
use App\Http\Resources\Admin\Users\UsersCollection;
use App\Repositories\Users\UsersRepoI;
use Illuminate\Http\Request;

class UsersController extends Controller
{
    protected $usersRepo;

    public function __construct(UsersRepoI $usersRepo)
    {
        $this->usersRepo = $usersRepo;
    }

    public function index()
    {
        $users = $this->usersRepo->getAllUsers();

        return response()->json(new UsersCollection($users));
    }

    public function store(CreateUserRequest $request)
    {
        $data = [
            'name'               => $request->post('name'),
            'email'              => $request->post('email'),
            'phone'              => $request->post('phone'),
            'password'           => bcrypt($request->post('password')),
            'can_access_banners' => ($request->post('can_access_banners')) ? 1 : 0,
            'can_access_gallery' => ($request->post('can_access_gallery')) ? 1 : 0,
        ];

        $this->usersRepo->createUser($data, $request->post('role'), $request->post('categories'));

        return response()->json([
            'status'  => 'OK',
            'message' => Alerts::CREATE_SUCCESS
        ]);
    }

    public function show($id)
    {
        $user = $this->usersRepo->with(['roles', 'categories'])->findOneBy(['id' => $id]);

        return response()->json(new UserResource($user));
    }

    public function update($id, EditUserRequest $request)
    {
        $data = [
            'name'               => $request->post('name'),
            'email'              => $request->post('email'),
            'phone'              => $request->post('phone'),
            'can_access_banners' => ($request->post('can_access_banners')) ? 1 : 0,
            'can_access_gallery' => ($request->post('can_access_gallery')) ? 1 : 0,
        ];

        if ($request->password) {
            $data['password'] = bcrypt($request->password);
        }

        $this->usersRepo->updateUser($id, $data, $request->post('role'), $request->post('categories'));

        return response()->json([
            'status'  => 'OK',
            'message' => Alerts::UPDATE_SUCCESS
        ]);
    }

    public function delete($id, Request $request)
    {
        $this->usersRepo->deleteUser($id);

        return response()->json([
            'status'  => 'OK',
            'message' => Alerts::DELETE_SUCCESS
        ]);
    }

    public function suspend($id, Request $request)
    {
        $this->usersRepo->suspend($id);

        return response()->json([
            'status'  => 'OK',
            'message' => Alerts::SUSPEND_SUCCESS
        ]);
    }

    public function activate($id, Request $request)
    {
        $this->usersRepo->activate($id);

        return response()->json([
            'status'  => 'OK',
            'message' => Alerts::ACTIVATE_SUCCESS
        ]);
    }
}

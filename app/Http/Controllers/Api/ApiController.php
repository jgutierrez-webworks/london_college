<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\Admin\Categories\CategoriesCollection;
use App\Http\Resources\Admin\Roles\RolesCollection;
use App\Http\Resources\Admin\Sections\SectionsCollection;
use App\Repositories\Custom\CustomRepoI;
use App\Repositories\Roles\RolesRepoI;
use App\Repositories\Users\UsersRepoI;
use Illuminate\Http\Request;

class ApiController extends Controller
{
    protected $customRepo, $rolesRepo, $usersRepo;

    public function __construct(CustomRepoI $customRepo, RolesRepoI $rolesRepo, UsersRepoI $usersRepo) 
    {
        $this->customRepo = $customRepo;
        $this->rolesRepo = $rolesRepo;
        $this->usersRepo = $usersRepo;
    }

    public function getRoles()
    {
        $roles = $this->rolesRepo->getRolesList();

        return response()->json(new RolesCollection($roles));
    }

    public function getCategories() 
    {
        $categories = $this->customRepo->getCategories();   

        return response()->json(new CategoriesCollection($categories));
    }

    public function getFrequentCategories()
    {
        $frequentCategories = $this->customRepo->getFrequentCategories();

        return response()->json($frequentCategories);
    }

    public function getSections()
    {
        $sections = $this->customRepo->getSections();

        return response()->json(new SectionsCollection($sections));
    }

    public function validUserEmail(Request $request)
    {
        $user = $this->usersRepo->findOneBy(['email' => $request->post('email')]);

        if (!empty($user)) {
            if (!empty($request->post('user')) && $request->post('user')[0] == $user->id) {
                $valid = 0;
            } else {
                $valid = 1;
            }
        } else {
            $valid = 0;
        }

        return response()->json([
            'valid' => $valid
        ]);
    }
}

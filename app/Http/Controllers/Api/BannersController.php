<?php

namespace App\Http\Controllers\Api;

use App\Globals\Alerts;
use App\Http\Controllers\Controller;
use App\Http\Requests\Banners\CreateBannerRequest;
use App\Http\Requests\Banners\EditBannerRequest;
use App\Http\Resources\Admin\Banners\BannerResource;
use App\Http\Resources\Admin\Banners\BannersCollection;
use App\Repositories\Banners\BannersRepoI;
use Illuminate\Http\Request;

class BannersController extends Controller
{
    protected $bannersRepo;

    public function __construct(BannersRepoI $bannersRepo)
    {
        $this->bannersRepo = $bannersRepo;
    }

    public function index()
    {
        $banners = $this->bannersRepo->getAllBanners();

        return response()->json(new BannersCollection($banners));
    }

    public function store(CreateBannerRequest $request)
    {
        $data = $request->except(['file', 'file_mobile']);

        if ((isset($request->file) && $request->hasFile('file') && $request->file('file')->isValid()) && (isset($request->file_mobile) && $request->hasFile('file_mobile') && $request->file('file_mobile')->isValid())) {
            $this->bannersRepo->store($data, $request->file('file'), $request->file('file_mobile'));

            return response()->json([
                'status'  => 'OK',
                'message' => Alerts::CREATE_SUCCESS
            ]);
        }

        return response()->json([
            'status'  => 'ERROR',
            'message' => Alerts::ERROR_505
        ]);
    }

    public function show($id)
    {
        $banner = $this->bannersRepo->with(['file', 'fileMobile'])->findOneBy(['id' => $id]);

        return response()->json(new BannerResource($banner));
    }

    public function update($id, EditBannerRequest $request)
    {
        $data = $request->except(['file']);
        $file = $fileMobile = null;

        if (isset($request->file) && $request->hasFile('file') && $request->file('file')->isValid()) {
            $file = $request->file('file');
        }

        if (isset($request->file_mobile) && $request->hasFile('file_mobile') && $request->file('file_mobile')->isValid()) {
            $fileMobile = $request->file('file_mobile');
        }

        $this->bannersRepo->update($id, $data, $file, $fileMobile);

        return response()->json([
            'status'  => 'OK',
            'message' => Alerts::UPDATE_SUCCESS
        ]);
    }

    public function delete($id, Request $request)
    {
        $this->bannersRepo->deleteBy(['id' => $id]);

        return response()->json([
            'status'  => 'OK',
            'message' => Alerts::DELETE_SUCCESS
        ]);
    }

    public function suspend($id, Request $request)
    {
        $this->bannersRepo->suspend($id);

        return response()->json([
            'status'  => 'OK',
            'message' => Alerts::SUSPEND_SUCCESS
        ]);
    }

    public function activate($id, Request $request)
    {
        $this->bannersRepo->activate($id);

        return response()->json([
            'status'  => 'OK',
            'message' => Alerts::ACTIVATE_SUCCESS
        ]);
    }

    public function upOrder($id, Request $request)
    {
        $this->bannersRepo->upOrder($id);

        return response()->json([
            'status'  => 'OK',
            'message' => 'El orden ha sido subido correctamente'
        ]);
    }

    public function downOrder($id, Request $request)
    {
        $this->bannersRepo->downOrder($id);

        return response()->json([
            'status'  => 'OK',
            'message' => 'El orden ha sido bajado correctamente'
        ]);
    }
}

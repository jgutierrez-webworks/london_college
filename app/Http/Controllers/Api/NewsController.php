<?php

namespace App\Http\Controllers\Api;

use App\Globals\Alerts;
use App\Http\Controllers\Controller;
use App\Http\Requests\News\CreateNewsRequest;
use App\Http\Requests\News\EditNewsRequest;
use App\Http\Resources\Admin\News\NewsCollection;
use App\Http\Resources\Admin\News\NewsResource;
use App\Repositories\News\NewsRepoI;
use Illuminate\Http\Request;

class NewsController extends Controller
{
    protected $newsRepo;

    public function __construct(NewsRepoI $newsRepo)
    {
        $this->newsRepo = $newsRepo;
    }

    public function index()
    {
        $news = $this->newsRepo->getAllNews();

        return response()->json(new NewsCollection($news));
    }

    public function store(CreateNewsRequest $request)
    {
        $data = $request->except(['seccion', 'categoria', 'banner']);
        $data['seccion_id'] = $request->post('seccion');
        $data['categoria_id'] = $request->post('categoria');

        if (isset($request->banner) && $request->hasFile('banner') && $request->file('banner')->isValid()) {
            $this->newsRepo->store($data, $request->file('banner'));

            return response()->json([
                'status'  => 'OK',
                'message' => Alerts::CREATE_SUCCESS
            ]);
        }

        return response()->json([
            'status'  => 'ERROR',
            'message' => Alerts::ERROR_505
        ]);
    }

    public function show($id)
    {
        $news = $this->newsRepo->with(['user', 'section', 'category', 'banner'])->findOneBy(['id' => $id]);

        return response()->json(new NewsResource($news));
    }

    public function update($id, EditNewsRequest $request)
    {
        $data = $request->except(['seccion', 'categoria', 'banner']);
        $data['seccion_id'] = $request->post('seccion');
        $data['categoria_id'] = $request->post('categoria');
        $fileBanner = null;

        if (isset($request->banner) && $request->hasFile('banner') && $request->file('banner')->isValid()) {
            $fileBanner = $request->file('banner');
        }

        $this->newsRepo->update($id, $data, $fileBanner);

        return response()->json([
            'status'  => 'OK',
            'message' => Alerts::UPDATE_SUCCESS
        ]);
    }

    public function delete($id, Request $request)
    {
        $this->newsRepo->delete($id);

        return response()->json([
            'status'  => 'OK',
            'message' => Alerts::DELETE_SUCCESS
        ]);
    }

    public function suspend($id, Request $request)
    {
        $this->newsRepo->suspend($id);

        return response()->json([
            'status'  => 'OK',
            'message' => Alerts::SUSPEND_SUCCESS
        ]);
    }

    public function activate($id, Request $request)
    {
        $this->newsRepo->activate($id);

        return response()->json([
            'status'  => 'OK',
            'message' => Alerts::ACTIVATE_SUCCESS
        ]);
    }
}

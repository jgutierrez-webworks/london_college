<?php

namespace App\Http\Controllers\Site;

use App\Globals\Constants;
use App\Http\Controllers\Controller;
use App\Repositories\Banners\BannersRepoI;
use App\Repositories\Gallery\GalleryRepoI;
use Illuminate\Http\Request;

class SiteController extends Controller
{
    protected $bannersRepo, $galleriesRepo;

    public function __construct(BannersRepoI $bannersRepo, GalleryRepoI $galleriesRepo)
    {
        $this->bannersRepo = $bannersRepo;
        $this->galleriesRepo = $galleriesRepo;
    }

    public function index()
    {
        $banners = $this->bannersRepo->getAllActiveBanners();

        return view('site.index', [
            'banners' => $banners
        ]);
    }    

    public function news(Request $request)
    {
        return view('site.news');
    }

    public function newsItem($slug)
    {
        return view('site.news-item', [
            'slug' => $slug
        ]);
    }

    public function galleries()
    {
        $galleries = $this->galleriesRepo->getAllGalleriesByStatus(Constants::STATUS_ACTIVE);
        
        return view('site.galleries', [
            'galleries' => $galleries
        ]);
    }

    public function mission()
    {
        return view('site.mission');
    }

    public function organization()
    {
        return view('site.organization');
    }

    public function review()
    {
        return view('site.review');
    }

    public function about()
    {
        return view('site.about');
    }
}

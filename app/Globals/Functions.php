<?php

namespace App\Globals;

use DateTime;
use Carbon\Carbon;

use App\Globals\Constants;
use App\Globals\Structures;
use Illuminate\Support\Facades\DB;

final class Functions
{
	/**
	 * Devuelve una fecha en formato local
	 *
	 * @param  string $fecha Fecha en ingles
	 * @return string Fecha en español dd-mm-yyyy
	 */
	public static function invertirFecha(String $fecha): String
	{
		return date('d-m-Y', strtotime($fecha));
	}

	/**
	 * Obtener el nombre de un mes segun su numero
	 *
	 * @param  string $numMes Numero relacionado al mes
	 * @return string Descripcion del mes
	 */
	public static function getNombreMes(String $numMes): String
	{
		return ($numMes > 0 && $numMes <= count(Structures::$meses)) ? Structures::$meses[$numMes] : null;
	}

    /**
     * Eliminar los acentos de una cadena
     *
     * @param  string $cadena
     * @return string
     */
    public static function eliminarAcentos(String $cadena): String
    {
        $tofind = "ÀÁÂÄÅàáâäÒÓÔÖòóôöÈÉÊËèéêëÇçÌÍÎÏìíîïÙÚÛÜùúûüÿÑñ";
        $replac = "AAAAAaaaaOOOOooooEEEEeeeeCcIIIIiiiiUUUUuuuuyNn";

        return utf8_encode(strtr(utf8_decode($cadena), utf8_decode($tofind), $replac));
    }

    /**
     * Corregir los nombres en mayusculas a minusculas con sus iniciales en mayusculas
     *
     * @param  string $name
     * @return string
     */
    public static function fixNameUpperCase(String $name): String
    {
        return ucwords(strtolower(trim($name)));
    }

    /**
	 * Obtener el primer dia del mes especificado
	 *
	 * @param  string $fecha
	 * @return string
	 */
    public static function getFirstDayMonth(String $fecha): String
    {
        $month = date('m', strtotime($fecha));
        $year = date('Y', strtotime($fecha));

        return date('Y-m-d', mktime(0,0,0, $month, 1, $year));
    }

    /**
     * Obtener el ultimo dia del mes especificado
     *
     * @param string $fecha
     * @return string
     */
    public static function getLastDayMonth(String $fecha): String
    {
      	$month = date('m', strtotime($fecha));
        $year = date('Y', strtotime($fecha));
        $day = date("d", mktime(0,0,0, $month+1, 0, $year));

        return date('Y-m-d', mktime(0,0,0, $month, $day, $year));
    }

    /**
     * Obtener en forma descriptiva el estado del registro
     *
     * @param  integer $reg
     * @return string
     */
    public static function getEstatusReg(Int $reg): Int
    {
        return ($reg) ? Constants::NAME_STATUS_ACTIVE : Constants::NAME_STATUS_INACTIVE;
    }

  /**
   * Reparar fecha a formato de base de datos
   *
   * @param  string $fechaInicio
   * @return string
   */
    public static function fixFechaInicio(String $fechaInicio): String
    {
        return date('Y-m-d H:i:s', strtotime(str_replace('/', '-', $fechaInicio)));
    }

    /**
     * Reparar fecha a formato de base de datos
     *
     * @param  string $fechaFin
     * @return string
     */
    public static function fixFechaFin(String $fechaFin): String
    {
        $fechaFin = date('Y-m-d H:i:s', strtotime(str_replace('/', '-', $fechaFin)));

        return date('Y-m-d H:i:s', strtotime('-1 second', strtotime($fechaFin)));
    }

    /**
     * Convertir un entero simple a compuesto con ceros a la derecha
     *
     * @param  integer $value
     * @return integer
     */
    public static function convertIntegerToZeroFill(Int $value): Int
    {
        return str_pad($value, 7, '0', STR_PAD_LEFT);
    }

    /**
     * Convertir un entero simple a compuesto con ceros a la derecha en especifico para la facturas
     *
     * @param  integer $value
     * @return integer
     */
    public static function convertIntegerToZeroFillFactura(Int $value): Int
    {
        return str_pad($value, 6, '0', STR_PAD_LEFT);
    }

    /**
     * Convertir a entero con ceros a la derecha a uno simple
     * 
     * @param  int $value
     * @return int
     */
    public static function convertZeroFillToInteger(String $value): Int
    {
        if($value)
        {
            //Convert to string
            $value = (String) $value;

            //Convert String to array for each caracters
            $arrayValue = str_split($value);

            $pos = false;
            $pivot = 0;

            while($pos === false)
            {
                if($arrayValue[$pivot] != 0)
                {
                    $pos = $pivot;
                }

                $pivot++;
            }


            return (Integer) substr($value, $pos);
        }

        return null;
    }

    /**
     * Obtener el id del ultimo registro de una tabla
     * 
     * @param  string $tablename
     * @return int
     */
    public static function getLastIdTable(String $tablename): Int
    {
        $data = DB::table($tablename)->orderBy('id', 'DESC')->first();

        return (!empty($data)) ? $data->id : null;
    }

    /**
     * Convertir ruta de servidor a ruta path del SO
     * 
     * @param  string $url
     * @return string
     */
    public static function convertUrlToPath(String $url): String
    {
        return str_replace('/', '\\', $url);
    }

    /**
     * Corregir caracteres especiales usados por los plugins para los montos a uno correcto para el lenguaje
     * 
     * @param  string $value
     * @return string
     */
    public static function fixMoneyValue(String $value): String
    {
        $value = str_replace('.', '', $value);

        return (substr($value, 0, 1) === '$') ? substr($value, 1) : $value;
    }

    /**
     * Convertir valor numerico en money con formato EN_US
     * 
     * @param  float  $value
     * @param  string  $symbol
     * @param  integer $r
     * @return float
     */
    public static function convertToMoney(Float $value, String $symbol = '$', Int $r = 2): Float
    {
        $n = $value;
        $c = is_float($n) ? 1 : number_format($n, $r);
        $d = '.';
        $t = ',';

        $sign = ($n < 0) ? '-' : '';
        $i = $n = number_format(abs($n), $r);
        $j = (($j = strlen($i)) > 2) ? $j % 2 : 0;

        return $symbol . $sign . ($j ? substr($i, 0, $j) + $t : '').preg_replace('/(\d{3})(?=\d)/', "$1" + $t, substr($i, $j));
    }

    /**
     * Formatear una fecha a descripcion completa
     * 
     * @param  string $date
     * @return string
     */
    public static function formatFullTextDate(String $date): String
    {
        $fullDate = Carbon::parse($date);
        
        return $fullDate->format("d") . ' de ' . Structures::$meses[$fullDate->format("m")] . ' ' . $fullDate->format('Y');
    }

    /**
     * Genera orden WebPay
     * 
     * @param  integer $length
     * @return string
     */
    public static function generaOrdenWebPay(Int $length = 15): String
    {    
        $characters = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $randomString = '';

        for ($i = 0; $i < $length; $i++) {  
            $randomString .= $characters[rand(0, strlen($characters) - 1)];
        }    

        return $randomString;  
    }

    /**
     * Obtener una cadena aleatorio de acuerdo a una longitud especifica
     * 
     * @param  integer $length
     * @return mixed
     */
    public static function generateRandomString(Int $length = 15): String
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyz';
        $randomString = '';
    
        for ($i = 0; $i < $length; $i++)
        {
            $randomString .= $characters[rand(0, strlen($characters) - 1)];
        }
        
        return $randomString;
    }

    /**
     * Validar si una fecha dada cumple los requisitos del formato dd-mm-yyyy
     * 
     * @param  string $date
     * @param  string $format
     * @return bool
     */
    public static function isValidateDate(String $date, String $format = 'Y-m-d'): Bool
    {
        $d = DateTime::createFromFormat($format, $date);
        // The Y ( 4 digits year ) returns TRUE for any integer with any number of digits so changing the comparison from == to === fixes the issue.
        return $d && $d->format($format) === $date;
    }

    /**
     * Calcular la edad segun una fecha indicada
     *
     * @param string $date
     * @return String
     */
    public static function getAgeByDate($date) : String 
    {
        //Get the current UNIX timestamp.
        $now = time();
        //Get the timestamp of the person's date of birth.
        $dob = strtotime($date);
        //Calculate the difference between the two timestamps.
        $difference = $now - $dob;
        //There are 31556926 seconds in a year.
        $age = floor($difference / 31556926);

        return $age;
    }

    /**
     * Validate is a image extension
     *
     * @param String $value
     * @return Bool
     */
    public static function isImageExtension(String $value): Bool
    {
        return in_array($value, ['jpg', 'png', 'jpeg', 'gif']);
    }

    /**
     * Validate is a video extension
     *
     * @param String $value
     * @return Bool
     */
    public static function isVideoExtension(String $value): Bool
    {
        return in_array($value, ['mp4', 'mov', 'ogg']);
    }

    /**
     * Validate is a mobile client
     *
     * @return boolean
     */
    public static function isMobile() 
    {
        $userAgent = $_SERVER['HTTP_USER_AGENT'];
        $mobileKeywords = ['Mobile', 'Android', 'Silk/', 'Kindle', 'BlackBerry', 'Opera Mini', 'Opera Mobi'];
    
        foreach ($mobileKeywords as $keyword) {
            if (stripos($userAgent, $keyword) !== false) {
                return true;
            }
        }
    
        return false;
    }
}

<?php

namespace App\Globals;

/**
 * Contiene las alertas agrupadas por cada caso
 */

final class Alerts
{
	/**
	 * Generales para formularios
	 */
	const CREATE_SUCCESS = 'Registro creado correctamente';
	const UPDATE_SUCCESS = 'Registro modificado correctamente';
	const CONFIRM_DELETE = '¿Estás seguro que desea eliminar el registro seleccionado?';
	const DELETE_SUCCESS = 'Registro eliminado correctamente';
	const SUSPEND_SUCCESS = 'Registro suspendido correctamente';
	const ACTIVATE_SUCCESS = 'Registro activado correctamente';
	const CANCEL_SUCCESS = 'Registro anulado correctamente';

	/**
	 * Contact
	 */
	const SEND_CONTACT_SUCCESS = 'Su mensaje ha sido enviado correctamente. Pronto nos contactaremos con usted!';

	/**
	 * Clientes
	 */
	const CLIENT_SUCCESS = 'Sus han sido registrados correctamente. Pronto será redirigido a la sección de confirmación.';
	const CLIENT_ERROR = 'Error al momento de registrar sus datos. Le pedimos disculpas e intente nuevamente con su proceso de registro';

	/**
	 * Pedidos
	 */
	const INVOICE_APPROVED = 'Factura ha sido aprobada correctamente';
	const INVOICE_REJECTED = 'Factura ha sido rechazada correctamente';
	const INVOICE_CANCELED = 'Factura ha sido anulada correctamente';

	/**
	 * Perfil Usuario
	 */
	const UPDATE_PROFILE_SUCCESS = 'Datos personales modificados correctamente';
	const UPDATE_PASSWORD_SUCCESS = 'Clave de usuario modificada correctamente';

	/**
	 * Bases de Datos
	 */
	const ERROR_EXCEPTION_DB = 'Error en base de datos.';

	/**
	 * Correos Electronicos
	 */
	const ENVIO_EMAIL_SUCCESS = 'Correo eléctronico enviado sastifactoriamente';
	const ENVIO_EMAIL_ERROR = 'Problemas al enviar correo eléctronico';

	/**
	 * Errores Sistema
	 */
	const ERROR_403 = 'Permisos insuficientes para realizar dicha acción.';
	const ERROR_404 = 'Página solicitada no encontrada';
	const ERROR_505 = 'Error interno del servidor. Intente de nuevo';
}
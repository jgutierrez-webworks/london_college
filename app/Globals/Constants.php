<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Globals;

/**
 * Description of Constantes
 *
 */
final class Constants 
{	
    /**
     * General
     */
    const STATUS_ACTIVE = 1;
    const NAME_STATUS_ACTIVE = 'Activo';
    const NAME_STATUS_INACTIVE = 'Inactivo';
    const LIMIT_RECORDS_TABLE = 50;
    const LIMIT_LATEST_ROWS_TABLE = 6;
    const LIMIT_CHARACTERS_DESCRIPTION_FEATURED_NEWS = 75;
    const LIMIT_CHARACTERS_DESCRIPTION_NEWS = 300;
    const NAME_DEFAULT_COMPANY = 'LONDON_COLLEGE';

    /**
     * Login
     */
    const NAME_FLAG_SITE = 'site';

    /**
     * Version
     */
    const VERSION_SYSTEM = '1.0.0';

    /**
     * Roles
     */
    const NAME_ROLE_SUPERADMINISTRATOR = 'superadministrador';
    const NAME_ROLE_ADMINISTRATOR = 'administrador';
    const NAME_ROLE_MODERATOR = 'moderador';
    const NAME_ROLE_CLIENT = 'cliente';
}
@extends('admin.emails.template.base')

@section('content')
    
    <tr style="border-collapse:collapse">
        <td style="padding:0;Margin:0;padding-left:20px;padding-right:20px;padding-top:40px;background-color:transparent" bgcolor="transparent" align="left">
            <table width="100%" cellspacing="0" cellpadding="0" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px">
                <tr style="border-collapse:collapse">
                    <td valign="top" align="center" style="padding:0;Margin:0;width:560px">
                        <table style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;background-position:left top" width="100%" cellspacing="0" cellpadding="0" role="presentation">
                            <tr style="border-collapse:collapse">
                                <td align="center" style="padding:0;Margin:0;padding-top:5px;padding-bottom:5px;font-size:0">
                                    <img src="https://hdfqgy.stripocdn.email/content/guids/CABINET_dd354a98a803b60e2f0411e893c82f56/images/23891556799905703.png" alt="Reset Password" style="display:block;border:0;outline:none;text-decoration:none;-ms-interpolation-mode:bicubic" width="175">
                                </td>
                            </tr>
                            <tr style="border-collapse:collapse">
                                <td align="center" style="padding:0;Margin:0;padding-top:15px;padding-bottom:15px">
                                    <h1 style="Margin:0;line-height:24px;mso-line-height-rule:exactly;font-family:arial, 'helvetica neue', helvetica, sans-serif;font-size:20px;font-style:normal;font-weight:normal;color:#333333">
                                        <strong>OLVIDASTE TU CONTRASEÑA?</strong>
                                    </h1>
                                </td>
                            </tr>
                            <tr style="border-collapse:collapse">
                                <td align="center" style="padding:0;Margin:0;padding-left:40px;padding-right:40px">
                                    <p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:helvetica, 'helvetica neue', arial, verdana, sans-serif;line-height:24px;color:#555555;font-size:16px">
                                        Hola,&nbsp;{{ $user->name }}
                                    </p>
                                </td>
                            </tr>
                            <tr style="border-collapse:collapse">
                                <td align="center" style="padding:0;Margin:0;padding-top:25px;padding-right:35px;padding-left:40px">
                                    <p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:helvetica, 'helvetica neue', arial, verdana, sans-serif;line-height:24px;color:#555555;font-size:16px;text-align:justify">
                                        Recibió este correo electrónico porque recibimos una solicitud de restablecimiento de contraseña para su cuenta.
                                    </p>
                                </td>
                            </tr>
                            <tr style="border-collapse:collapse">
                                <td align="center" style="padding:0;Margin:0;padding-top:25px;padding-left:40px;padding-right:40px">
                                    <p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:helvetica, 'helvetica neue', arial, verdana, sans-serif;line-height:24px;color:#555555;font-size:16px;text-align:justify">
                                        Si no realizó esta solicitud, simplemente ignore este correo electrónico. De lo contrario, haga clic en el botón de abajo para cambiar su contraseña:
                                    </p>
                                </td>
                            </tr>
                            <tr style="border-collapse:collapse">
                                <td align="center" style="Margin:0;padding-left:10px;padding-right:10px;padding-top:30px;padding-bottom:20px">
                                    <span class="es-button-border es-button-border-1" style="border-style:solid;border-color:#2980B9;background:#FFFFFF;border-width:2px;display:inline-block;border-radius:10px;width:auto">
                                        <a href="{{ url(config('app.url') . '/intranet/reset-password/' . $token) }}" target="_blank" class="es-button" style="mso-style-priority:100 !important;text-decoration:none;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;color:#2980B9;font-size:14px;border-style:solid;border-color:#FFFFFF;border-width:15px 20px 15px 20px;display:inline-block;background:#FFFFFF;border-radius:10px;font-family:arial, 'helvetica neue', helvetica, sans-serif;font-weight:bold;font-style:normal;line-height:17px;width:auto;text-align:center">
                                            Restablecer Contraseña
                                        </a>
                                    </span>
                                </td>
                            </tr>
                            <tr style="border-collapse:collapse">
                                <td align="center" style="padding:0;Margin:0;padding-top:20px;padding-left:40px;padding-right:40px">
                                    <p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:helvetica, 'helvetica neue', arial, verdana, sans-serif;line-height:24px;color:#555555;font-size:16px;text-align:justify">
                                        Este enlace de restablecimiento de contraseña caducará en {{ config('auth.passwords.users.expire') }} minutos.
                                    </p>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>

@endsection

@section('subcopy')
    
    <table class="es-footer es-visible-simple-html-only" cellspacing="0" cellpadding="0" align="center" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;table-layout:fixed !important;width:100%;background-color:transparent;background-repeat:repeat;background-position:center top">
        <tr style="border-collapse:collapse">
            <td class="es-stripe-html" style="padding:0;Margin:0;background-color:#FAFAFA" bgcolor="#fafafa" align="center">
                <table class="es-footer-body" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;background-color:transparent;width:600px" cellspacing="0" cellpadding="0" bgcolor="transparent" align="center">
                    <tr style="border-collapse:collapse">
                        <td align="left" style="Margin:0;padding-bottom:5px;padding-top:15px;padding-left:20px;padding-right:20px">
                            <table width="100%" cellspacing="0" cellpadding="0" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px">
                                <tr style="border-collapse:collapse">
                                    <td valign="top" align="center" style="padding:0;Margin:0;width:560px">
                                        <table width="100%" cellspacing="0" cellpadding="0" role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px">
                                            <tr style="border-collapse:collapse">
                                                <td align="center" style="padding:0;Margin:0">
                                                    <p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:helvetica, 'helvetica neue', arial, verdana, sans-serif;line-height:18px;color:#555555;font-size:12px">
                                                        Si tiene problemas para hacer click en el botón "Restablecer Contraseña", copia y pega la URL a continuación en tu navegador web: 
                                                        <a href="{{ url(config('app.url') . '/intranet/reset-password/' . $token) }}" target="_blank" style="-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;text-decoration:underline;color:#333333;font-size:12px" class="unsubscribe">
                                                            {{ url(config('app.url') . '/intranet/reset-password/' . $token) }}
                                                        </a>.
                                                    </p>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>

@endsection
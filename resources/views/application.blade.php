<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width,initial-scale=1.0">
	<meta name="csrf-token" content="{{ csrf_token() }}">
	<!-- <link rel="icon" href="<%= BASE_URL %>favicon.ico"> -->

	<title>{{ env('APP_NAME', '') }}</title>
	<!-- Styles -->
	<link rel="stylesheet" href="{{ asset(mix('css/app.css')) }}">

	<!-- Favicon -->
	<link rel="shortcut icon" href="{{ asset('images/logo/favicon.png') }}">

	@routes
</head>
<body>
	<noscript>
		<strong>
			Lo sentimos, la plantilla del panel de administración de Vuejs no funciona correctamente sin JavaScript habilitado. Por favor, activelo para continuar.
		</strong>
	</noscript>

	<div id="app">
	</div>

	<script src="{{ asset(mix('js/app.js')) }}"></script>
</body>
</html>

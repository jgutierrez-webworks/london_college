@extends('site.template.base')

@section('page_title')
	
	<section id="page-title" class="page-title-parallax page-title-dark" style="background-image: url({{ asset('assets/site/images/bg-news.jpg') }}); background-size: cover; padding: 120px 0;" data-bottom-top="background-position:0px 0px;" data-top-bottom="background-position:0px -300px;">
		<div class="container clearfix">
			<h1>Listado de Noticias</h1>
			<span>&nbsp;</span>
			<ol class="breadcrumb">
				<li class="breadcrumb-item"><a href="{{ route('site.index') }}">Inicio</a></li>
				<li class="breadcrumb-item active" aria-current="page">Noticias</li>
			</ol>
		</div>
	</section>

@endsection

@section('content')
    
    <div class="content-wrap">
		<div class="container clearfix">
			<!-- Post Content
			============================================= -->
			<div class="postcontent nobottommargin col_last clearfix">				
				<news-component />
			</div><!-- .postcontent end -->
			<!-- Sidebar
			============================================= -->
			<div class="sidebar nobottommargin clearfix">
				<sidebar-component filter="news" />
			</div><!-- .sidebar end -->
		</div>
	</div>

@endsection
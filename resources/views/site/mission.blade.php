@extends('site.template.base')

@section('page_title')
	
	<section id="page-title" class="page-title-parallax page-title-dark" style="background-image: url({{ asset('assets/site/images/bg-news.jpg') }}); background-size: cover; padding: 120px 0;" data-bottom-top="background-position:0px 0px;" data-top-bottom="background-position:0px -300px;">
		<div class="container clearfix">
			<h1>Misión - Visión</h1>
			<span>&nbsp;</span>
			<ol class="breadcrumb">
				<li class="breadcrumb-item"><a href="{{ route('site.index') }}">Inicio</a></li>
				<li class="breadcrumb-item active" aria-current="page">Misión - Visión</li>
			</ol>
		</div>
	</section>

@endsection

@section('content')
    
    <div class="content-wrap">
		<div class="container clearfix">
			<!-- Post Content
			============================================= -->
			<div class="postcontent nobottommargin col_last clearfix">				
				<div class="row">
                    <div class="col">
                        <h2>Misión</h2>
                        <p class="text-justify">
                            Entregar a las y los estudiantes una educación acogedora y familiar que
                            fomente la creatividad, el trabajo en equipo y la perseverancia, mediante una
                            formación altamente proactiva, participativa y comprometida, que les permita
                            desarrollar habilidades y valores, destacándose como personas capaces de
                            respetar el medio ambiente e integrarse a la sociedad, aportando
                            significativamente a ella.
                        </p>
                    </div>
                </div>
                <div class="row">
                    <div class="col">
                        <h2>Visión</h2>
                        <p class="text-justify">
                            Llegar a ser una comunidad educativa reconocida por sus sellos y
                            formación integral, sustentada en la calidad, excelencia e innovación,
                            fomentando en las y los estudiantes la creatividad, el trabajo en equipo y la
                            perseverancia.
                        </p>
                    </div>
                </div>
			</div><!-- .postcontent end -->
            <!-- Sidebar
			============================================= -->
			<div class="sidebar nobottommargin clearfix">
				<sidebar-component filter="inside" />
			</div><!-- .sidebar end -->
        </div>
	</div>

@endsection
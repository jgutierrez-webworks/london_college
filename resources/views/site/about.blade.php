@extends('site.template.base')

@section('page_title')
	
	<section id="page-title" class="page-title-parallax page-title-dark" style="background-image: url({{ asset('assets/site/images/bg-news.jpg') }}); background-size: cover; padding: 120px 0;" data-bottom-top="background-position:0px 0px;" data-top-bottom="background-position:0px -300px;">
		<div class="container clearfix">
			<h1>Nuestro Colegio</h1>
			<span>&nbsp;</span>
			<ol class="breadcrumb">
				<li class="breadcrumb-item"><a href="{{ route('site.index') }}">Inicio</a></li>
				<li class="breadcrumb-item active" aria-current="page">Nuestro Colegio</li>
			</ol>
		</div>
	</section>

@endsection

@section('content')
    
    <div class="content-wrap">
		<div class="container clearfix">
			<!-- Post Content
			============================================= -->
			<div class="postcontent nobottommargin col_last clearfix">
				<div class="row">
                    <div class="col">
                        <p class="text-justify">
                            El Centro Educacional London, es una Institución Educativa particular
                            subvencionada, polivalente, que funciona, en San José de la Estrella 603.
                            Comuna de La Florida, desde Marzo de 1990, como anexo de la casa Matriz
                            Centro Educacional London creado en el año 1981 como Escuela Básica.
                        </p>
                        <p class="text-justify">
                            El Centro Educacional London, define su identidad bajo una visión
                            humanista laica, entregando las bases que sustentan nuestra labor Educativa
                            de acuerdo a la LEGE. Así contribuimos a la formación integral del Educando
                            que libremente opte a nuestro estilo de Educación (Humanístico - Científica o
                            Técnica). Nuestro Centro Educativo atiende una importante cantidad de
                            alumnos vulnerables provenientes de sectores con riesgo social, caracterizados
                            por la falta de oportunidades, y bajo nivel educacional de los padres.
                        </p>
                        <p class="text-justify">
                            Atendemos una población total de 550 alumnos, distribuidos en
                            Enseñanza Media Técnico Profesional, donde dictamos 3 especialidades que
                            corresponden al área técnica Gastronomía mención cocina y el área comercial
                            Administración mención Recursos Humanos y Contabilidad. Contamos.
                        </p>
                        <p class="text-justify">
                            <b>El Cuerpo Docente y Administrativos está formado por:</b>
                            <ul class="pl-5">
                                <li>55 Docentes</li>
                                <li>2 Monitores</li>
                                <li>5 Inspectores</li>
                                <li>7 Administrativos</li>
                                <li>6 Auxiliares</li>
                            </ul>
                        </p>                
                    </div>
                </div>
			</div><!-- .postcontent end -->
			<!-- Sidebar
			============================================= -->
			<div class="sidebar nobottommargin clearfix">
				<sidebar-component filter="inside" />
			</div><!-- .sidebar end -->
		</div>
	</div>

@endsection
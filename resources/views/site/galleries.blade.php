@extends('site.template.base')

@section('page_title')
	
	<section id="page-title" class="page-title-parallax page-title-dark d-none" style="background-image: url({{ asset('assets/site/images/bg-news.jpg') }}); background-size: cover; padding: 120px 0;" data-bottom-top="background-position:0px 0px;" data-top-bottom="background-position:0px -300px;">
		<div class="container clearfix">
			<h1>Galería</h1>
			<span>Sección de los eventos más destacados de nuestra institución</span>
			<ol class="breadcrumb">
				<li class="breadcrumb-item"><a href="{{ route('site.index') }}">Inicio</a></li>
				<li class="breadcrumb-item active" aria-current="page">Galería</li>
			</ol>
		</div>
	</section>

@endsection

@section('content')
	
    <div class="content-wrap content-wrap-gallery nobottompadding">		
		<div id="portfolio" class="portfolio grid-container portfolio-nomargin portfolio-full clearfix {{ (!Functions::isMobile()) ? 'portfolio-notitle' : '' }}">
			@foreach($galleries as $gallery)

				@if($gallery->files->count() === 0)
					@continue
				@endif
			
				@php
					$firstFile = $gallery->files->first();					
				@endphp

				<article class="portfolio-item pf-media pf-icons" data-lightbox="gallery">
					<div class="portfolio-image" style="background: url({{ asset('storage/galeria/' . $gallery->codigo . '/' . $firstFile->nombre) }}) center center / cover; min-height: 300px; border: 7px #fff solid;">
						<div class="portfolio-overlay">
							<a href="{{ asset('storage/galeria/' . $gallery->codigo . '/' . $firstFile->nombre) }}" class="left-icon" data-lightbox="gallery-item"><i class="icon-line-stack-2"></i></a>
							@foreach($gallery->files as $file)
								@if($loop->index !== 0)
									<a href="{{ asset('storage/galeria/' . $gallery->codigo . '/' . $file->nombre) }}" class="d-none" data-lightbox="gallery-item"></a>
								@endif								
							@endforeach
						</div>
					</div>
					<div class="portfolio-desc">
						@if(Functions::isMobile())
							<a href="{{ asset('storage/galeria/' . $gallery->codigo . '/' . $firstFile->nombre) }}" class="left-icon lead text-center mb-2" data-lightbox="gallery-item"><i class="icon-line-stack-2"></i>&nbsp;Ver Fotos</a>
						@endif						
						<h3>{{ $gallery->nombre }}</h3>
						<span>{{ Structures::$meses[$gallery->fecha->format('m')] . ' ' . $gallery->fecha->format('Y') }} - {{ $gallery->lugar }}</span>
					</div>
				</article>
			@endforeach
	
			
		</div><!-- #portfolio end -->
	</div>

@endsection
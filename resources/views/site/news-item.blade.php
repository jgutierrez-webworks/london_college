@extends('site.template.base')

@section('styles')
	@parent
	<style>
		#page-title h1 {
			line-height: 1.5;
		}
	</style>
@endsection

@section('page_title')
	
	<section id="page-title" class="page-title-parallax page-title-dark" style="background-image: url({{ asset('assets/site/images/bg-news.jpg') }}); background-size: cover; padding: 100px 0;" data-bottom-top="background-position:0px 0px;" data-top-bottom="background-position:0px -300px;">
		<div class="container clearfix">
			<div class="row">
				<div class="col-md-10 offset-md-1">
					<h1 class="text-center">&nbsp;</h1>
					<span>&nbsp;</span>
				</div>				
			</div>									
		</div>
	</section>

@endsection

@section('content')
    
	<div class="content-wrap">
		<div class="container clearfix">
			<!-- Post Content
			============================================= -->		
			<div class="postcontent nobottommargin col_last clearfix">
				<news-item-component slug="{{ $slug }}" />
			</div><!-- .postcontent end -->
			<!-- Sidebar
			============================================= -->
			<div class="sidebar nobottommargin clearfix">
				<sidebar-component filter="news" />
			</div><!-- .sidebar end -->
		</div>
	</div>

@endsection
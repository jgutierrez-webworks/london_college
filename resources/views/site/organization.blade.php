@extends('site.template.base')

@section('page_title')
	
	<section id="page-title" class="page-title-parallax page-title-dark" style="background-image: url({{ asset('assets/site/images/bg-news.jpg') }}); background-size: cover; padding: 120px 0;" data-bottom-top="background-position:0px 0px;" data-top-bottom="background-position:0px -300px;">
		<div class="container clearfix">
			<h1>Organigrama</h1>
			<span>&nbsp;</span>
			<ol class="breadcrumb">
				<li class="breadcrumb-item"><a href="{{ route('site.index') }}">Inicio</a></li>
				<li class="breadcrumb-item active" aria-current="page">Organigrama</li>
			</ol>
		</div>
	</section>

@endsection

@section('content')
    
    <div class="content-wrap">
		<div class="container clearfix">
			<!-- Post Content
			============================================= -->
			<div class="postcontent nobottommargin col_last clearfix">				
				<div class="row">
					<div class="col">
						<p class="text-justify">
							La organización del Establecimiento se fundamenta dentro de un marco
							axiológico de la Política Educacional vigente, perfilándose en el contexto
							general para el desarrollo económico y social de la nación en términos del
							sistema Educativo.
						</p>
						<h4 class="text-justify">
							Entidad Titular. Perfiles.
						</h4>
						<p class="text-justify">
							<b>Director:</b> Es el docente responsable de la planificación, organización,
							supervisión y funcionamiento del quehacer educativo. Es el representante del
							Establecimiento ante las autoridades Educacionales correspondientes y el
							responsable de la toma de decisiones, de crear los canales de comunicación
							que lleguen oportunamente a todos los miembros de la comunidad escolar, de
							las normas legales y reglas internas vigentes supervisando su correcta
							aplicación.
							<br><br>
							● Promover el diálogo y la participación de todos los miembros de la
							comunidad educativa en la elaboración, realización y elaboración del
							proyecto educativo, asegurando que los objetivos estén de acuerdo con los
							requerimientos de la comunidad educativa y en concordancia con los
							objetivos de la educación nacional
							<br>
							● Apoyar la formación permanente y la calificación del personal a su cargo.
							<br>
							● Presidir los encuentros del equipo de gestión.
							<br>
							● Comunicar al organismo respectivo, su ausencia del Establecimiento y el
							nombre de quien le sustituye en el ejercicio de su cargo.
							<br>
							● Informar oportunamente al personal de su dependencia de las normas
							legales y reglamentarias vigentes, en especial aquellos referidos a planes y
							programas de estudios, sistema de evaluación y formación escolar
							supervisando su correcta aplicación.
							<br>
							● Proveer del material didáctico necesario y adecuado al nivel y modalidad
							de enseñanza que imparte el establecimiento.
						</p>
						<p class="text-justify">
							<b>Equipo de Gestión:</b> Es un estamento formado por los docentes que ocupan
							cargos directivos dentro del Establecimiento: Directora, Inspectora General,
							Jefe Técnico, sesionará de acuerdo a como el director estime conveniente,
							siendo el quien lo convoca y preside.
							<br><br>
							● Cada integrante debe asumir y comprometerse con la realización del
							proyecto educativo de la Institución
							<br>
							● Debe proponer y coordinar las diferentes áreas de trabajo en las
							perspectivas Educativas que ellas proponen
							<br>
							● Incentivar y proponer iniciativas que ayuden la actualización de la labor
							Educativa.
							<br>
							● Evaluar periódicamente las actividades del plan de acción del
							Establecimiento.
						</p>
						<p class="text-justify">
							<b>Unidad Técnico pedagógica:</b> El Docente directivo responsable de coordinar el
							desarrollo de las actividades curriculares del Establecimiento.
							<br>
							● Programar, Organizar, supervisar y evaluar las actividades
							correspondientes del proceso Enseñanza Aprendizaje.
							<br>
							● Velar por el rendimiento escolar de los alumnos, procurando el
							mejoramiento continuo del proceso Enseñanza Aprendizaje.
							<br>
							● Propiciar la integración entre los diferentes programas de estudio, de los
							diferentes programas de estudio.
							<br>
							● Contribuir al perfeccionamiento del personal docente del Establecimiento
							en materias de evaluación y curriculum.
							<br>
							● Dirigir los consejos técnicos que le competen.
							<br>
							● Planificar, supervisar y evaluar los planes y programas, acordes a las
							necesidades y conforme a las normas vigentes.
							<br>
							● Informarse permanentemente, de los cambios que va experimentando la
							educación en sus distintas áreas y hacer partícipe, oportunamente quienes
							corresponda.
						</p>
						<p class="text-justify">
							Inspectoría General: Es el Docente Directivo responsable de las actividades
							del Establecimiento Educacional y que se desarrollen en sana convivencia y en
							un ambiente de disciplina
							<br><br>
							● Aplicar, supervisar y apoyar el cumplimiento del reglamento interno.
							<br>
							● Verificar que los alumnos concurran con responsabilidad a cumplir en
							forma oportuna con sus actividades escolares.
							<br>
							● Cuidar la asistencia y responsabilidad del profesor para tomar y dejar sus
							cursos con puntualidad, en cada una de las horas de clases que les
							corresponda.
							<br>
							● Controlar mediante la utilización de los procedimientos adecuados las
							inasistencias, atrasos, justificaciones, certificados médicos y otros de la
							rutina escolar.
							<br>
							● Controlar diariamente justificativos de inasistencia de los alumnos a clase.
							<br>
							● Comprobar que todo alumno que ingresa fuera del horario de se le registre
							en un cuaderno de control de atrasos.
							<br>
							● Disponer oportunamente de guías de trabajos para ser aplicados por los
							profesores en las horas que correspondan.
							<br>
							● Informar y delegar al profesor jefe las situaciones reiteradas de
							inasistencias y atrasos
							<br>
							● Supervisar las funciones delegadas a los asistentes de la Educación
							inspectores y personal de servicios menores
							<br>
							● Colaborar a los Docentes en el control de las citaciones de los apoderados
							cuando corresponda.
							<br>
							● Velar por el correcto cumplimiento administrativo controlando horarios,
							atrasos, inasistencia del todo el personal de su dependencia.
							25
							<br>
							● Autorizar la salida a terreno de los alumnos y docentes.
							<br>
							● Supervisar el cumplimiento de la labor Docente.
							<br>
							● Velar por el cuidado y mantención de los materiales e Infraestructura del
							Establecimiento
						</p>
						<p class="text-justify">
							Docentes: Profesor titulado y acreditado para ejercer la Docencia de acuerdo
							con las disposiciones legales que al respecto señala el Ministerio de Educación
							de Chile, siendo el responsable directo de conducir el Proceso de Enseñanza
							de Aprendizaje, fomentar principios morales, sociales y humanos en sus
							Educando, ejecutando sistemáticamente las horas de clases y funciones
							estipuladas en cada uno de sus respectivos contratos.
							<br><br>
							● Fomentar, internalizar y proyectar en sus alumnos valores, hábitos, a través
							del ejemplo personal y del desarrollo de la disciplina, como medio para
							adquirir una autodisciplina constructiva.
							<br>
							● Participar a todos los consejos que por sus funciones sea citado: Técnico,
							General, Profesores Jefes, etc.
							<br>
							● Cumplir cabalmente con su horario de trabajo y con las disposiciones de
							índole técnica que sean establecidas en cada unidad Educativa.
							<br>
							● Entregar en forma oportuna la documentación que sea requerida,
							respetando fechas indicaciones emanadas de la coordinación de desarrollo
							pedagógico.
							<br>
							● Innovar las estrategias metodológicas que permitan motivar al alumno en
							su diario aprender.
							<br>
							● Preparar al estudiante físico, moral e intelectualmente para enfrentarse a la
							vida adulta.
							<br>
							● Asistir y participar activamente a reuniones de Reflexión Pedagógica,
							según plan operativo, anual y carga horaria.
							<br>
							● Realizar un seguimiento pedagógico y conductual constante de los
							alumnos a su cargo.
							<br>
							● Entregar conocimientos teórico- práctico acorde con los principios
							filosóficos del colegio y los cambios educativos del país.
							<br>
							● Participar en las actividades programadas por la comunidad Escolar tales
							como: Revista de Gimnasia, Gala Folclórica, Licenciaturas, Actos Cívicos, etc
						</p>
						<h4 class="text-justify">
							Perfil del Docente
						</h4>
						<p class="text-justify">
							● Consistente en su labor formadora.
							<br>
							● Comprometido con los principios que sustentan el proyecto Educativo Institucional.
							<br>
							● Capaz de autoevaluarse y aceptar críticas constructivas.
							<br>
							● Capaz de superarse constantemente en su quehacer Docente.
							<br>
							● Un modelo en su actuar, justo, honesto, creativo, activo y cuidadoso en sus materiales propios como ajenos.
							<br>
							● Comprometido con las actividades extra curriculares organizadas en el establecimiento.
							<br>
							● Partícipe activo de las reuniones de reflexión pedagógica por J.E.C.
						</p>
						<p class="text-justify">
							Profesor Jefe
							<br><br>
							● Velar permanentemente por el desarrollo integral de los alumnos perteneciente a su curso.
							<br>
							● Trabajar en estrecha relación con los profesores de los subsectores de
							Aprendizaje para orientar la acción Docente hacia la conciencia del
							crecimiento integral de los alumnos.
							<br>
							● Ser agente activo, integrador y motivador del curso.
							<br>
							● Preparar el consejo de curso con los alumnos de la directiva de curso.
							<br>
							● Derivar a Inspectoría, los casos de los alumnos con problemas
							significativos y que requieran para ello de decisiones que sobrepasen su
							autoridad como profesor jefe.
							<br>
							● Preocuparse por la salud, bienestar, rendimiento, asistencia, puntualidad,
							presentación personal y otros comportamientos de los alumnos del curso,
							realizando un oportuno seguimiento y atención individual.
							<br>
							● Dejar constancia en el libro de clases de cada una de las entrevistas
							realizadas con los apoderados avalados por su firma.
							<br>
							● Ser anexo entre las autoridades del colegio.
							<br>
							● Informar en los consejos evaluativos de profesores la situación global del
							curso y personal de cada alumno.
							<br>
							● Mantener informados a los apoderados en relación con su desempeño
							general del grupo curso y privadamente de las actuaciones personales de
							cada alumno.
							<br>
							● Mantener al día, libros de clases, evaluaciones y calificaciones de los
							alumnos, informe de personalidad, ficha escolar y otras que la dirección
							determine.
						</p>
						<p class="text-justify">
							Grupos Profesionales de Trabajo
							<br><br>
							● Fomentar la instalación de espacios regulares de trabajo colectivo Docente
							<br>
							● Desarrollar al docente en el ámbito personal social y profesional.
							<br>
							● Promover la reflexión Pedagógica sobre las prácticas cotidianas de los Docentes, el rediseño
							<br>
							● Potenciar la innovación Pedagógica.
							<br>
							● Promover el respeto por la diversidad de los estudiantes y sus contextos.
							<br>
							● Fomentar la modalidad de trabajo en grupo
							<br>
							● Fortalecer el carácter profesional de los Docentes.
							<br>
						</p>
						<p class="text-justify">
							Departamentos
							<br><br>
							● Realizar el trabajo técnico Pedagógico en concordancia con el proyecto Educativo del Establecimiento.
							<br>
							● Trabajar en forma armónica con el jefe de unidad técnica Pedagógica o con la dirección cuando así proceda.
							<br>
							● Planificar, organizar y coordinar el proceso Enseñanza Aprendizaje en coordinación con la Unidad Técnico Pedagógica.
							<br>
							● Confeccionar procedimientos Evaluativos.
							<br>
							● Evaluar periódicamente los procesos y resultados obtenidos por los Educando						
							<br>
							● Establecer un conjunto de remédiales para retroalimentar el proceso de Aprendizaje.
							<br>
							● Mantener informado sobre las innovaciones curriculares.
							<br>
							● Presentar informe anual del trabajo de los departamento a la Unidad Técnica Pedagógica
							<br>
							● Elaborar guías de trabajo para ser utilizadas en ausencia del profesor titular.
							<br>
						</p>
						<p class="text-justify">
							Jefe de Especialidad.
							<br><br>
							● Asesorar a los Docentes todo lo relativo a la Enseñanza Aprendizaje y al
							proceso de producción correspondiente a sus respectivas especialidades.
							<br>
							● Contribuir a la integración de los módulos y subsectores de Aprendizaje de
							los planes de los planes de estudio y el proceso de Titulación.
							<br>
							● Es el representante del jefe de la Unidad Técnica Pedagógica ante los
							profesores de especialidad.
							<br>
							● Asesorar en el desarrollo de la práctica profesional de los alumnos
							egresados de las diferentes especialidades, colaborando en los contactos
							con las empresas e industrias.
							<br>
							● Velar por higiene y seguridad industrial de los alumnos que laboran en los
							talleres de su área.
							<br>
							● Promover, estimular y facilitar las visitas profesionales a las empresas e
							industrias.
						</p>
						<p class="text-justify">
							<b>Asistentes de la Educación</b> Es el colaborador de las actividades Docentes y
							administrativas inherentes a las Docencia.
							<br>
							● Facilitar, apoyando la tarea educativa del colegio
							<br>
							● Vigilar el orden y comportamiento del alumnado en la biblioteca, clases,
							talleres, laboratorio, en ausencia del profesor, en el patio, casino, etc. De							
							los que serán responsables directos según distribución de turnos y
							sectores
							<br>
							● Permanecer en el Establecimiento durante los turnos que se designen.
							<br>
							● Llevar libros y registros, según la distribución de trabajo dispuesta por
							Inspectoría General.
							<br>
							● Colaborar en las actividades extraescolares, según el control que se
							ordene ejercer.
							<br>
							● Mantener contacto permanente con el Inspector General o con quien
							corresponda, para informar de sus actividades y en particular de cualquier
							situación extraordinaria.
							<br>
							● Fiscalizar la correcta presentación personal de los alumnos, procurando
							impulsar este hábito a través del ejemplo.
							<br>
							● Presentar atención de primeros auxilios a los alumnos.
						</p>
						<h4>
							Administración y Finanzas:
						</h4>
						<p class="text-justify">						
							● Mantener el Establecimiento, para su correcta funcionalidad
							<br>
							● Infraestructura (Servicios básicos, luz, agua, gas, salas de clases, mobiliario y equipo, dependencias en general )
							<br>
							● Adquisiciones de recursos materiales de oficina.
							<br>
							● Supervisar el personal a su cargo, contador, secretarias y auxiliares.
							<br>
							● Operacionalizar los recursos económicos del colegio: ingresos, egresos, movimientos bancarios.
							<br><br>
							Secretaria
							<br><br>
							● Organizar y difundir la documentación oficial del Establecimiento
							requiriendo de todos los antecedentes necesarios
							<br>
							● Atender tareas específicas de secretaria tales como: colaborar con la
							Dirección en otras áreas específicas que se le soliciten para atender
							materias operacionales del colegio
							<br>
							● Mantener actualizados archivos y registros del Establecimiento.
						</p>
					</div>
				</div>
			</div><!-- .postcontent end -->
			<!-- Sidebar
			============================================= -->
			<div class="sidebar nobottommargin clearfix">
				<sidebar-component filter="inside" />
			</div><!-- .sidebar end -->
		</div>
	</div>

@endsection
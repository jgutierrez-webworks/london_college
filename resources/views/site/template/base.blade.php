<!DOCTYPE html>
<html dir="ltr" lang="es">

@section('head')
<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<meta name="author" content="Webworks" />
	<meta name="description" content="El Centro Educacional London, es una organización de educación que proporciona una formación integral del educando." />
	<meta name="keywords" content="Centro Educacional London, college,, london, london college, educacional, organizacion, formacion integral, educando, alumnos, institucion, san jose de la estrella, la florida chile, colegio en la florida" />
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<meta name="Resource-type" content="Index">
	<meta name="Revisit-after" content="2 days">
	<meta name="robots" content="follow">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!-- Document Title
	============================================= -->
	<title>@yield('title', 'CENTRO EDUCACIONAL LONDON')</title>

	<link rel="apple-touch-icon" sizes="57x57" href="{{ asset('assets/site/images/icons/apple-icon-57x57.png') }}">
	<link rel="apple-touch-icon" sizes="60x60" href="{{ asset('assets/site/images/icons/apple-icon-60x60.png') }}">
	<link rel="apple-touch-icon" sizes="72x72" href="{{ asset('assets/site/images/icons/apple-icon-72x72.png') }}">
	<link rel="apple-touch-icon" sizes="76x76" href="{{ asset('assets/site/images/icons/apple-icon-76x76.png') }}">
	<link rel="apple-touch-icon" sizes="114x114" href="{{ asset('assets/site/images/icons/apple-icon-114x114.png') }}">
	<link rel="apple-touch-icon" sizes="120x120" href="{{ asset('assets/site/images/icons/apple-icon-120x120.png') }}">
	<link rel="apple-touch-icon" sizes="144x144" href="{{ asset('assets/site/images/icons/apple-icon-144x144.png') }}">
	<link rel="apple-touch-icon" sizes="152x152" href="{{ asset('assets/site/images/icons/apple-icon-152x152.png') }}">
	<link rel="apple-touch-icon" sizes="180x180" href="{{ asset('assets/site/images/icons/apple-icon-180x180.png') }}">
	<link rel="icon" type="image/png" sizes="192x192" href="{{ asset('assets/site/images/icons/android-icon-192x192.png') }}">
	<link rel="icon" type="image/png" sizes="32x32" href="{{ asset('assets/site/images/icons/favicon-32x32.png') }}">
	<link rel="icon" type="image/png" sizes="96x96" href="{{ asset('assets/site/images/icons/favicon-96x96.png') }}">
	<link rel="icon" type="image/png" sizes="16x16" href="{{ asset('assets/site/images/icons/favicon-16x16.png') }}">
	<link rel="manifest" href="{{ asset('assets/site/images/icons/manifest.json') }}">
	<meta name="msapplication-TileColor" content="#ffffff">
	<meta name="msapplication-TileImage" content="{{ asset('assets/site/images/icons/ms-icon-144x144.png') }}">
	<meta name="theme-color" content="#ffffff">
	
	@routes

    @section('styles')
        <!-- Stylesheets
        ============================================= -->
        <link href="https://fonts.googleapis.com/css?family=Istok+Web:400,700&display=swap" rel="stylesheet" type="text/css" />
        <link rel="stylesheet" href="{{ asset('assets/site/css/bootstrap.min.css') }}" type="text/css" />
        <link rel="stylesheet" href="{{ asset('assets/site/css/style-v20231018.min.css') }}" type="text/css" />
		<link rel="stylesheet" href="{{ asset('assets/site/css/swiper.css') }}" type="text/css" />
        <link rel="stylesheet" href="{{ asset('assets/site/css/dark.min.css') }}" type="text/css" />
        <link rel="stylesheet" href="{{ asset('assets/site/css/font-icons.min.css') }}" type="text/css" />
        <link rel="stylesheet" href="{{ asset('assets/site/css/animate.min.css') }}" type="text/css" />
        <link rel="stylesheet" href="{{ asset('assets/site/css/magnific-popup.min.css') }}" type="text/css" />
        <link rel="stylesheet" href="{{ asset('assets/site/css/custom-v20231012.min.css') }}" type="text/css" />

        <meta name="viewport" content="width=device-width, initial-scale=1" />
		<link rel="stylesheet" href="{{ asset('assets/site/css/responsive-v20231012.min.css') }}" type="text/css" />

        <link rel="stylesheet" href="{{ asset('assets/site/css/colors.php?color=0474c4') }}" type="text/css" />
        <!-- Hosting Demo Specific Stylesheet -->
        <link rel="stylesheet" href="{{ asset('assets/site/demos/course/css/fonts.css') }}" type="text/css" />
        <link rel="stylesheet" href="{{ asset('assets/site/demos/course/course.css') }}" type="text/css" />
        <!-- / -->	
        @show
</head>
@show
<body class="stretched">
	<div id="app">
		<!-- Document Wrapper
		============================================= -->
		<div id="wrapper" class="clearfix">
			<!-- Top Bar
			============================================= -->
			<div id="top-bar" class="bg-color dark">
				<div class="container clearfix">
					<div class="row justify-content-between">
						<div class="col-12 col-md-auto mr-md-auto">
						</div>
						<div class="col-12 col-md-auto pl-0">
							<ul id="top-social">
								<li><a href="https://www.facebook.com/p/London-College-Centro-Educacional-61556720519996/?locale=sl_SI" class="si-facebook" title="Facebook" target="_blank"><span class="ts-icon"><i class="icon-facebook"></i></span><span class="ts-text">Facebook</span></a></li>
								<li><a href="#" class="si-twitter" title="Twitter"><span class="ts-icon"><i class="icon-twitter"></i></span><span class="ts-text">Twitter</span></a></li>
								<li><a href="https://www.instagram.com/londoncollege_oficial/" class="si-instagram" title="Instagram" target="_blank"><span class="ts-icon"><i class="icon-instagram2"></i></span><span class="ts-text">Instagram</span></a></li>
								<li><a href="tel:+5622823235" class="si-call" title="Teléfono"><span class="ts-icon"><i class="icon-call"></i></span><span class="ts-text">+56.2282.3235</span></a></li>
								<li><a href="mailto:contacto@londoncollege.com" class="si-email3" title="Email"><span class="ts-icon"><i class="icon-envelope-alt"></i></span><span class="ts-text">contacto@londoncollege.com</span></a></li>
							</ul><!-- #top-social end -->
						</div>
					</div>
				</div>
			</div>
			<!-- Header
			============================================= -->
			<header id="header" class="header-size-sm" data-sticky-shrink="false">
				<div class="container">
					<div class="header-row">
						<nav class="navbar navbar-expand-lg p-0 m-0 w-100">
							<div id="logo">
								<a href="{{ route('site.index') }}" class="standard-logo" title="London College"><img src="{{ asset('assets/site/images/logo_london_college.png') }}" alt="Logo London College"></a>
								<a href="{{ route('site.index') }}" class="retina-logo" title="London College"><img src="{{ asset('assets/site/images/logo_london_college.png') }}" alt="Logo London College"></a>
							</div>
							<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
								<span class="icon-line-menu"></span>
							</button>
							<div class="collapse navbar-collapse align-items-end" id="navbarNav">
								<ul class="navbar-nav ml-auto">
									<li class="nav-item {{ Ekko::isActiveRoute('site.index') }}">
										<a class="nav-link" href="{{ route('site.index') }}" title="Inicio">Inicio</a>
									</li>
									<li class="nav-item dropdown {{ Ekko::isActiveRoute(['site.mission', 'site.organization', 'site.review', 'site.about']) }}">
										<a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" title="Corporación">
											Corporación
										</a>
										<div class="dropdown-menu" aria-labelledby="navbarDropdown">
											<a class="dropdown-item {{ Ekko::isActiveRoute('site.mission') }}" href="{{ route('site.mission') }}" title="Misión - Visión">Misión - Visión</a>
											<div class="dropdown-divider"></div>
											<a class="dropdown-item {{ Ekko::isActiveRoute('site.organization') }}" href="{{ route('site.organization') }}" title="Organigrama">Organigrama</a>
											<div class="dropdown-divider"></div>
											<a class="dropdown-item {{ Ekko::isActiveRoute('site.review') }}" href="{{ route('site.review') }}" title="Reseña Histórica">Reseña Histórica</a>
											<div class="dropdown-divider"></div>
											<a class="dropdown-item {{ Ekko::isActiveRoute('site.about') }}" href="{{ route('site.about') }}" title="Nuestro Colegio">Nuestro Colegio</a>
										</div>
									</li>
									<li class="nav-item dropdown {{ Ekko::isActiveRoute(['site.news', 'site.news-item']) }}">
										<a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" title="Noticias">
								    		Noticias
    									</a>
										<sections-menu-component />
									</li>
									<li class="nav-item">
										<a href="{{ asset('assets/site/resources/postulaciones-admision.pdf') }}" class="nav-link" title="Admisión" target="_blank">Admisión</a>
									</li>
									<li class="nav-item {{ Ekko::isActiveRoute('site.galleries') }}">
										<a href="{{ route('site.galleries') }}" class="nav-link" title="Galería">Galería</a>
									</li>
									<li class="nav-item dropdown">
										<a class="nav-link dropdown-toggle" href="#" id="navbarDocumentsDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" title="Documentos">
											Documentos
										</a>
										<div class="dropdown-menu" aria-labelledby="navbarDocumentsDropdown">
											<a class="dropdown-item" href="{{ asset('assets/site/resources/proyecto-educativo.pdf') }}" title="Proyecto Educativo" target="_blank">Proyecto Educativo</a>
											<div class="dropdown-divider"></div>
											<a class="dropdown-item" href="{{ asset('assets/site/resources/reglamento-de-convivencia-2023.pdf') }}" title="Reglamento de Convivencia" target="_blank">Reglamento de Convivencia</a>
											<div class="dropdown-divider"></div>
											<a class="dropdown-item" href="{{ asset('assets/site/resources/reglamento_evaluacion_2023.pdf') }}" title="Reglamento de Evaluación" target="_blank">Reglamento de Evaluación</a>											
										</div>
									</li>
									<li class="nav-item">
										<a href="#contacto" class="nav-link menu-scroll" title="Contacto">Contacto</a>
									</li>
								</ul>
							</div>
						</nav>
					</div>
				</div>
				<div id="header-wrap" class="bg-light">
					<div class="container">
						<div class="row header-row">							
							<div class="col-md-6">
								<headlines-component />
								<div id="primary-menu-trigger">
									<svg class="svg-trigger" viewBox="0 0 100 100"><path d="m 30,33 h 40 c 3.722839,0 7.5,3.126468 7.5,8.578427 0,5.451959 -2.727029,8.421573 -7.5,8.421573 h -20"></path><path d="m 30,50 h 40"></path><path d="m 70,67 h -40 c 0,0 -7.5,-0.802118 -7.5,-8.365747 0,-7.563629 7.5,-8.634253 7.5,-8.634253 h 20"></path></svg>
								</div>
							</div>
							<div class="col-md-6">
								<div class="row header-buttons">
									<div class="col-12 col-md-4 my-1 my-md-0 text-center">
										<a href="https://proyecto.webescuela.cl/sistema/webclass/" class="w-100 button button-rounded button-border button-small m-0" title="Web Class" target="_blank">Web Class</a>
									</div>
									<div class="col-12 col-md-4 my-1 my-md-0 text-center">
										<a href="https://classroom.google.com/u/0/h" class="w-100 button button-rounded button-border button-small m-0" title="Classroom" target="_blank">Classroom</a>
									</div>
									<div class="col-12 col-md-4 my-1 my-md-0 text-center">
										<a href="https://mitextoescolar.mineduc.cl/" class="w-100 button button-rounded button-border button-small m-0" title="Textos Escolares" target="_blank">Textos Escolares</a>
									</div>
								</div>
							</div>
							<div class="col-12">
								<form class="top-search-form" method="GET">
									<input type="text" name="query" class="form-control" value="" placeholder="Type &amp; Escribe Algo..." autocomplete="off">
								</form>
							</div>
						</div>
					</div>
				</div>
				<div class="header-wrap-clone"></div>
			</header><!-- #header end -->			
			<!-- Page Title
			============================================= -->
			@yield('page_title')
			<!-- Slider
			============================================= -->
			@yield('slider')
			<!-- Content
			============================================= -->
			<section id="content">
				@yield('content')
			</section><!-- #content end -->
			<!-- Footer
			============================================= -->
			<footer id="footer" class="dark">
				<div class="container" id="contacto">
					<!-- Footer Widgets
					============================================= -->
					<div class="footer-widgets-wrap">
						<div class="row">
							<div class="col-12 col-md" style="background: url({{ asset('assets/site/images/world-map.png') }}) no-repeat center center; background-size: 100%;">
								<img class="mb-3" src="{{ asset('assets/site/images/logo_london_college_footer.png') }}" alt="Footer Logo">
								<small class="d-block mb-4 text-muted">&copy; 2020-2021</small>
								<p class="text-white-50" style="font-size: 16px;">El Centro Educacional London, es una organización de educación que proporciona una formación integral del educando.</p>
							</div>
							<div class="col-12 col-md">
								<h4 class="text-uppercase ls2 font-weight-normal">ENLACES RECOMENDADOS</h4>
								<ul class="list-unstyled mb-0">
									<li><h5 class="mb-2 font-weight-normal"><a href="https://www.educarchile.cl" target="_blank" title="Educar Chile">Educar Chile</a></h5></li>
									<li><h5 class="mb-2 font-weight-normal"><a href="http://www.enlaces.cl" target="_blank" title="Enlaces">Enlaces</a></h5></li>
									<li><h5 class="mb-2 font-weight-normal"><a href="http://www.yoestudio.cl" target="_blank" title="Yo Estudio">Yo Estudio</a></h5></li>
									<li><h5 class="mb-2 font-weight-normal"><a href="http://www.eligevivirsano.cl" target="_blank" title="Elige Vivir Sano">Elige Vivir Sano</a></h5></li>
									<li><h5 class="mb-2 font-weight-normal"><a href="https://www.curriculumnacional.cl/estudiantes/" target="_blank" title="Aprendo en Línea">Aprendo en Línea</a></h5></li>
								</ul>
							</div>
							<div class="col-12 col-md col-sm mt-4 mt-md-0 mt-lg-0 mt-xl-0">
								<h4 class="text-uppercase ls2 font-weight-normal">Accesos Directos</h4>
								<ul class="list-unstyled mb-0">
									<li><h5 class="mb-2 font-weight-normal"><a href="{{ route('site.mission') }}" title="Misión -  Visión">Misión -  Visión</a></h5></li>
									<li><h5 class="mb-2 font-weight-normal"><a href="{{ route('site.organization') }}" title="Organigrama">Organigrama</a></h5></li>
									<li><h5 class="mb-2 font-weight-normal"><a href="{{ route('site.review') }}" title="Reseña histórica">Reseña histórica</a></h5></li>
									<li><h5 class="mb-2 font-weight-normal"><a href="{{ route('site.about') }}" title="Colegio">Nuestro Colegio</a></h5></li>
									<li><h5 class="mb-2 font-weight-normal"><a href="{{ asset('assets/site/resources/proyecto-educativo.pdf') }}" title="Proyecto Educativo" target="_blank">Proyecto Educativo</a></h5></li>
									<li><h5 class="mb-2 font-weight-normal"><a href="{{ asset('assets/site/resources/reglamento-evaluacion.pdf') }}" title="Reglamento de Evaluación" target="_blank">Reglamento de Evaluación</a></h5></li>
								</ul>
							</div>
							<div class="col-12 col-md mt-4 mt-md-0 mt-lg-0 mt-xl-0">
								<h4 class="text-uppercase ls2 font-weight-normal">Contacto</h4>
								<div class="clearfix">
									<h4 class="mb-0">Sede Párvulo</h4>
									<a href="https://goo.gl/maps/MoK3dVP1B7CftthN6" target="_blank" title="Dirección"><i class="icon-line-map-pin mr-2"></i>Tauro #10520</a>
								</div>
								<div class="mt-2 clearfix">
									<h4 class="mb-0">Sede Básica</h4>
									<a href="https://goo.gl/maps/TABD13CPmNTf6ixQ7" target="_blank" title="Dirección"><i class="icon-line-map-pin mr-2"></i>Gral Arriagada #752</a>
								</div>
								<div class="mt-2 clearfix">
									<h4 class="mb-0">Sede Media</h4>
									<a href="https://goo.gl/maps/ijc93z1DCC1m7gkYA" target="_blank" title="Dirección"><i class="icon-line-map-pin mr-2"></i>San José De La Estrella #603</a>
								</div>
								<div class="mt-3 clearix">
									<a href="tel:+5622823235" class="mb-1 d-block" title="Teléfono"><i class="icon-line2-call-in mr-2"></i>+56 2282 3235</a>
									<a href="mailto:contacto@londoncollege.cl" title="Email"><i class="icon-line2-envelope mr-2"></i>contacto@londoncollege.cl</a>
								</div>															
							</div>
						</div>
					</div><!-- .footer-widgets-wrap end -->
				</div>
				<!-- Copyrights
				============================================= -->
				<div id="copyrights">
					<div class="container clearfix">
						<div class="row align-items-center justify-content-between">
							<div class="col-md-6">
								<div class="copyright-links">Desarrollado por <a href="http://www.webworks.cl" target="_blank">Webworks</a><br>
								<a href="#" title="Términos de Uso">Términos de Uso</a> / <a href="#" title="Políticas de Privacidad">Políticas de Privacidad</a></div>
							</div>
							<div class="col-md-6 d-flex justify-content-md-end mt-4 mt-md-0">
								<div class="copyrights-menu copyright-links mb-0 clearfix">
									<a href="{{ route('site.index') }}" title="Inicio">Inicio</a>/<a href="{{ route('site.news') }}" title="Noticias">Noticias</a>/<a href="#" title="Admisión">Admisión</a>/<a href="{{ route('site.galleries') }}" title="Galería">Galería</a>
								</div>
							</div>
						</div>
					</div>
				</div><!-- #copyrights end -->
			</footer><!-- #footer end -->
		</div><!-- #wrapper end -->
		<!-- Go To Top
		============================================= -->
		<div id="gotoTop" class="icon-angle-up"></div>
	</div>
    
    @section('scripts')
        <!-- JavaScripts
        ============================================= -->
        <script src="{{ asset('assets/site/js/jquery.js') }}"></script>		
        <script src="{{ asset('assets/site/js/plugins.min.js') }}"></script>		
		<script src="{{ asset('assets/site/dist/js/app-v20231013.js') }}"></script>

        <!-- Footer Scripts
        ============================================= -->
		<script src="{{ asset('assets/site/js/functions.js') }}"></script>
		<script>
			/** Scroll To Section **/
            $('.menu-scroll').click(function() {
                var targetDiv = $(this).attr('href');
                $('html, body').animate({
                    scrollTop: $(targetDiv).offset().top - 20
                }, 1500);
            });
		</script>
    @show
</body>
</html>
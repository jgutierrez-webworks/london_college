@extends('site.template.base')

@section('slider')

	@if($banners->isNotEmpty())

		@if($banners->count() > 1)
			<section id="slider" class="slider-element slider-parallax swiper_wrapper min-vh-custom clearfix" data-effect="fade" data-autoplay="8000" data-speed="650" data-loop="true">
		@else
			<section id="slider" class="slider-element slider-parallax swiper_wrapper min-vh-custom clearfix">
		@endif
			<div class="slider-parallax-inner">
				<div class="swiper-container swiper-parent">
					<div class="swiper-wrapper">
						@foreach ($banners as $banner)
							@if(Functions::isVideoExtension($banner->file->extension))			
								<div class="swiper-slide">
									<div class="vertical-middle">
										<div class="container text-center">
											<div class="row justify-content-center">
												<div class="col-md-8">
													<div class="slider-title mb-5 dark clearfix">
														<h2 class="text-white text-rotater mb-2" data-separator="," data-rotate="fadeIn" data-speed="3000">
															@if($banner->titulo_principal)
																{{ $banner->titulo_principal }}
															@endif

															@if($banner->titulo_secundario)
																<span class="t-rotate text-white">{{ $banner->titulo_secundario }}</span>
															@endif
														</h2>

														@if($banner->link_redirect_activo)
															<a href="{{ ($banner->url_link_redirect) ? $banner->url_link_redirect : '#' }}" class="button button-rounded button-small btn-custom-london m-0 ml-2">Ver más ...</a><br>
														@endif
													</div>
													<div class="clear"></div>
													<p class="lead mb-0 text-uppercase text-white ls2" style="font-size: 110%">Centro Educacional London 2021</p>													
												</div>
											</div>
										</div>
									</div>
									<div class="video-wrap">
										<video id="slide-video" preload="auto" loop autoplay muted>
											@if(Functions::isMobile() && !empty($banner->fileMobile))
												<source src="{{ asset('storage/banners/' . $banner->codigo . '/mobile/' . $banner->fileMobile->nombre) }}" />
											@else
												<source src="{{ asset('storage/banners/' . $banner->codigo . '/' . $banner->file->nombre) }}" />
											@endif
										</video>
										<div class="video-overlay" style="background-color: rgba(0,0,0,0.55);"></div>
									</div>
								</div>
							@else
								@if(Functions::isMobile())
									@if(!empty($banner->fileMobile))
										<div class="swiper-slide" style="background-image: url({{ asset('storage/banners/' . $banner->codigo . '/mobile/' . $banner->fileMobile->nombre) }}); background-size: cover; background-position: center;">
									@else
										<div class="swiper-slide" style="background-image: url({{ asset('storage/banners/' . $banner->codigo . '/thumbs-min/' . $banner->file->nombre) }}); background-size: cover; background-position: center;">
									@endif
								@else
									<div class="swiper-slide" style="background-image: url({{ asset('storage/banners/' . $banner->codigo . '/' . $banner->file->nombre) }}); background-size: cover;">
								@endif
									<div class="vertical-middle">
										<div class="container text-center">
											<div class="row justify-content-center">
												<div class="col-md-7">
													<div class="slider-title mb-5 dark clearfix">
														<h2 class="text-white text-rotater mb-2" data-separator="," data-rotate="fadeIn" data-speed="3000">
															@if($banner->titulo_principal)
																{{ $banner->titulo_principal }}
															@endif

															@if($banner->titulo_secundario)
																<span class="t-rotate text-white">{{ $banner->titulo_secundario }}</span>
															@endif
														</h2>

														@if($banner->link_redirect_activo)
															<a href="{{ ($banner->url_link_redirect) ? $banner->url_link_redirect : '#' }}" class="button button-rounded button-small btn-custom-london m-0 ml-2">Ver más ...</a><br>
														@endif
													</div>													
												</div>
											</div>
										</div>
									</div>
								</div>
							@endif
						@endforeach
					</div>
				</div>
			</div>
		</section>
	@endif

@endsection

@section('content')

    <div class="content-wrap pt-0" style="overflow: visible;">
		<!-- Section Courses
		============================================= -->
		<div class="section parallax" style="padding: 50px 0 60px; background-image: url({{ asset('assets/site/demos/course/images/icon-pattern.jpg') }}); background-size: auto; background-repeat: repeat"  data-bottom-top="background-position:0px 100px;" data-top-bottom="background-position:0px -500px;">
			<!-- Wave Shape Divider
			============================================= -->
			<div class="wave-top" style="position: absolute; top: 0; left: 0; width: 100%; background-image: url({{ asset('assets/site/demos/course/images/wave-3.svg') }}); height: 12px; z-index: 2; background-repeat: repeat-x;"></div>
			<div class="container mb-3">
				<div class="row">
					<div class="col-12 col-md-8 offset-md-2">						
						<p class="lead mb-0 font-weight-bold text-uppercase text-center ls2" style="font-size: 110%">Centro Educacional London {{ date('Y') }}</p>
						<div class="clearfix"></div>
						<search-form-component />
					</div>
				</div>
			</div>
			<div class="container">
				<div class="heading-block border-bottom-0 mb-5 center">
					<h3>Comunicados & Noticias</h3>
					<span>Estar comunnicados con la comunidad educacional es uno de los pilares fundamentales, este espacio busca facilitar el acceso a toda la información oficial.</span>
				</div>
				<div class="clear"></div>
				<recent-news-component />
			</div>
			<!-- Wave Shape Divider - Bottom
			============================================= -->
			<div class="wave-bottom" style="position: absolute; top: auto; bottom: 0; left: 0; width: 100%; background-image: url({{ asset('assets/site/demos/course/images/wave-3.svg') }}); height: 12px; z-index: 2; background-repeat: repeat-x; transform: rotate(180deg);"></div>
		</div>
		<!-- Wave Shape Divider
		============================================= -->
		<div class="wave-bottom" style="position: absolute; top: -12px; left: 0; width: 100%; background-image: url({{ asset('assets/site/demos/course/images/wave-3.svg') }}); height: 12px; z-index: 2; background-repeat: repeat-x; transform: rotate(180deg);"></div>
		<div class="container">
			<div class="heading-block border-bottom-0 my-4 center">
				<img src="{{ asset('assets/site/images/logo_london_college.png') }}" alt="London College">
				<h3>Departamentos & UTP</h3>
				<span>Estimada comunidad London, este espacio busca facilitar el acceso a las actividades desarrolladas por la comunidad.</span>
			</div>
			<!-- Categories
			============================================= -->
			<div class="row course-categories clearfix mb-4">
				<div class="col-lg-2 col-sm-3 col-6 mt-4">
					<div class="card hover-effect">
						<img class="card-img" src="{{ asset('assets/site/demos/course/images/categories/teacher.jpg') }}" alt="Card image">
						<a href="#" class="card-img-overlay rounded p-0" style="background-color: rgba(50,71,66,0.8);">
							<span><i class="icon-line2-user"></i>Orientación</span>
						</a>
					</div>
				</div>
				<div class="col-lg-2 col-sm-3 col-6 mt-4">
					<div class="card hover-effect">
						<img class="card-img" src="{{ asset('assets/site/demos/course/images/categories/business.jpg') }}" alt="Card image">
						<a href="#" class="card-img-overlay rounded p-0" style="background-color: rgba(252,108,52,0.8);">
							<span><i class="icon-chart-bar1"></i>Matemáticas</span>
						</a>
					</div>
				</div>					
				<div class="col-lg-2 col-sm-3 col-6 mt-4">
					<div class="card hover-effect">
						<img class="card-img" src="{{ asset('assets/site/demos/course/images/categories/language.jpg') }}" alt="Card image">
						<a href="#" class="card-img-overlay rounded p-0" style="background-color: rgba(80,167,159,0.8);">
							<span><i class="icon-line2-globe"></i>Languaje</span>
						</a>
					</div>
				</div>
				<div class="col-lg-2 col-sm-3 col-6 mt-4">
					<div class="card hover-effect">
						<img class="card-img" src="{{ asset('assets/site/demos/course/images/categories/health.jpg') }}" alt="Card image">
						<a href="#" class="card-img-overlay rounded p-0" style="background-color: rgba(158,108,87,0.8);">
							<span><i class="icon-heartbeat"></i>Salud & Deporte</span>
						</a>
					</div>
				</div>					
				<div class="col-lg-2 col-sm-3 col-6 mt-4">
					<div class="card hover-effect">
						<img class="card-img" src="{{ asset('assets/site/demos/course/images/categories/music.jpg') }}" alt="Card image">
						<a href="#" class="card-img-overlay rounded p-0" style="background-color: rgba(251,51,100,0.8);">
							<span><i class="icon-music1"></i>Música</span>
						</a>
					</div>
				</div>
				<div class="col-lg-2 col-sm-3 col-6 mt-4">
					<div class="card hover-effect">
						<img class="card-img" src="{{ asset('assets/site/demos/course/images/categories/paint.jpg') }}" alt="Card image">
						<a href="#" class="card-img-overlay rounded p-0" style="background-color: rgba(23,116,234,0.8);">
							<span><i class="icon-paint-brush"></i>Arte</span>
						</a>
					</div>
				</div>
			</div>
			<div class="clear"></div>
		</div>
		<!-- Featues Section
		============================================= -->
		<div class="section mt-5 mb-0" style="padding: 80px 0; background-image: url({{ asset('assets/site/demos/course/images/icon-pattern-bg.jpg') }}); background-size: auto; background-repeat: repeat">
			<!-- Wave Shape
			============================================= -->
			<div class="wave-top" style="position: absolute; top: 0; left: 0; width: 100%; background-image: url({{ asset('assets/site/demos/course/images/wave-3.svg') }}); height: 12px; z-index: 2; background-repeat: repeat-x;"></div>
			<div class="container">
				<div class="row">
					<div class="col-lg-8">
						<div class="row dark clearfix">
							<!-- Feature - 1
							============================================= -->
							<div class="col-md-6">
								<div class="feature-box media-box pb-4">
									<div class="fbox-icon">
										<a href="#">
											<i class="icon-line2-book-open rounded-0 bg-transparent text-left"></i>
										</a>
									</div>
									<div class="fbox-content">
										<h3 class="text-white">Proyecto Educativo</h3>
										<p class="text-white">
											La formulación de este Proyecto Educativo, nace del trabajo participativo
											de toda la Comunidad Educativa del Centro Educacional London, definiendo su
											identidad bajo una visión humanista laica.
										</p>										
										<a href="{{ asset('assets/site/resources/proyecto-educativo.pdf') }}" class="mt-2 more-link text-white" title="Leer Más" target="_blank">Leer Más</a>
									</div>
								</div>
							</div>
							<!-- Feature - 2
							============================================= -->
							<div class="col-md-6">
								<div class="feature-box media-box pb-4">
									<div class="fbox-icon">
										<a href="#">
											<i class="icon-line2-note rounded-0 bg-transparent text-left"></i>
										</a>
									</div>
									<div class="fbox-content">
										<h3 class="text-white">Postulaciones y admisión</h3>
										<p class="text-white">
											Pueden postularse quienes ingresan por primera vez o deseen cambiarse de un establecimiento público o particular, quienes no tengan continuidad el siguiente nivel, quienes quieran reingresar al sistema educativo.
										</p>
										<a href="{{ asset('assets/site/resources/postulaciones-admision.pdf') }}" class="mt-2 more-link text-white" title="Leer Más" target="_blank">Leer Más</a>
									</div>
								</div>
							</div>
							<!-- Feature - 3
							============================================= -->
							<div class="col-md-6">
								<div class="feature-box media-box pb-4">
									<div class="fbox-icon">
										<a href="#">
											<i class="icon-line2-user rounded-0 bg-transparent text-left"></i>
										</a>
									</div>
									<div class="fbox-content">
										<h3 class="text-white">Reglamento de Evaluación</h3>
										<p class="text-white">
											El Centro Educacional London concibe la evaluación como un medio para
											fomentar y estimular el aprendizaje y mejoramiento continuo de los estudiantes(as).
										</p>
										<a href="{{ asset('assets/site/resources/reglamento-evaluacion.pdf') }}" class="mt-2 more-link text-white" title="Leer Más" target="_blank">Leer Más</a>
									</div>
								</div>
							</div>
							<!-- Feature - 4
							============================================= -->
							<div class="col-md-6">
								<div class="feature-box media-box pb-4">
									<div class="fbox-icon">
										<a href="#">
											<i class="icon-line2-globe rounded-0 bg-transparent text-left"></i>
										</a>
									</div>
									<div class="fbox-content">
										<h3 class="text-white">Comunidad Educacional London</h3>
										<p class="text-white">
											Comunidad educativa comprometida con el desarrollo integral de sus
											estudiantes, que proporciona oportunidades pedagógicas en todo el proceso
											educativo.
										</p>
										<a href="{{ asset('assets/site/resources/proyecto-educativo.pdf') }}" class="mt-2 more-link text-white" title="Leer Más" target="_blank">Leer Más</a>
									</div>
								</div>
							</div>
						</div>
					</div>
					<!-- Registration Foem
					============================================= -->
					<div class="col-lg-4">
						<div class="card shadow" style="opacity: 1 !important">
							<div class="card-body">
								<div class="badge registration-badge shadow-sm alert-primary">CONTACTO</div>
								<h4 class="card-title ls-1 mt-4 font-weight-bold h5">Resolvamos tus dudas, contáctenos!</h4>
								<h6 class="card-subtitle mb-4 font-weight-normal text-uppercase ls2 text-black-50">COLEGIO EDUCACIONAL LONDON - LA FLORIDA.</h6>
								
								<contact-form-component />
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- Wave Shape
			============================================= -->
			<div class="wave-bottom" style="position: absolute; top: auto; bottom: 0; left: 0; width: 100%; background-image: url({{ asset('assets/site/demos/course/images/wave-3.svg') }}); height: 12px; z-index: 2; background-repeat: repeat-x; transform: rotate(180deg);"></div>
		</div>
		<!-- Promo Section
		============================================= -->
		<div class="section m-0">
			<div class="container">
				<div class="row">
					<div class="col-md-5 center">
						<img src="{{ asset('assets/site/images/directora.jpg') }}" class="mt-5 img-fluid img-thumbnail" loading="lazy" alt="Directora" data-animate="fadeInLeft">
					</div>
					<div class="offset-md-1 col-md-6 float-right">
						<div class="heading-block border-bottom-0 mb-4">
							<img src="{{ asset('assets/site/images/logo_london_college.png') }}" alt="London College">
							<h3 class="mt-3">Palabras de la Directora!</h3>
							<span>Educamos con verdad, honestidad y respeto! </span>
						</div>
						<p class="mb-2 text-justify">
							Quisiéramos extender nuestro profundo agradecimiento a padres, madres, 
							apoderados, estudiantes, al igual que a nuestros docentes y asistentes de la 
							educación, por sus esfuerzos para adaptarse a los cambios que ha traido la 
							Pandemia. Estamos muy agradecidos de todos los integrantes de nuestra 
							comunidad por haber demostrado positivismo, flexibilidad y fortaleza durante estos 
							tiempos difíciles ayudándonos a seguir de la mejor forma con el proceso escolar 
							aprendiendo en conjunto y tomando las mejores decisiones para el logro de los 
							objetivos.
						</p>
						<a href="{{ asset('assets/site/resources/comunicado-directora.pdf') }}" class="button button-rounded button-large btn-custom-london ls0 ls0 nott font-weight-normal m-0 mt-3" title="Nuestro Colegio" target="_blank">Ver Más</a>
					</div>
				</div>
			</div>
		</div>
	</div>
    
@endsection
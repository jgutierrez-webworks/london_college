@extends('site.template.base')

@section('page_title')
	
	<section id="page-title" class="page-title-parallax page-title-dark" style="background-image: url({{ asset('assets/site/images/bg-news.jpg') }}); background-size: cover; padding: 120px 0;" data-bottom-top="background-position:0px 0px;" data-top-bottom="background-position:0px -300px;">
		<div class="container clearfix">
			<h1>Reseña Histórica</h1>
			<span>&nbsp;</span>
			<ol class="breadcrumb">
				<li class="breadcrumb-item"><a href="{{ route('site.index') }}">Inicio</a></li>
				<li class="breadcrumb-item active" aria-current="page">Reseña Histórica</li>
			</ol>
		</div>
	</section>

@endsection

@section('content')
    
    <div class="content-wrap">
		<div class="container clearfix">
			<!-- Post Content
			============================================= -->
			<div class="postcontent nobottommargin col_last clearfix">				
				<div class="row">
                    <div class="col">
                        <p class="text-justify">
                            El Centro Educacional London, es una Institución Educativa con 34
                            años de vida, fue creada en el año 1981 en la avenida General Arriagada 752
                            como Escuela Básica, siendo sus Fundadores el matrimonio Fuentes Azola,
                            ocupando el cargo de Sostenedora la Sra. María Azola Ulloa y el cargo de
                            Director el Señor Sergio Fuentes Alcayaga.
                        </p>
                        <p class="text-justify">
                            En sus comienzos el colegio funcionaba con pre kínder y kínder y 1 y 2
                            combinado. Luego la matrícula se acrecentó llegando a tener tres cursos por
                            nivel y Enseñanza Media para los cursos de 1º medios.
                        </p>
                        <p class="text-justify">
                            En el año 1995 se planifica un proyecto ambicioso con Jornada escolar
                            completa que consiste en entregar una educación de calidad a nuestros
                            estudiantes egresados de enseñanza básica, en su nuevo local Anexo
                            ubicado en San José de la estrella 603. De esta manera se trabaja con 3
                            Primeros Medios con Enseñanza Media Humanista Científica. Este mismo
                            año, se hace un estudio de mercado con la población atendida en nuestra
                            comunidad escolar y se decide ampliar la Enseñanza Media a Técnico
                            Profesional, convirtiéndose en un Colegio Polivalente, donde se da la
                            alternativa de dos carreras técnicas, Contabilidad Computacional y Técnico en
                            Control de Calidad.
                        </p>
                        <p class="text-justify">
                            Con la llegada de la Reforma Educacional, se inicia nuevamente el
                            proceso de estudio de las carreras técnicas, que satisficiera las necesidades de
                            nuestros alumnos, lo que permite ampliarnos nuevamente en las carreras de
                            Gastronomía y Administración.
                        </p>
                    </div>
                </div>
			</div><!-- .postcontent end -->
			<!-- Sidebar
			============================================= -->
			<div class="sidebar nobottommargin clearfix">
				<sidebar-component filter="inside" />
			</div><!-- .sidebar end -->
		</div>
	</div>

@endsection
import Constants from './../constants'

export default {
    methods: {
        validateRut(rut) {
            rut = rut.toLowerCase()
            rut = rut.split('-')

            if (rut.length < 2) return false

            let T = rut[0]
            let DV1 = rut[1]
            let M = 0, S = 1, R = T

            for (; T; T = Math.floor(T / 10))
                S = (S + T % 10 * (9 - M++ % 6)) % 11

            let DV2 = S ? S - 1 : 'k'

            if (DV1 != DV2) return false

            return true
        },
        getAge(date) {
            let today = new Date()
            let birthDate = new Date(date)
            let age = today.getFullYear() - birthDate.getFullYear();

            const  m = today.getMonth() - birthDate.getMonth()

            if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
                age--
            }
            
            return age
        },
        getTitleBySlug(slug) {
            var words = slug.split('-')

            for (var i = 0; i < words.length; i++) {
                var word = words[i]
                words[i] = word.charAt(0).toUpperCase() + word.slice(1)
            }

            return words.join(' ')
        },
        getFullBaseUrl() {
            return Constants.BASE_URL
        },
        getAssetURLByPath(path) {
            return `${Constants.BASE_URL}/${path}`
        }
    },
    data() {
        return {
            baseURL: ''
        }
    },
    created() {
        this.baseURL = process.env.MIX_BASE_URL
    }
}
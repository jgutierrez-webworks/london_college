import Vue from 'vue'

export default {
    methods: {
        openLoader() {
            this.loader = Vue.$loading.show(this.config)
        },
        closeLoader() {
            if(this.loader !== null) {
                this.loader.hide()
            }
        }
    },
    data() {
        return {
            loader: null,
            config: { 
                color: '#0474c4' 
            }
        }
    }
}
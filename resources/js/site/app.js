/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

/** 
 * Vue
 */
import Vue from "vue";

/**
 * Vue Bus
 */
import VueBus from 'vue-bus';

/**
 * Vue Composition API
 */
import VueCompositionAPI from '@vue/composition-api'

/**
 * Vue Toast
 */
import VueToast from 'vue-toast-notification';
// Import any of available themes
import 'vue-toast-notification/dist/theme-default.css';

// Import component
import VueLoading from 'vue-loading-overlay';
// Import stylesheet
import 'vue-loading-overlay/dist/vue-loading.css';

// Vue Bus
Vue.component('VueBus', VueBus);

// Composition API
Vue.use(VueCompositionAPI)

// Vue Loading
Vue.use(VueLoading);

// Vue toast
Vue.use(VueToast, {
    transition: "Vue-Toastification__bounce",
    maxToasts: 20,
    newestOnTop: true,
    position: "top-right",
    timeout: 8000,
    closeOnClick: true,
    pauseOnFocusLoss: true,
    pauseOnHover: true,
    draggable: true,
    draggablePercent: 0.6,
    showCloseButtonOnHover: false,
    hideProgressBar: false,
    closeButton: "button",
    icon: true,
    filterBeforeCreate: (toast, toasts) => {
        if (toasts.filter(
            t => t.type === toast.type
        ).length !== 0) {
            return false;
        }
        return toast;
    }
});

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

/** Template **/
Vue.component('headlines-component', require('./components/Template/HeadlinesComponent.vue').default);
Vue.component('sections-menu-component', require('./components/Template/SectionsMenuComponent.vue').default);

/** Index */
Vue.component('contact-form-component', require('./components/Index/ContactFormComponent.vue').default);
Vue.component('recent-news-component', require('./components/Index/RecentNewsComponent.vue').default);
Vue.component('search-form-component', require('./components/Index/SearchFormComponent.vue').default);

/** News */
Vue.component('news-component', require('./components/News/NewsComponent.vue').default);
Vue.component('news-item-component', require('./components/News/NewsItemComponent.vue').default);
Vue.component('sidebar-component', require('./components/News/SidebarComponent.vue').default);

/** Galleries */
Vue.component('gallery-component', require('./components/Galleries/GalleryComponent.vue').default);

const app = new Vue({
    el: '#app',
});
import axios from 'axios'

class CustomService {
    async getSections() {
        try {
            const response = await axios.get(route('api.customs.sections.all'))
            return response.data
        } catch (error) {
            console.log(error)
        }
    }

    async getCategories() {
        try {
            const response = await axios.get(route('api.customs.categories.all'))
            return response.data
        } catch (error) {
            console.log(error)
        }
    }

    async getFrequentCategories() {
        try {
            const response = await axios.get(route('api.customs.categories.frequent'))
            return response.data
        } catch (error) {
            console.log(error)
        }
    }

    async getFilterParams() {
        const urlSearchParams = new URLSearchParams(window.location.search)
        const params = {}

        if (urlSearchParams.get('search')) {
            params.search = urlSearchParams.get('search')
        }

        if (urlSearchParams.get('section')) {
            params.section = urlSearchParams.get('section')
        }

        if (urlSearchParams.get('category')) {
            params.category = urlSearchParams.get('category')
        }

        if (urlSearchParams.get('tags')) {
            params.tags = urlSearchParams.get('tags')
        }

        return Promise.resolve(params)
    }
}

export default new CustomService()
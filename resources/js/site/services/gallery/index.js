import axios from 'axios'

class GalleryService {
    async getAllGalleries() {
        try {
            const response = await axios.get(route('api.site.galleries.all'))
            return response.data
        } catch (error) {
            console.log(error)
        }
    }
}

export default new GalleryService()
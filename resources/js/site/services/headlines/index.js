import axios from 'axios'

class HeadlinesService {
    async getAllHeadlines() {
        try {
            const response = await axios.get(route('api.site.headlines.all'))
            return response.data
        } catch (error) {
            console.log(error)
        }
    }
}

export default new HeadlinesService()
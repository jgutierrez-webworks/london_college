import axios from 'axios'

class NewsService {
    async getNewsByFilters(params) {
        try {
            const response = await axios.get(route('api.site.news.all', params))
            return response.data
        } catch (error) {
            console.log(error)
        }
    }

    async getFeaturedNews() {
        try {
            const response = await axios.get(route('api.site.news.featured'))
            return response.data
        } catch (error) {
            console.log(error)
        }
    }

    async getRecentNews() {
        try {
            const response = await axios.get(route('api.site.news.recent'))
            return response.data
        } catch (error) {
            console.log(error)
        }
    }

    async getFullNewsBySlug(slug) {
        try {
            const response = await axios.get(route('api.site.news.show', slug))
            return response.data
        } catch (error) {
            console.log(error)
        }
    }

    async getRelatedNewsBySlug(slug) {
        try {
            const response = await axios.get(route('api.site.news.related', slug))            
            return response.data
        } catch (error) {
            console.log(error)
        }
    }
}

export default new NewsService()